<?php 
  use App\Http\Controllers\Frontend\ApiContreller;

    $totalAmount = '';
    $totalVat = '';
    $totalAll = '';
    $iNum = 1;
   $user = \Auth::user();
   $user_level = $user->level; 
    
?>@extends('backend.layout.master') 
@section('titlepage', 'รายงานสต๊อก'.$textTypeSouvenir) 
@section('topScript')
 @endsection 
@section('content')
  <div class="panel-body">
  	 <div class="text-right" style="padding-top:5px"><form class="form-inline" method="get"> 
        <div class="form-group">
	        <label>กรองผลลัพท์ </label>
	         <div class="form-group">
	       <?php #==================================== Start !=  staff ===================================
	           if($user_level != 'Staff'){  ?> 
	        <select name="event_id" class="form-control" > 
	              <option value="">--เลือก{{$menuEventName}}--</option> 
	              <?php
	                 $rowEvents = ApiContreller::getEventLists();
	                 foreach ($rowEvents as $rowEvent) {
	                    $event_name    = json_decode($rowEvent->event_name, true);
	                    $event_name    = $event_name['data']['th'];
	                  ?>
	              <option value="{{$rowEvent->event_id}}" <?php echo ($event_id == $rowEvent->event_id)? "selected": "";?>  >{{ApiContreller::strCrop($event_name, 22)}}</option>
	              <?php }?>
	          </select>
	        <?php }#==================================== End !=  staff ===================================?>
	        </div>
	          
          <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>ค้นหา</button> 
        </div></form>
        <br></div>

       <div class="panel-body"> 
       	<?php 
       	  $CateSouvenirLists = ApiContreller::getCateSouvenirLists($event_id);
          $getValSouvenir =  json_encode($CateSouvenirLists);

         if($getValSouvenir != '' && $getValSouvenir != '[]'){ 
	      foreach ($CateSouvenirLists as $CateSouvenirList) {
	      		$row_souvenir 	= ApiContreller::getCateSouvenir(  $CateSouvenirList['souvenir_id']);
                $souvenir_name 	= json_decode($row_souvenir->souvenir_name, true); 
                $souvenir_name 	=  $souvenir_name['data']['th'];
                $souvenir_id 	= $row_souvenir->souvenir_id;  

       	?><div class="row"> 
	          <div class="col-md-12">
	            <div class="panel-cascade">
	              <div class="panel-heading">
	                <h3 class="panel-title"> <i class="fa fa-folder"></i> {{$souvenir_name}}</h3>
	              </div>
	              <div class="row"> 
	              <table class="table table-striped">
	              <thead> 
	                <tr>
	                  <th>ไซส์</th>
	                  <th>จำนวนสต๊อกทั้งหมด</th>
	                  <th>ชำระเงินเรียบร้อย</th> 
	                  <th>VIP</th>
	                  <th>รอการชำระเงิน</th>
	                  <th>ทั้งหมด</th> 
	                  <th>คงเหลือ</th>
	                   <th>รับแล้ว</th>
	              </tr>
	              </thead>
	              <tbody> 
	              	<?php 
	              		$totalStock    = 0;
	               		$totalTicketSuccess    = 0;
	               		$totalTicketWait    = 0;
	               		$totalTicketSucc_Wait    = 0;
	               		$totalTicketTotal    = 0;
	               		$totalTicketVIP    = 0;
	               		$totlaResultReceiveSuccess = 0;

	              	  $Souvenirs = ApiContreller::getSouvenir($event_id, $souvenir_id);
         			  $getSouvenirs =  json_encode($Souvenirs);

         			  if($getSouvenirs != '' && $getSouvenirs != '[]'){ 
         			  	foreach($Souvenirs as $Souvenir) {
         			  		$souvenir_type_name = json_decode($Souvenir->souvenir_type_name, true); 
                			$souvenir_type_name = $souvenir_type_name['data']['th']; 
                 			$souvenir_type_id 	= $Souvenir->souvenir_type_id;
                 			$souvenir_type_stock = $Souvenir->souvenir_type_stock;

                 			$souvenirSuccess 	= ApiContreller::getFuncSouvenirReport($souvenir_type_id , 'SUCCESS' );
                 		 
                 			$souvenirWait 	= ApiContreller::getFuncSouvenirReport($souvenir_type_id , 'WAIT' );
                 		    $countResultReceiveSccuess 	= ApiContreller::getSouvenirReceivedReport($souvenir_type_id , 'SUCCESS' );

							$souvenirSucc_Wait = $souvenirSuccess+$souvenirWait;
							$souvenirTotal = ($souvenir_type_stock-$souvenirSucc_Wait);
	              	?>
	              	 <tr>
	                  <td>{{$souvenir_type_name}}</td>
	                  <td>{{$souvenir_type_stock}}</td>
	                  <td>{{$souvenirSuccess}}</td> 
	               
	                  <td>{{$souvenirWait}}</td>
	                  <td>{{$souvenirSucc_Wait}}</td>
	                  <td>{{$souvenirTotal}}</td> 
	                  <td>{{$countResultReceiveSccuess}}</td> 
	              	</tr>
	              	<?php
	              		$totalStock     +=   $souvenir_type_stock;
	               		$totalTicketSuccess     +=   $souvenirSuccess;
	               		$totalTicketWait     +=   $souvenirWait;
	               		$totalTicketSucc_Wait     +=   $souvenirSucc_Wait;
	               		$totalTicketTotal     +=   $souvenirTotal;  
	               		$totlaResultReceiveSuccess     +=   $countResultReceiveSccuess;  
	              	 }?> 
	                  <tr>
	                  <th>รวม</th>
	                  <th><?php echo number_format($totalStock, 0, '.', ',');?></th>
	                  <th><?php echo number_format($totalTicketSuccess, 0, '.', ',');?></th> 
	                  <th><?php echo number_format($totalTicketWait, 0, '.', ',');?></th>
	                  <th><?php echo number_format($totalTicketSucc_Wait, 0, '.', ',');?></th>
	                  <th><?php echo number_format($totalTicketTotal, 0, '.', ',');?></th>
	                  <th><?php echo number_format($totlaResultReceiveSuccess, 0, '.', ',');?></th>
	              	 </tr>
	              	<?php }#end ?>
	              </tbody>
	              </table>
	            </div>
	            </div>
	          </div> 
   			</div>  
   			<?php }//?>
  		<?php }else{// End getValSouvenirif?>
  		<div class="text-center" style="background-color: #eee; padding:10px 20px"><h4>ไม่พบข้อมูล</h4></div>
  		<?php }?>
    </div>
  </div>
@endsection
@section('bottomScript')  
<script type="text/javascript">
jQuery(document).ready(function(){  
  
  });   
</script> 
@endsection 
 