<?php 
  use App\Http\Controllers\Frontend\ApiContreller;
  
  $birthday = date('Y-m-d', strtotime($chkTicket['birthday']));
  $pre_birthday =  date('d/m/Y', strtotime($birthday));
  $pre_type_event = 1;
  $iNumArr = 1;

?>@extends('backend.layout.popup') 
@section('titlepage', 'จัดการ '.  $textPaymentVIP) 
@section('topScript')
<link type="text/css" rel="stylesheet" href="{{ URL::asset('public/resources/css/materialize.min.css') }}"  media="screen,projection"/>
<style type="text/css">
.red-text{color: #f00}
  nav i, nav [class^="mdi-"], nav [class*="mdi-"], nav i.material-icons{ height:auto}
  label{font-size: 1em;}
  .breadcrumb {
    font-size: auto;
    color: rgba(255,255,255,0.7);
}.collapsible-header{ background-color:#ddd; padding:0.5em}
.input-field label, .input-field label.active{ font-size:1em; font-weight:normal}
.collapsible-body p{ padding:0px; font-size:1em; font-weight:normal}
[type="radio"]:not(:checked)+label, [type="radio"]:checked+label, [type="checkbox"]+label{ font-size:1em; font-weight:normal; padding-top:0}
.error{ color:#ff0000}
input:not([type]), input[type=text], input[type=password], input[type=email], input[type=url], input[type=time], input[type=date], input[type=datetime], input[type=datetime-local], input[type=tel], input[type=number], input[type=search], textarea.materialize-textarea{ font-size:1em}
.payment_page .modal{ background-color: transparent}
.payment_page .z-depth-4, .modal{box-shadow:0 16px 28px 0 rgba(0,0,0,0.0),0 25px 55px 0 rgba(0,0,0,0.0)}
h5{ font-size: 14px}
#divTicketDetail .row{ margin-bottom: 10px}
#divTicketDetail strong{ font-weight: bold;}
</style>
@endsection 
@section('content') 
<div class="panel panel-cascade">
         <br> 
    <div class="panel-body "> 
      <div class="ro">
         <p>แก้ไขตั๋ว order no</p> 
    <form action="#" method="post" enctype="multipart/form-data" name="basic_validate" class="form-horizontal cascade-forms" id="basic_validate" novalidate>
      <div class="collapsible-body" style="display:block"> 
          <div class="row">
            <div class="input-field col s6 l6">
              <select name="pre_name" id="pre_name" class="form-control">
                <option value="" disabled="" selected="">คำนำหน้า/Title *</option>
                <option value="ด.ช." <?php echo ( $chkTicket['pre_name']  == 'ด.ช.' )? "selected":"";?>>ด.ช./Master</option>
                <option value="ด.ญ." <?php echo ( $chkTicket['pre_name']  == 'ด.ญ.' )? "selected":"";?>>ด.ญ./Miss</option> 
                <option value="Mr." <?php echo ( $chkTicket['pre_name']  == 'Mr.' )? "selected":"";?>>นาย/Mr.</option>
                <option value="Mrs." <?php echo ( $chkTicket['pre_name']  == 'Mrs.' )? "selected":"";?>>นาง/Mrs.</option>
                <option value="Miss" <?php echo ( $chkTicket['pre_name']  == 'Miss' )? "selected":"";?>>นางสาว/Miss</option>
              </select>
            </div> 
          </div>
          <div class="row">
            <div class="input-field col s12 l6">
              <input type="text" class="validate eng_only" name="f_name" id="f_name"  value="<?php echo $chkTicket['f_name'] ?>">
                  <label for="f_name_1" class="active">ชื่อ/First Name *</label>
            </div>
            <div class="input-field col s12 l6">
              <input type="text" class="validate eng_only" name="l_name" id="l_name" value="<?php echo $chkTicket['l_name'] ?>">
                  <label for="l_name_1" class="active">นามสกุล/Last Name *</label>
            </div>
          </div>

          
          <div class="row">
            <div class="input-field col s12 l6">
              <input type="email" class="validate" name="email" id="email" value="<?php echo $chkTicket['email'] ?>">
                   <label for="email_1" class="active">อีเมล/Email</label>
            </div> 
            <div class="input-field col s12 l6">
              <input type="tel" class="validate" name="tel" id="tel" value="<?php echo $chkTicket['tel'] ?>">
                   <label for="tel_1" class="active">เบอร์มือถือ/Mobile No.</label>
            </div> 
          </div>
         
          <div class="row">
            <div class="input-field col s12 l6">
              <input type="tel" class="validate input_citizen_id" name="citizen_id" id="citizen_id" value="<?php echo $chkTicket['citizen_id'] ?>">
                    <label for="citizen_id_rs_1" class="active">เลขบัตรประชาชน/ID Card or Passport No. *</label>
                    <div class="citizen_id_rs_1 error-message-citizen"></div>
            </div>
            <div class="input-field col s6 l6">
              <select name="gender" id="gender" class="form-control">
                <option value="" disabled="" selected="">เพศ/Gender *</option>
                <option value="ชาย/Male" <?php echo ($chkTicket['gender']  == 'ชาย/Male')? "selected":"";?>>ชาย/Male</option>
                <option value="หญิง/Female" <?php echo ($chkTicket['gender']  == 'หญิง/Female')? "selected":"";?>>หญิง/Female</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12 l6">
              <input id="datepicker_1" type="text" class="datepicker_1 picker__input" name="birthday" value="<?php echo  $birthday?>" readonly="" tabindex="-1" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="datepicker_1_root"><div class="picker" id="datepicker_1_root" tabindex="0" aria-hidden="true"><div class="picker__holder"><div class="picker__frame"><div class="picker__wrap"><div class="picker__box"><div class="picker__date-display"><div class="picker__weekday-display">Monday</div><div class="picker__month-display"><div>May</div></div><div class="picker__day-display"><div>9</div></div><div class="picker__year-display"><div>2016</div></div></div><div class="picker__calendar-container"><div class="picker__header"><select class="picker__select--month browser-default" disabled="" aria-controls="datepicker_1_table" title="Select a month"><option value="0">January</option><option value="1">February</option><option value="2">March</option><option value="3">April</option><option value="4" selected="">May</option><option value="5" disabled="">June</option><option value="6" disabled="">July</option><option value="7" disabled="">August</option><option value="8" disabled="">September</option><option value="9" disabled="">October</option><option value="10" disabled="">November</option><option value="11" disabled="">December</option></select><select class="picker__select--year browser-default" disabled="" aria-controls="datepicker_1_table" title="Select a year"><option value="1926">1926</option><option value="1927">1927</option><option value="1928">1928</option><option value="1929">1929</option><option value="1930">1930</option><option value="1931">1931</option><option value="1932">1932</option><option value="1933">1933</option><option value="1934">1934</option><option value="1935">1935</option><option value="1936">1936</option><option value="1937">1937</option><option value="1938">1938</option><option value="1939">1939</option><option value="1940">1940</option><option value="1941">1941</option><option value="1942">1942</option><option value="1943">1943</option><option value="1944">1944</option><option value="1945">1945</option><option value="1946">1946</option><option value="1947">1947</option><option value="1948">1948</option><option value="1949">1949</option><option value="1950">1950</option><option value="1951">1951</option><option value="1952">1952</option><option value="1953">1953</option><option value="1954">1954</option><option value="1955">1955</option><option value="1956">1956</option><option value="1957">1957</option><option value="1958">1958</option><option value="1959">1959</option><option value="1960">1960</option><option value="1961">1961</option><option value="1962">1962</option><option value="1963">1963</option><option value="1964">1964</option><option value="1965">1965</option><option value="1966">1966</option><option value="1967">1967</option><option value="1968">1968</option><option value="1969">1969</option><option value="1970">1970</option><option value="1971">1971</option><option value="1972">1972</option><option value="1973">1973</option><option value="1974">1974</option><option value="1975">1975</option><option value="1976">1976</option><option value="1977">1977</option><option value="1978">1978</option><option value="1979">1979</option><option value="1980">1980</option><option value="1981">1981</option><option value="1982">1982</option><option value="1983">1983</option><option value="1984">1984</option><option value="1985">1985</option><option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016" selected="">2016</option></select><div class="picker__nav--prev" data-nav="-1" role="button" aria-controls="datepicker_1_table" title="Previous month"> </div><div class="picker__nav--next picker__nav--disabled" data-nav="1" role="button" aria-controls="datepicker_1_table" title="Next month"> </div></div><table class="picker__table" id="datepicker_1_table" role="grid" aria-controls="datepicker_1" aria-readonly="true"><thead><tr><th class="picker__weekday" scope="col" title="Sunday">S</th><th class="picker__weekday" scope="col" title="Monday">M</th><th class="picker__weekday" scope="col" title="Tuesday">T</th><th class="picker__weekday" scope="col" title="Wednesday">W</th><th class="picker__weekday" scope="col" title="Thursday">T</th><th class="picker__weekday" scope="col" title="Friday">F</th><th class="picker__weekday" scope="col" title="Saturday">S</th></tr></thead><tbody><tr><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1462035600000" role="gridcell" aria-label="2016-05-01">1</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1462122000000" role="gridcell" aria-label="2016-05-02">2</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1462208400000" role="gridcell" aria-label="2016-05-03">3</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1462294800000" role="gridcell" aria-label="2016-05-04">4</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1462381200000" role="gridcell" aria-label="2016-05-05">5</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1462467600000" role="gridcell" aria-label="2016-05-06">6</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1462554000000" role="gridcell" aria-label="2016-05-07">7</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1462640400000" role="gridcell" aria-label="2016-05-08">8</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--today picker__day--highlighted" data-pick="1462726800000" role="gridcell" aria-label="2016-05-09" aria-activedescendant="true">9</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1462813200000" role="gridcell" aria-label="2016-05-10" aria-disabled="true">10</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1462899600000" role="gridcell" aria-label="2016-05-11" aria-disabled="true">11</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1462986000000" role="gridcell" aria-label="2016-05-12" aria-disabled="true">12</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463072400000" role="gridcell" aria-label="2016-05-13" aria-disabled="true">13</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463158800000" role="gridcell" aria-label="2016-05-14" aria-disabled="true">14</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463245200000" role="gridcell" aria-label="2016-05-15" aria-disabled="true">15</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463331600000" role="gridcell" aria-label="2016-05-16" aria-disabled="true">16</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463418000000" role="gridcell" aria-label="2016-05-17" aria-disabled="true">17</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463504400000" role="gridcell" aria-label="2016-05-18" aria-disabled="true">18</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463590800000" role="gridcell" aria-label="2016-05-19" aria-disabled="true">19</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463677200000" role="gridcell" aria-label="2016-05-20" aria-disabled="true">20</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463763600000" role="gridcell" aria-label="2016-05-21" aria-disabled="true">21</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463850000000" role="gridcell" aria-label="2016-05-22" aria-disabled="true">22</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1463936400000" role="gridcell" aria-label="2016-05-23" aria-disabled="true">23</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1464022800000" role="gridcell" aria-label="2016-05-24" aria-disabled="true">24</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1464109200000" role="gridcell" aria-label="2016-05-25" aria-disabled="true">25</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1464195600000" role="gridcell" aria-label="2016-05-26" aria-disabled="true">26</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1464282000000" role="gridcell" aria-label="2016-05-27" aria-disabled="true">27</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1464368400000" role="gridcell" aria-label="2016-05-28" aria-disabled="true">28</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1464454800000" role="gridcell" aria-label="2016-05-29" aria-disabled="true">29</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1464541200000" role="gridcell" aria-label="2016-05-30" aria-disabled="true">30</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--disabled" data-pick="1464627600000" role="gridcell" aria-label="2016-05-31" aria-disabled="true">31</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1464714000000" role="gridcell" aria-label="2016-06-01" aria-disabled="true">1</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1464800400000" role="gridcell" aria-label="2016-06-02" aria-disabled="true">2</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1464886800000" role="gridcell" aria-label="2016-06-03" aria-disabled="true">3</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1464973200000" role="gridcell" aria-label="2016-06-04" aria-disabled="true">4</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1465059600000" role="gridcell" aria-label="2016-06-05" aria-disabled="true">5</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1465146000000" role="gridcell" aria-label="2016-06-06" aria-disabled="true">6</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1465232400000" role="gridcell" aria-label="2016-06-07" aria-disabled="true">7</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1465318800000" role="gridcell" aria-label="2016-06-08" aria-disabled="true">8</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1465405200000" role="gridcell" aria-label="2016-06-09" aria-disabled="true">9</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1465491600000" role="gridcell" aria-label="2016-06-10" aria-disabled="true">10</div></td><td role="presentation"><div class="picker__day picker__day--outfocus picker__day--disabled" data-pick="1465578000000" role="gridcell" aria-label="2016-06-11" aria-disabled="true">11</div></td></tr></tbody></table></div><div class="picker__footer"><button class="btn-flat picker__today" type="button" data-pick="1462726800000" disabled="" aria-controls="datepicker_1">Today</button><button class="btn-flat picker__clear" type="button" data-clear="1" disabled="" aria-controls="datepicker_1">Clear</button><button class="btn-flat picker__close" type="button" data-close="true" disabled="" aria-controls="datepicker_1">Close</button></div></div></div></div></div></div>
                <label for="datepicker_1" class="active truncate">วันเดือนปีเกิด/Date of Birth *</label>
            </div>
            <div class="input-field col s6 l6">
              <div class="row">
                <div class="col l12">
                       <h5 class="black-text"><span class="member_age"><?php  echo   ApiContreller::getAge($birthday); ?></span> ปี/Years</h5>
                      <label for="age" class="active">อายุ/Age</label>
                      </div>
              </div>
            </div>
          </div><div class="row">
            <div class="input-field col s6 l6"> 
                <input type="text" class="validate" name="nationality" value="<?php echo $chkTicket['nationality'] ?>" id="nationality">
                            <label for="nationality" class="active">สัญชาติ/Nationality *</label>
              </div>
          </div> 
          <div class="row">@foreach($Souvenirs as $rowCateSouvenir)
       <?php   $souvenir_name = json_decode($rowCateSouvenir->souvenir_name, true); 
               $souvenir_name =  $souvenir_name['data']['lang_th'];
               $souvenir_pic =  $rowCateSouvenir->souvenir_pic;
        ?>
        <div class="col l4">
          <h5><strong><?php echo $souvenir_name;?></strong></h5>
          <p  class="pre_radio ">
            <input class="with-gap required"  type="radio" id="size-<?php echo $pre_type_event;?>-9999"  name="souvenir_type_id[{{$iNumArr}}]" value=""   />
          </p>
          <?php $TypeSouvenir = ApiContreller::getSouvenir($event_id, $rowCateSouvenir->souvenir_id);?>
          @foreach($TypeSouvenir as $rowTypeSouvenir) 
          <?php  $souvenir_type_name  = json_decode($rowTypeSouvenir->souvenir_type_name, true); 
                 $souvenir_type_name  = $souvenir_type_name['data']['lang_th']; 
                 $souvenir_type_id    = $rowTypeSouvenir->souvenir_type_id;
                 $souvenir_type_stock = $rowTypeSouvenir->souvenir_type_stock;  

           //===================================  Start Souvenir ===================================
               $souvenirSuccess  = ApiContreller::getFuncSouvenirReport($souvenir_type_id , 'SUCCESS' );
               $souvenirWait   = ApiContreller::getFuncSouvenirReport($souvenir_type_id , 'WAIT' );
               $souvenirSucc_Wait = $souvenirSuccess+$souvenirWait;
               $souvenirTotal = ($souvenir_type_stock-$souvenirSucc_Wait);

           //===================================  End Souvenir ===================================
              if($souvenirTotal >= 1 ){
           ?><p>
             <input class="with-gap required"  type="radio" id="size-<?php echo $pre_type_event;?>-<?php echo $souvenir_type_id;?>"  name="souvenir_type_id" value="<?php echo $souvenir_type_id;?>" <?php if($chkTicket['type_souvenir_id'] == $souvenir_type_id ){?>checked <?php }?>/>
             <label for="size-<?php echo $pre_type_event;?>-<?php echo $souvenir_type_id;?>"><?php echo $souvenir_type_name;?></label>
          </p><?php }else{?><p>
           <input class="with-gap"   type="radio" id="size-s" disabled="disabled"  />
           <label for="size-s"><?php echo $souvenir_type_name;?><span class="red-text">หมดแล้ว/SOLD OUT</span></label>
         </p><?php }?>
          @endforeach 
        </div> 
      @endforeach 
       </div>
        <div class="form-group">
              
              <div class="col-lg-11 col-md-10 text-center">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" id="_token">
                <input type="submit" value="บันทึก" class="btn bg-primary text-white btn-lg" >
                <input name="action" value="บันทึก | Save"  type="hidden" >
              <input type="hidden" name="ticket_id" value="<?php echo $ticket_id ?>" /> 
              <input type="hidden" name="event_id" value="<?php echo $event_id ?>" />  
              </div>
            </div>
      </div></form>
      </div> 
    </div>
  </div>
@endsection
@section('bottomScript')
<script type="text/javascript" src="{{ URL::asset('public/resources/js/materialize.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/jquery.validate.min.js') }}"></script>

<script type="text/javascript">
 function letterAdd(){ 
            jQuery.validator.addMethod("lettersonly", function(value, element){
              return this.optional(element) || /^[a-z," "]+$/i.test(value);
            }, "กรุณากรอกเป็นภาษาอังกฤษ/Please enter your information in English");  
       }
       letterAdd();

       jQuery('.picker__input').pickadate({
              closeOnSelect: true,
                selectMonths: true,
                format: 'yyyy-mm-dd' ,
                max: true,
                selectYears: 90,
                onOpen: function () {
                   this.clear();
                    },
                onSet: function(arg) {
                   //======
                  var dayBirth = $('.picker__input').val(); 
                    var get_age=dayBirth.split('-'); 
                  var d=new Date();
                  var n=d.getFullYear(); 
                  var age=n-get_age[0]; 
                  $('.member_age').text(age); 
                  if ( 'select' in arg ){  
                         this.close();
                    } 
                //======= 
                } 
        });

     jQuery(".eng_only").keypress(function(event){
        var ew = event.which;
        if(ew == 32)
            return true;
        if(48 <= ew && ew <= 57)
            return true;
        if(65 <= ew && ew <= 90)
            return true;
        if(97 <= ew && ew <= 122)
            return true;
        return false;
    });
 
</script>
@endsection 
 