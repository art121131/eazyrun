<?php
  use App\Http\Controllers\Frontend\ApiContreller;

  $user = \Auth::user();
  $user_level = $user->level;

  $inputForm = 0;
  $lang = 'th';
  $ticketStatusSelector = 'OPEN';

  $menuMessages = ApiContreller::getMenuMeaasge();

   foreach ($menuMessages as $menuMessage)
   {
      $textDetail = json_decode($menuMessage->text_menu_detail, true);
      $textMenuId =  $menuMessage->text_menu_id;

      if($textMenuId == 13){
        $textTicketType =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 24){
        $btnContinue =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 25){
        $txtErrorTicketNull =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 52){
        $textOrganizerBy  =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 13){
      $textTicketType =  $textDetail['data'][$lang] ;
     }
      if($textMenuId == 19){
        $textQty =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 20){
        $textProductOther =  $textDetail['data'][$lang] ;
      }
       if($textMenuId == 28){
        $textShippingName =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 29){
        $textFeeName =  $textDetail['data'][$lang] ;
      }
       if($textMenuId == 30){
        $textPickup =  $textDetail['data'][$lang] ;
      }
          if($textMenuId == 31){
        $textHomeDelivery =  $textDetail['data'][$lang] ;
      }
       if($textMenuId == 39){
        $textOrderSummary =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 40){
        $textSubtotal =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 41){
        $textPaynow =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 42){
        $textFeeDelivery =  $textDetail['data'][$lang] ;
      }
      if($textMenuId == 43){
        $textTotal =  $textDetail['data'][$lang] ;
      }
       if($textMenuId == 44){
        $textTransactionFee =  $textDetail['data'][$lang] ;
      }
       if($textMenuId == 52){
        $textOrganizerBy  =  $textDetail['data'][$lang] ;
      }
       if($textMenuId == 35){
        $textFree =  $textDetail['data'][$lang] ;
      }
  }

  $ticketSelectLimit = $events->event_ticket_select_limit;
  $eventID  = $events->event_id;
  $eventTypeDelivery = $events->event_type_delivery;

?>@extends('backend.layout.master')
@section('titlepage',  $menuWalkin)
@section('topScript')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.min.css">
 <link  href="{{URL::asset('public/resources/css/datepicker.css') }}" rel="stylesheet">
<style type="text/css">
 .input-group-btn{ width: 25px}
 .popover-markup .popover{ width:150%;}
.popover-markup label{padding:0px;}
.container{ margin:0px auto;}
.radio-hide input{ position: absolute; left: -20px;bottom: -20px; height: 0;  }
.box-tickets-info  .card, .box-product-info  .card{ overflow: hidden;}
.btn-outline-primary{ cursor: pointer;}
#toast-container{ top:60px}
.btn-outline-primary.active, .btn-outline-primary:active, .show>.btn-outline-primary.dropdown-toggle {
    color: #fff;
    background-color: #007bff;
    border-color: #007bff;
}
.btn.active, .btn:active {
    background-image: none;
}
.box-tickets-info .card {overflow: hidden;}
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.btn-outline-primary {
    cursor: pointer;
}
 .inputForm, .inputFormProduct{
    width: 100px!important;
    border: none;
    background-color: #fff!important;

 }
  .pointer{ cursor: pointer;}
</style>
 @endsection
@section('content')
  <div class="row">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-body">
                <form method="post" name="basic_validate" class="form-horizontal cascade-forms" id="ticketMyList" autocomplete="off">
               <div class="row">
                  <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="card p-3 mb-3">
                      <table class="table no-border-top">
                        <thead>
                          <tr>
                            <th width="70%" class="th-ticket-form">{{$textTicketType}}</th>
                            <th class="text-center">{{$textQty}}</th>
                          </tr>
                        </thead>
                        <tbody id="box_ticket">
                          @foreach($tickets as $rowTicket)
                          <?php
                           $ticketDisplay = true;
                           $ticketStatusSoldout = false;

                           @$ticketName    = json_decode($rowTicket->ticket_title, true);
                           @$ticketDes    = json_decode($rowTicket->ticket_des, true);

                           $ticketName = $ticketName['data'][$lang];
                           $ticketDes = $ticketDes['data'][$lang];

                            $priceTicket = $rowTicket->ticket_price;
                            $ticketID = $rowTicket->ticket_id;

                            $ticket_stock = $rowTicket->ticket_stock;
                            $ticket_display = $rowTicket->ticket_display;

                             //============== Start Count Ticket ===============//
                              $ticketSuccess        =  ApiContreller::getFuncStockReport($ticketID, 'SUCCESS' ,$eventID  );
                              $ticketWait       =  ApiContreller::getFuncStockReport($ticketID, 'WAIT'  ,$eventID );
                              $ticketAddTicket      =  ApiContreller::getFuncStockReport($ticketID, 'ADDTICKET'  ,$eventID );
                              $ticketSuccWaitAddticket = $ticketSuccess+$ticketWait+$ticketAddTicket;
                              $ticketTotal = ($ticket_stock-$ticketSuccWaitAddticket);
                            //============== Start Count Ticket ===============//

                             $ticket_date_start_on = $rowTicket->ticket_date_start_on;
                             $ticket_date_end_on   = $rowTicket->ticket_date_end_on;
                             //======= chk ticket_display//
                            if(($ticket_display == 2) && ($dateNow >= $ticket_date_start_on) && ($dateNow <= $ticket_date_end_on)){
                               $ticketDisplay = true;

                            }else if(($ticket_display == 2 && $dateNow < $ticket_date_start_on ) || ($ticket_display == 2 && $dateNow > $ticket_date_end_on ) ){
                               $ticketDisplay = false;
                            }

                             if($dateNow >= $ticket_date_start_on && $dateNow <= $ticket_date_end_on && $ticketTotal >= 1 ){// End if chk date
                                $ticketStatusSelector = 'OPEN';
                             }else if($dateNow < $ticket_date_start_on && $dateNow < $ticket_date_end_on  ){// End if chk date
                                $ticketStatusSelector = 'COMINGSOON';
                             }else  if($dateNow > $ticket_date_end_on ||  $ticketTotal < 1 ){// End if chk date
                                 $ticketStatusSoldout = true;
                                 $ticketStatusSelector = 'SOLDOUT';
                             }
                           //====================


                             $limitSelector = $ticketSelectLimit;
                             if($ticketTotal <= $ticketSelectLimit){
                                 $limitSelector = $ticketTotal;
                             }// Check Selector
                              if($ticketDisplay == true){

                               // echo $limitSelector;
                          ?>
                          <tr>
                            <td><span class="text-secondary">{{$ticketName}}</span><br>
                             @if($ticketDes != '')<span class="text-secondary txt-14">{{$ticketDes}}</span><br> @endif
                              <span class="text-danger txt-14 ">฿<?php echo number_format($priceTicket, 2, '.', ',');?></span>
                            </td>
                            <td class="pr-0 pl-0 text-center">
                              <?php if( $ticketStatusSelector == 'OPEN' ){?>
                               <div class="form-group">
                                   <div class="input-group number-spinner" >
                                        <span class="input-group-btn">
                                            <a  data-dir="dwn" id="dw_{{$ticketID}}" class="pointer"><img src="{{ URL::asset('public/resources') }}/images/btn_down.svg" height="30"></a>
                                        </span>
                                        <input type="text" disabled name="ticketSelect[]" id="ticketSelect" class="form-control text-center inputForm" value="0" max="{{$limitSelector}}" min=0>
                                        <span class="input-group-btn">
                                            <a  data-dir="up" id="up_{{$ticketID}}" class="pointer"><img src="{{ URL::asset('public/resources') }}/images/btn_up.svg" height="30"></a>
                                        </span>
                                    </div>
                                </div>
                                <?php }else if( $ticketStatusSelector == 'SOLDOUT' ){?>
                                 <button type="button" class="btn btn-danger">SOLD OUT</button>
                                <?php }else if($ticketStatusSelector == 'COMINGSOON'  ){?>
                                 <button type="button" class="btn btn-warning text-white">COMING SOON</button>
                                <?php }?>
                            </td>
                          </tr>
                          <input name="ticket_id[]" type="hidden" value="<?php echo $ticketID?>">
                          <input name="ticket_price[]" id="price{{$ticketID}}" type="hidden" value="<?php echo $priceTicket?>">
                          <input name="ticket_delivery_price[]" type="hidden" value="<?php echo $rowTicket->ticket_delivery_price?>">
                          <input name="ticket_num[]" id="ticketID{{$ticketID}}" class="inputNum" type="hidden" value="0">
                          <input name="ticket_name[]" type="hidden" value="<?php echo $ticketName?>">
                          <?php }//End ticketDisplay?>
                         @endforeach
                        </tbody>
                      </table>
                    </div>
                    @if($countProducts > 0)<br>
                    <div class="card p-3 mb-3">
                      <table class="table no-border-top">
                        <thead>
                          <tr>
                            <th width="70%" class="th-ticket-form">{{$textProductOther}}</th>
                            <th class="text-center">{{$textQty}}</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($products as $rowProduct)
                          <?php
                           @$productName      = json_decode($rowProduct->product_name, true);

                           $productName     = $productName['data'][$lang];
                           $productPrice    = $rowProduct->product_price;

                           @$productDes    = json_decode($rowProduct->product_detail, true);
                            $productDes = $productDes['data'][$lang];

                           $productID = $rowProduct->product_id;
                          ?>
                          <tr>
                            <td><span class="text-secondary">{{$productName}}</span><br>
                               @if($productDes != '')<span class="text-secondary txt-14">{{$productDes}}</span><br> @endif
                              <span class="text-danger txt-14">฿<?php echo number_format($productPrice, 2, '.', ',');  ?></span>
                            </td>
                             <td class="pr-0 pl-0">
                                 <div class="form-group">
                                   <div class="input-group product-number-spinner">
                                        <span class="input-group-btn">
                                            <a data-dir="dwn" id="pdw_{{$productID}}" class="pointer"><img src="{{ URL::asset('public/resources') }}/images/btn_down.svg" height="30"></a>
                                        </span>
                                        <input type="text" disabled name="productSelect[]" id="productSelect" class="form-control text-center inputFormProduct required" value="{{$inputForm}}" max=9 min=0>
                                        <span class="input-group-btn">
                                            <a data-dir="up" id="pup_{{$productID}}" class="pointer"><img src="{{ URL::asset('public/resources') }}/images/btn_up.svg" height="30"></a>
                                        </span>
                                    </div>
                                </div>
                                <input name="product_id[]" type="hidden" value="<?php echo $rowProduct->product_id?>">
                                <input name="product_name[]" type="hidden" value="<?php echo $productName?>">
                                <input name="product_price[]" type="hidden" value="<?php echo $productPrice?>">
                                <input name="product_num[]" id="productID{{$productID}}" type="hidden" value="0">
                            </td>
                          </tr>
                           @endforeach
                        </tbody>
                      </table>
                    </div>
                    @endif
                    <br>
                  </div>
                  <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="card ">
                      <table class="table no-border-top">
                        <thead>
                          <tr>
                            <th class="w-50">{{$textShippingName}}</th>
                            <th class="text-right"></th>
                          </tr>
                        </thead>
                        <tbody class="box-payment-option-delivery">
                          @if($eventTypeDelivery != 3)
                          <tr>
                            <td>
                              <label class="form-check-label">
                                <input class="form-check-input required" type="radio" name="event_type_delivery" id="event_type_delivery1" value="NOSEND"  @if($eventTypeDelivery == 2) checked @endif >
                             &nbsp;    <span class="text-secondary">{{$textPickup}}</span>
                              </label>
                            </td>
                            <td class="text-right"></td>
                          </tr>
                          @endif
                           @if($eventTypeDelivery != 2)
                         <tr>
                            <td >
                              <label class="form-check-label">
                                <input class="form-check-input required" type="radio" name="event_type_delivery" id="event_type_delivery2" value="SEND"  @if($eventTypeDelivery != 1) checked @endif>
                             &nbsp;   <span class="text-secondary">{{$textHomeDelivery}}</span>
                              </label>
                            </td>
                            <td class="text-right text-danger"> </td>
                          </tr>
                           @endif
                        </tbody>
                      </table>
                      </div>
                      <br/>
                  </div>
                  <div class="col-lg-6 col-md-12 col-sm-12">
                      <div class="card">
                          <table class="table no-border-top">
                            <thead>
                              <tr>
                                <th class="w-50">วิธีชำระเงิน</th>
                              </tr>
                            </thead>
                            <tbody class="box-payment-option-delivery">
                                 <tr>
                                     <td>
                                         <label class="form-check-label">
                                            <input class="form-check-input required" type="radio" name="psb_channel" id="psb_channel1" value="CASH" checked>
                                                &nbsp;  <span class="text-secondary">เงินสด</span>
                                          </label><br/>
                                         <label class="form-check-label">
                                            <input class="form-check-input required" type="radio" name="psb_channel" id="psb_channel2" value="CREDIT">
                                                &nbsp;  <span class="text-secondary">บัตรเครดิต</span>
                                          </label>
                                          <br/>
                                         <label class="form-check-label">
                                            <input class="form-check-input required" type="radio" name="psb_channel" id="psb_channel3" value="BARCODE">
                                                &nbsp;  <span class="text-secondary">บาร์โค้ด</span>
                                          </label><br/>
                                          <div class="row">
                                          <label class="form-check-label col-md-3 col-xl-2">
                                            <input class="form-check-input required" type="radio" name="psb_channel" id="psb_channel4" value="OTHER">
                                                &nbsp;  <span class="text-secondary">อื่นๆ</span>
                                          </label>
                                              <div class="col-md-8 col-xl-8">
                                                  <input type="text" name="order_remark" id="psb_other" class="form-control" disabled="true">
                                              </div>
                                          </div>
                                     </td>
                                 </tr>
                            </tbody>
                          </table>
                      </div>
                      <br/>
                     </div>
                  <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="card">
                      <table class="table no-border-top table-payment">
                        <thead>
                          <tr>
                            <th class="w-50">{{$textOrderSummary}}</th>
                            <th class="text-center">{{$textQty}}</th>
                            <th class="text-right">{{$textSubtotal}}</th>
                          </tr>
                        </thead>
                        <tbody>
                           <tr>
                            <td>
                              <span class="text-secondary">{{$textSubtotal}}</span>
                            </td>
                            <td colspan="2" class="text-right"><span class="text-danger" id="orderPriceText">฿0</span>
                              <input name="orderPrice" value="0" id="orderPrice" type="hidden">
                            </td>
                          </tr>
                            <tr>
                            <td>
                              <span class="text-secondary">{{$textFeeDelivery}}</span>
                            </td>
                            <td colspan="2" class="text-right text-danger"><span class="txt-order-price-delivery">0</span>
                               <input name="orderDelivery" value="0" type="hidden" id="orderDelivery">
                               <input name="optionDeliveryPrice" value="0" type="hidden" id="optionDeliveryPrice">
                             </td>
                          </tr>
                            <tr>
                            <td>
                              <span class="text-secondary">{{$textTransactionFee}}</span>
                            </td>
                            <td colspan="2" class="text-right text-danger"><span class="txt-order-price-fee">0</span>
                              <input name="orderFee" value="0" type="hidden" id="orderFee">
                            </td>
                          </tr>
                            <tr>
                            <td>
                              <h4>{{$textTotal}}</h4>
                            </td>
                            <td colspan="2" class="text-right"><span class="txt-18 text-black txt-order-price-net font-weight-bold" id="orderPriceNetText">0</span>
                               <input name="orderPriceNet" value="0" type="hidden" id="orderPriceNet">
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row" style="padding:20px">
                  <div class="box-tickets-info"></div>
                  <div class="box-product-info"></div>
                </div>
                <input type="hidden" name="event_id" value="{{$eventID}}" id="event_id">
               <div id="div__content"> </div>
               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" id="_token">
               <div class="fixed-bottom bg-white p-2 bg-shadow text-center"><button type="submit" id="btnNext" class="btn bg-orange btn-lg text-white btn-payment pointer">{{$btnContinue}} <img src="{{ URL::asset('public/resources') }}/images/ic_next_white.svg" height="30"></button></div>
             </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('bottomScript')
<script type="text/javascript" src="{{ URL::asset('public/resources/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ URL::asset('public/resources/js/bof/jquery-ui-1.10.3.custom.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/datepicker.th-TH.js') }}"></script>
<script type="text/javascript">
   toastr.options = {
    "closeButton": false,"debug": false,"newestOnTop": false, "progressBar": false,"positionClass": "toast-top-center", "preventDuplicates": false, "onclick": null, "showDuration": "300", "hideDuration": "1000", "timeOut": "1000", "extendedTimeOut": "1000", "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"
  }
  $(function () {
     function chk_engonly(){
      jQuery(".eng_only").keypress(function(event){
            var ew = event.which;
            if(ew == 32)
                return true;
            if(48 <= ew && ew <= 57)
                return true;
            if(65 <= ew && ew <= 90)
                return true;
            if(97 <= ew && ew <= 122)
                return true;
            return false;
        });
    }
    function fnToast(){
      toastr.options = {
          "closeButton": false,"debug": false,"newestOnTop": false, "progressBar": false,"positionClass": "toast-top-center", "preventDuplicates": false, "onclick": null, "showDuration": "300", "hideDuration": "3000", "timeOut": "4000", "extendedTimeOut": "1000", "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"
        }

    }
     function chkBiB(){

        jQuery(".bib").blur(function(){

          var varBib = $(this).val();
          var divBib  = $(this).attr('id');
           jQuery.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
               url : '{{URL::to('tickets/check-bib' )}}',
              type:"POST",
              cache: false,
              data : "varBib="+varBib+"&eventID="+{{$eventID}},
              success :function(data){
                  if(data == 'NO'){
                      toastr.warning( 'BIB ซ้ำไม่สามารถทำการเพิ่มได้')
                      $('#'+divBib).val('');
                  }
               }
            });
         });
     }//

     $(document).on('change', 'input[name=psb_channel]', function (e) {
         if(e.target.value == 'OTHER'){
             $('#psb_other').attr('disabled',false);
         } else {
             $('#psb_other').attr('disabled',true);
         }
     });
     function calPrice(){
        var totalPrice = 0
        jQuery('.inputNum').each(function() {
            var id = $(this).attr('id').substr(8)
            var amount = $(this).val()
            var price = $('#price'+id).val()
            totalPrice += parseFloat(amount) * parseFloat(price)
        });
        jQuery('#orderPrice').val(totalPrice);
        jQuery("#orderPriceText").text("฿"+totalPrice);

        jQuery('#orderPriceNet').val(totalPrice);
        jQuery("#orderPriceNetText").text("฿"+totalPrice);
     }
    btnSubmit();
    function btnSubmit(){
          var statusValidate = true;

          jQuery("#btnNext").click(function(){
            var iNum = 1;
            jQuery("#ticketMyList").validate({
              errorElement : 'div',
              errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {

                  $(placement).append(error)
                } else {
                  error.insertAfter(element);

                }
              },
              submitHandler: function(form) {
                var totalSum = 0;
                jQuery('.inputForm').each(function() {
                    if($(this).val()!="") {
                        totalSum += parseFloat($(this).val());
                     }
                 });
                if(totalSum == '' && totalSum == 0 && productTotalPlus == 0){
                  fnToast()
                  toastr.warning( '{{$txtErrorTicketNull}}')
                  return false;
                }else{
                    $(window).unbind("beforeunload");
                    //form.submit();
                    jQuery("#btnNext").attr("disabled", "disabled");
                    jQuery.ajax({
                            headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                           },
                             url : '{{URL::to('backoffice_management/walkin/save-order' )}}',
                            type:"POST",
                            cache: false,
                            data : $('#ticketMyList').serialize(),
                            success :function(data){
                                if(data.status == 'ok'){
                                    $(".box-tickets-info").empty();
                                    $('#psb_other').val("");
                                    $("#psb_channel1").prop('checked', true);
                                    $('#psb_other').attr('disabled',true);
                                    jQuery('.inputNum').each(function() {
                                        jQuery(this).val(0);
                                    });
                                    jQuery('.inputForm').each(function() {
                                        jQuery(this).val(0);
                                    });
                                    calPrice()
                                    toastr.success( 'Save Success')
                                } else {
                                    toastr.warning( 'Save Fail')
                                }
                                jQuery("#btnNext").removeAttr("disabled");
                             }
                          });
                }
              },
               invalidHandler: function(event, validator) {
                   var errors = validator.numberOfInvalids();
                   if (errors) {
                        toastr.warning( 'กรุณาระบุ/This field is required.')
                    }
                }
             });
       });

    }//btnSubmit


   //===================
    var totalPlus = 0;
    var totalAll = 0;
    // spinner(+-btn to change value) & total to parent input
    $(document).on('click', '.number-spinner a', function () {

       var total = 0;

          jQuery('.inputForm').each(function() {
              if($(this).val()!="") {
                  total += parseFloat($(this).val());
               }
           });

        var btn = $(this),
        input = btn.closest('.number-spinner').find('input'),
        oldValue = input.val().trim();

        var boxID = jQuery(this).attr('id');
        var ticketBoxID = boxID.substr(3);

    if (btn.attr('data-dir') == 'up') {
      if(total < input.attr('max') ){//if(oldValue < {{$ticketSelectLimit}}){
        oldValue++; //self total
        total++;  //total all
        totalPlus++; //last index
        totalAll++; //total all

        //======================= Add Ticket Start =======================
        if(totalAll == 1){
         // $('.box-tickets-info').addClass('col-md-6');
        }
        toastr.success( 'Added')

         jQuery.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
           url : '{{URL::to('backoffice_management/walkin/add-ticket' )}}',
          type:"POST",
          cache: false,
          data : "totalPlus="+totalPlus+"&ticketBoxID="+ticketBoxID+"&oldValue="+oldValue+"&ticketNumber="+total+"&lang="+'{{$lang}}'+"&eventID="+{{$eventID}},
          success :function(data){

            jQuery('.box-tickets-info').append(data);
              chk_engonly();
              chkBiB();
           }
        });
        //======================= Add Ticket End  =======================
      }
    } else {
      if (total > input.attr('min') && oldValue != 0) { //if (oldValue > input.attr('min')) {

        jQuery('#ticket_'+ticketBoxID+'_'+oldValue).remove();
         toastr.warning( 'Removed')
         oldValue--;
          total--;
          totalAll--;
        //======================= Remove Ticket Start =======================
        if(totalAll == 0){
           $('.box-tickets-info').removeClass('col-md-6');
        }
        //======================= Remove Ticket End  =======================
      }
    }
   // console.log(oldValue)

      $('#ticketID'+ticketBoxID).val(oldValue);
      input.val(oldValue);
      calPrice();
  });


// Click Product===========
  var productTotalPlus = 0;

 $(document).on('click', '.product-number-spinner a', function () {
        var btn = $(this),
        input = btn.closest('.product-number-spinner').find('input'),
        oldValue = input.val().trim();

        var boxID = jQuery(this).attr('id');
        var productID = boxID.substr(4);

    if (btn.attr('data-dir') == 'up') {
      if(oldValue <  {{$ticketSelectLimit}}){
        oldValue++;
        productTotalPlus++;
        totalAll++;
        if(totalAll == 1){
         // $('.box-tickets-info').addClass('col-md-6');
        }
        toastr.success( 'Added')
        //=================
        jQuery.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
           url : '{{URL::to('tickets/add-product' )}}',
          type:"POST",
          cache: false,
          data : "productTotalPlus="+productTotalPlus+"&productID="+productID+"&oldValue="+oldValue+"&lang="+'{{$lang}}'+"&eventID="+{{$eventID}},
          success :function(data){
            jQuery('.box-product-info').append(data);

           }
        });
        //=================
      }
    } else {
      if (oldValue > input.attr('min')) {

        jQuery('#product_'+productID+'_'+oldValue).remove();
         toastr.warning( 'Removed')
        oldValue--;
        productTotalPlus--;
        totalAll--;
         if(totalAll == 0){
           $('.box-product-info').removeClass('col-md-6');
        }
      }
    }

    $('#productID'+productID).val(oldValue);
    //#productID+id
    input.val(oldValue);
    console.log(oldValue)
  });
  // Click Product===========
   // console.log(totalAll)
   //===================
    fnOrderCalPrice();

     function fnOrderCalPrice(){
      var valueOrderPrice     = parseFloat(jQuery('#orderPrice').val());

      //============ Cal Delivery ===============//
       var optionTypeDelivery = $('.box-payment-option-delivery input:radio:checked').val();

       var optionDeliveryPrice  = parseFloat(jQuery('#optionDeliveryPrice').val());

        valueOrderDelivery = 0;
       if(optionTypeDelivery == 'SEND'){

           jQuery('#orderDelivery').val(optionDeliveryPrice);
            valueOrderDelivery  = optionDeliveryPrice ;

             jQuery('.txt-order-price-delivery').text('฿'+optionDeliveryPrice.toFixed(2));
        }else if(optionTypeDelivery == 'NOSEND'){
            jQuery('#orderDelivery').val(0);
            jQuery('.txt-order-price-delivery').text('{{$textFree}}');
            valueOrderDelivery = 0;
        }
         jQuery('.txt-price-delivery').text('฿'+optionDeliveryPrice.toFixed(2));
      //============ Cal Delivery ===============//


      //============ Cal Fee ===============//
      var ratePayPal          = {{$events->event_rate_paypal}};
      var ratePaysbuy         = {{$events->event_rate_psb}};

      var priceOrderPaypal   =  (((valueOrderPrice+valueOrderDelivery)*ratePayPal)/100);
      var priceOrderPsb      =  (((valueOrderPrice+valueOrderDelivery)*ratePaysbuy)/100);
      //num.toFixed(2)
      jQuery('.text-payment-paypal').text('฿'+priceOrderPaypal.toFixed(2))
      jQuery('.text-payment-psb').text('฿'+priceOrderPsb.toFixed(2))

      var optionTypePayment = $('.box-payment-option-fee input:radio:checked').val();
      var orderPriceFee = 0;

      if(optionTypePayment == 'paypal'){
          orderPriceFee = priceOrderPaypal;
      }else if(optionTypePayment == 'psb'){
          orderPriceFee = priceOrderPsb;
      }

       jQuery('.txt-order-price-fee').text('฿'+orderPriceFee.toFixed(2))
       parseFloat(jQuery('#orderFee').val(orderPriceFee));
      //============ Cal Fee ===============//

      //============ Cal Order Price All ===============//

      var sumOrderPrice = parseFloat(valueOrderPrice+valueOrderDelivery+orderPriceFee);
      jQuery('#orderPriceNet').val(sumOrderPrice);
       jQuery('.txt-order-price-net').text('฿'+Number(sumOrderPrice.toFixed(2)).toLocaleString('en'))
      //============ Cal Order Price All ===============//


   }//fnOrderCalPrice
});

   //==========
 </script>
@endsection
