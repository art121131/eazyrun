<?php 
  use App\Http\Controllers\Frontend\ApiContreller;

    $totalAmount = '';
    $totalVat = '';
    $totalAll = '';
    $iNum = 1;

    $event_id = $event_id ;

   $user = \Auth::user();
   $user_level = $user->level; 
  
?>@extends('backend.layout.master') 
@section('titlepage', 'ยอดขายแยกตามช่องทาง') 
@section('topScript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.min.css">
<style>
    .form-inline label{
            margin-bottom: 15px;
        }
    @media (min-width: 768px){
        .form-inline .form-group {
            display: inline-block;
            margin-bottom: 15px;
            width: 100%;
            vertical-align: middle;
        }
        .form-inline .form-control {
            display: inline-block;
            width: 100%;
        }
        .form-inline label{
            margin-bottom: 15px;
        }
    }

    @media (min-width: 1055px){
        .form-inline .form-group {
            display: inline-block;
            margin-bottom: 0;
            width: auto;
            vertical-align: middle;
        }
        .form-inline label{
            padding-right: 10px;
        }
    }
</style>
@endsection 
@section('content') 
 <div class="col-md-12">
          <div class="panel"> 
            <div class="panel-body"> 
                <div class="form-group">

                    <table width="100%">
                      <tbody><tr>
                        <td width="30%"> </td>
                        <td width="70%"> <div class="text-right" style="padding-top:5px"><form class="form-inline"  method="get" style="margin-right:15px" autocomplete="off">
                            <div class="form-group">
                                <label>กรองผลลัพท์ </label> 
                                   <?php
                                  #==================================== Start !=  staff ===================================
                                   /*if($user_level != 'Staff'){  ?> 
                                   <div class="form-group">
                                <select name="event_id" class="form-control" method="" >

                                      <option value="" disabled>--เลือก{{$menuEventName}}--</option> 
                                      <?php
                                         /*$rowEvents = ApiContreller::getEventLists();
                                         foreach ($rowEvents as $rowEvent) {
                                            $event_name    = json_decode($rowEvent->event_name, true);
                                            $event_name    = $event_name['data']['th'];
                                          ?>
                                      <option value="{{$rowEvent->event_id}}" <?php echo ($event_id == $rowEvent->event_id)? "selected": "";?>  >{{ApiContreller::strCrop($event_name, 22)}}</option>
                                      <?php }?>
                                  </select>
                                   </div>
                              <?php }#==================================== End !=  staff =================================== */?>
                                    <div class="form-group">
                                    <label class="sr-only" for="dateStart">วันที่</label>
                                    <input type="text" class="form-control" id="dateStart" placeholder="จากวันที่" name="dateStart" value="<?php echo $dateStart?>">
                                  </div> 
                                  <div class="form-group">
                                    <label class="sr-only" for="dateEnd">วันที่</label>
                                    <input type="text" class="form-control" id="dateEnd" placeholder="ถึงวันที่" name="dateEnd" value="<?php echo $dateEnd?>">
                                  </div> 
                                <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>ค้นหา</button> 
                                <a href="{{URL::to('backoffice_management/order/financial-classify' )}}">ดูทั้งหมด </a>
                                </div>
                              </form>  
                            </div></td>
                            </tr>
                          </tbody>
                        </table>  
                          <br></div>
                         <div class="form-group"> 
                            <div class="col-md-12">
                              <div class="panel-cascade"> 
                                <div class="row"> 
                                <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th style="width:50%">ประเภทบัตร</th> 
                                    <th>จำนวน(ขายแล้ว)</th>
                                    <th>จำนวนเงิน</th>  
                                </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                    $AllTotalCount = 0; 
                                    $AllTotalPrice = 0; 
                               

                                    $rowTickets = ApiContreller::getTicketTypeLists($event_id);
                                    foreach ($rowTickets as $rowTicket) {
                                        
                                      $ticket_name    = json_decode($rowTicket->ticket_title, true); 
                                      $ticket_name = $ticket_name['data']['th']; 
                                      $ticketID = $rowTicket->ticket_id;
 
                                       $ticketSuccess =  ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' , $dateStart  , $dateEnd );
                      
                                      $priceTicket = $ticketSuccess*$rowTicket->ticket_price ;
                                  ?>
                                  <tr>
                                  <td>{{$ticket_name}} / {{$rowTicket->ticket_price}} บาท</td> 
                                    <td><?php echo number_format($ticketSuccess, 0, '.', ',');?></td>
                                    <td><?php echo number_format($priceTicket, 0, '.', ',');?></td>  
                                    <td></td> 
                                  </tr>
                                  <?php
                                     $AllTotalCount+=$ticketSuccess;
                                     $AllTotalPrice +=$priceTicket;
                             

                                   }// End foreach?>
                                   <tr>
                                    <th>รวม</th>
                                    <th><?php  echo number_format($AllTotalCount, 0, '.', ',');?></th>
                                    <th><?php  echo number_format($AllTotalPrice, 0, '.', ',');?></th> 
                                    <th></th>  
                                </tr>  
                                </tbody></table>
                              </div>
                              </div>
                            </div> 
                          </div> 
                          <div class="form-group"> 
                            <div class="col-md-12">
                              <div class="panel-cascade"> 
                                <div class="row"> 
                                <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th style="width:50%">ประเภทสินค้าเพิ่มเติม</th> 
                                    <th>จำนวน(ขายแล้ว)</th>
                                    <th>จำนวนเงิน</th>  
                                </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                    $AllTotalProductCount = 0; 
                                    $AllTotalProductPrice = 0;  

                                    $rowProducts = ApiContreller::getProductTypeLists($event_id);
                                    foreach ($rowProducts as $rowProduct) {
                                        
                                      $productMame    = json_decode($rowProduct->product_name, true); 
                                      $productMame = $productMame['data']['th']; 
                                      $productID = $rowProduct->product_id; 
                                      $productPrice = $rowProduct->product_price; 
                                  
                                      $countProduct = ApiContreller::getFuncProductStock($productID,  $event_id, $dateStart , $dateEnd  );
                                      
                                      $priceProductOrders = $countProduct*$productPrice ;
                                  ?>
                                  <tr>
                                    <td>{{$productMame}} / {{$productPrice}}</td> 
                                    <td>{{$countProduct}}</td> 
                                    <td><?php echo number_format($priceProductOrders, 0, '.', ',');?></td>
                             
                                    <td></td> 
                                  </tr>
                                  <?php
                                     $AllTotalProductCount  +=$countProduct;
                                     $AllTotalProductPrice  +=$priceProductOrders; 

                                   }// End foreach?>
                                   <tr>
                                    <th>รวม</th>
                                    <th><?php  echo number_format($AllTotalProductCount, 0, '.', ',');?></th>
                                    <th><?php  echo number_format($AllTotalProductPrice, 0, '.', ',');?></th> 
                                    <th></th>  
                                </tr>  
                                </tbody></table>
                              </div>
                              </div>
                            </div> 
                          </div> 
                          <input name="action" value="บันทึก | Save" type="hidden"> 

                  </div>
                  <!-- /panel body --> 
                </div>
              </div>
@endsection
@section('bottomScript')
<script src="{{ URL::asset('public/resources/js/bof/jquery-ui-1.10.3.custom.min.js') }}"></script>  
<script type="text/javascript">
jQuery(document).ready(function(){  
       $( "#dateStart" ).datepicker({
            maxDate: 0,  
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd', 
      onClose: function( selectedDate ) {
        $( "#dateEnd" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#dateEnd" ).datepicker({
      maxDate: 0,   
      changeMonth: true,
      dateFormat: 'yy-mm-dd', 
      onClose: function( selectedDate ) {
        $( "#dateStart" ).datepicker( "option", "maxDate", selectedDate );
      }
    }); 
  });   
</script> 
@endsection 
 