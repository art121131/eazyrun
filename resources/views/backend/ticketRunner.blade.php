
<?php

use App\Http\Controllers\Frontend\ApiContreller; ?>
@extends('backend.layout.master') 
@section('titlepage', "รายชื่อ") 
@section('topScript')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link  href="{{URL::asset('public/resources/css/datepicker.css') }}" rel="stylesheet">
<style>
    #toast-container{ top:60px}
    .table-bordered>thead>tr>th {
        text-align: center;
        white-space: nowrap;
    }
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
    }
    #table-search tbody tr {
        display: none;
    }
    .page-integer {
        color: #3c8dbc;
        border: 1px solid #D1D1D1;
        padding: 1px 3px;
    }
    .active {
        color: #ffffff;
        background-color: #3c8dbc;
    }
    .text-total {
        color: #0080FF;
        font-weight: bold;
    }
    .btn-edit {
        padding: 2px 5px;
        border-radius: 5px;
        border: 1px solid #ddd;
        cursor: pointer;
    }
    
    .loading {
        margin:auto; 
        margin-top:100px; 
        margin-bottom:100px;
    }
    
    @media (max-width: 992px) and (min-width: 1px){
        .sidebar-collapsed, .sidebar.toggled {
            margin-left: 0;
        }
        .sidebar {
            margin-left: -220px;
        }
    }
</style>
@endsection 
@section('content')
<div class="col-md-12">
    <div class="panel"> 
        <div class="panel-body"> 
            <div class="text-center" style="padding:5px 0px 10px 0px">
                <form class="" id="form-filter" method="get" style="margin-right:15px" autocomplete="off">
                    <div class="form-group row">
                        <label class="col-md-offset-2 col-md-2 text-right" style="padding:6px" for="nameMember">ชื่อ - นามสกุล</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="nameMember" placeholder="ชื่อ - นามสกุล" name="nameMember">
                        </div>
                        <label class="col-md-1 text-right" style="padding:6px" for="bibNo">Bib No.</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="bibNo" placeholder="Bib No." name="bibNo">
                        </div>          
                    </div>
                    <div class="form-group row">
                        <label class="col-md-offset-2 col-md-2 text-right" style="padding:6px" for="dateStart">วันที่</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="dateStart" placeholder="จากวันที่" name="dateStart">
                        </div>
                        <label class="col-md-1 text-right" style="padding:6px" for="dateEnd">จนถึง</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="dateEnd" placeholder="ถึงวันที่" name="dateEnd">
                        </div>          
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-md-offset-2 text-right" style="padding:6px">ช่วงอายุ</label>
                        <div class="col-md-2">
                            <input type="number" class="form-control" placeholder="ตั้งแต่อายุ" name="ageStart">
                        </div>
                        <label class="col-md-1 text-right" style="padding:6px">จนถึง</label>
                        <div class="col-md-2">
                            <input type="number" class="form-control" placeholder="จนถึงอายุ" name="ageEnd">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-offset-2 col-md-2 text-right" style="padding:6px" for="telNo">เบอร์โทรศัพท์</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="telNo" placeholder="เบอร์โทรศัพท์" name="telNo">
                        </div>
                        <label class="col-md-1 text-right" style="padding:6px" for="email">อีเมล์</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="email" placeholder="อีเมล์" name="email">
                        </div>          
                    </div>
                    <div class="form-group row">
                        <label class="col-md-offset-2 col-md-2 text-right" style="padding:6px" for="telNo">เลขบัตรประชาชน</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="citizen_id" placeholder="เลขบัตรประชาชน" name="citizen_id">
                        </div>
                        <label class="col-md-1 text-right" style="padding:6px" for="country">ประเทศ</label>
                        <div class="col-md-2">
                            <select name="country" class="form-control">
                                <option value="">--เลือกทั้งหมด--</option> 
                                <?php
                                foreach ($countries as $value) {
                                    ?>
                                    <option value="{{$value->id}}">{{$value->country_name}}</option>
                                <?php } ?>
                            </select>
                        </div>          
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-md-offset-2 text-right" style="padding:6px">ประเภทตั๋ว</label>
                        <div class="col-md-2">
                            <select name="ticket_id" class="form-control">
                                <option value="">--เลือกทั้งหมด--</option> 
                                <?php
                                foreach ($tickets as $value) {
                                    $ticket_name = json_decode($value->ticket_title, true);
                                    $ticket_name = $ticket_name['data']['th'];
                                    ?>
                                    <option value="{{$value->ticket_id}}">{{$ticket_name}}</option>
                                <?php } ?>
                            </select>
                        </div>
                        <label class="col-md-1 text-right" style="padding:6px">เพศ</label>
                        <div class="col-md-2">
                            <select name="gender" class="form-control">
                                <option value="">--เลือกทั้งหมด--</option> 
                                <option value="ชาย">ชาย</option> 
                                <option value="หญิง">หญิง</option> 
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-9 col-md-offset-2 col-sm-12">
                            <a type="submit" class="btn btn-primary" id="btnSearch"><i class="fa fa-search" aria-hidden="true"></i> ค้นหา</a>&nbsp;&nbsp;
                            <a type="submit" class="btn btn-success" id="exportExcel"><i class="fa fa-table" aria-hidden="true"></i> Export Excel</a>
                        </div>
                    </div>
                </form>      
            </div>
            <div class="text-left" style="padding:5px 0px 10px 10px">
                <span id="totalMember"></span>
            </div>
            <div class="table-responsive" id="content-table">
            </div>
        </div>
    </div>
</div>
@endsection
@section('bottomScript')  
<script src="{{ URL::asset('public/resources/js/bof/jquery-ui-1.10.3.custom.min.js') }}"></script>  
<script type="text/javascript" src="{{ URL::asset('public/resources/js/bootbox.min.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/datepicker.th-TH.js') }}"></script>
<script type="text/javascript">
toastr.options = {
    "closeButton": false,"debug": false,"newestOnTop": false, "progressBar": false,"positionClass": "toast-top-center", "preventDuplicates": false, "onclick": null, "showDuration": "1000", "hideDuration": "1000", "timeOut": "1000", "extendedTimeOut": "1000", "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"
} 
jQuery(document).ready(function () {
   
    $("#dateStart").datepicker({
        endDate:  new Date(),
        language: 'th-TH',
        format: 'yyyy-mm-dd',
        hide: function (e) {
            $("#dateEnd").datepicker("setStartDate",(e.target.value == '' ? null : e.target.value));
        }
    });

    $("#dateEnd").datepicker({
        endDate:  new Date(),
        language: 'th-TH',
        format: 'yyyy-mm-dd',
        hide: function (e) {
           $("#dateStart").datepicker("setEndDate", (e.target.value == '' ? new Date() : e.target.value));
        }
    });

    $("#exportExcel").click(function () {
        var data = $("#form-filter").serialize()
        window.open("{{URL::to('backoffice_management/ticket/ticket-export?')}}"+ data);
    });
    
    $('#btnSearch').click(function () {
        jQuery("#totalMember").removeClass('text-danger');
        jQuery("#totalMember").addClass('text-total');
        jQuery("#totalMember").text("กำลังค้นหาข้อมูล");
        $("#content-table").html(`<table class="table table-bordered"><thead><tr><th>No.</th><th>Order No.</th><th>Ticket No.</th><th>ระยะ</th>
            <th>วันเวลา ชำระเงิน</th><th>ราคาบัตร</th><th>ของที่ระลึก</th><th>ประเภท </th><th>ชื่อ-นามสกุล</th><th>Bib No.</th><th>เลขบัตรประชาชน</th>
            <th>วันเดือนปี เกิด</th><th>อายุ</th><th>เพศ</th> <th>ประเทศ</th> <th>สัญชาติ</th>  <th>อีเมล</th>  <th>เบอร์โทร</th> <th>กรุ๊ปเลือด</th><th>ชื่อ ผู้ติดต่อฉุกเฉิน</th>
            <th>เบอร์โทร ผู้ติดต่อฉุกเฉิน</th><th>ชื่อสมาชิกที่สมัคร</th><th>วิธีการรับของ</th> <th>ชื่อผู้ส่ง</th><th>ที่อยู่จัดส่ง</th> <th>จังหวัด</th><th>รหัสไปร์ษณีย์</th>
            </tr></thead><tbody><tr><td colspan="27"><div class="loading text-center"><p><b>Loading...</b></p>
            <img src="{{URL::asset('public/resources/images/loading-cicle.gif')}}"></div></td></tr></tbody></table>`); 
        jQuery.ajax({
            url: `{{URL::to('backoffice_management/ticket/ticket-search')}}`,
            type: "GET",
            cache: false,
            data: $("#form-filter").serialize(),
            success: function (data) {
                $("#content-table").html(data); 
                setPageInteger();
                btnEdit();
            }
        });
    });
    
    document.getElementById('btnSearch').click();
    
    function btnEdit(){
        $('.btn-edit').click(function () {
            jQuery.ajax({
                url: '{{URL::to('backoffice_management/ticket/ticket-data' )}}',
                type: "GET",
                cache: false,
                data: "dataID=" + $(this).attr('data-id'),
                success: function (data) {
                    data.ticket = formatNull(data.ticket)
                    var opCountry = ""
                    var opSouvenir = ""
                    if(data.countries != ''){
                        data.countries.map((d,i)=>{
                            opCountry += `<option value="${d.id}" ${(data.ticket.countries_id == d.id ? 'selected' : '')}>${d.country_name}</option>`
                        })
                    }
                    if(data.type_souvenir != ''){
                        data.type_souvenir.map((d,i)=>{
                            if(d.souvenir_type_name != ''){
                                d.souvenir_type_name = JSON.parse(d.souvenir_type_name)
                               opSouvenir += `<option value="${d.souvenir_type_id}" ${(data.ticket.type_souvenir_id == d.souvenir_type_id ? 'selected' : '')}>${d.souvenir_type_name.data.en}</option>` 
                            }                        
                        })
                    }
                    bootbox.dialog({
                        title: 'Ticket No. '+data.ticket.ticket_code,
                        message: `<div class="row">`+
                                `<div class="col-md-12">`+
                                `<form class="form-horizontal" id="form-edit">`+

                                `<div class="form-group row">`+
                                `<div class="col-md-6"><label for="pre_name">คำนำหน้า</label>`+
                                `<select name="pre_name" id="pre_name" class="browser-default required form-control">`+
                                `<option value="" disabled selected>คำนำหน้า/Title *</option>`+
                                `<option value="Mr." ${(data.ticket.pre_name == 'Mr.' ? 'selected' : '')}>นาย/Mr.</option>`+
                                `<option value="Mrs." ${(data.ticket.pre_name == 'Mrs.' ? 'selected' : '')}>นาง/Mrs.</option>`+
                                `<option value="Miss" ${(data.ticket.pre_name == 'Miss.' ? 'selected' : '')}>นางสาว/Miss</option>`+
                                `</select></div><div class="col-md-6"><label for="bib_no">Bib No.</label>`+
                                `<input type="text" class="form-control required" id="bib_no" name="bib_no" placeholder="BIB No." value="${data.ticket.bib}">`+
                                `</div></div>` +                         
                                `<div class="form-group row">`+
                                `<div class="col-md-6"><label for="f_name">ชื่อ</label>`+
                                `<input type="text" class="form-control required" id="f_name" name="f_name" placeholder="First Name" value="${data.ticket.f_name}">`+
                                `</div><div class="col-md-6"><label for="l_name">นามสกุล</label>`+
                                `<input type="text" class="form-control required" id="l_name" name="l_name" placeholder="Last Name" value="${data.ticket.l_name}">`+
                                `</div></div>` +
                                `<div class="form-group row">`+
                                `<div class="col-md-6"><label for="citizen_id">เลขบัตรประชาชน</label>`+
                                `<input type="text" class="form-control required" id="citizen_id" name="citizen_id" placeholder="ID Card or Passport No." value="${data.ticket.citizen_id}">`+
                                `</div><div class="col-md-6"><label for="email">อีเมล</label>`+
                                `<input type="text" class="form-control required" id="email" name="email" placeholder="E-mail" value="${data.ticket.email}">`+
                                `</div></div>` +
                                `<div class="form-group row">`+
                                `<div class="col-md-6"><label for="tel">เบอร์มือถือ</label>`+
                                `<input type="text" class="form-control required" id="tel" name="tel" placeholder="Tel." value="${data.ticket.tel}">`+
                                `</div><div class="col-md-6"><label for="birthday">วันเกิด</label>`+
                                `<input type="text" class="form-control required datepicker" id="birthday" name="birthday" placeholder="Birthday" value="${(data.ticket.birthday != "" ? getDate(data.ticket.birthday) : "")}">`+
                                `</div></div>` +
                                `<div class="form-group row">`+
                                `<div class="col-md-6"><label for="countries_id">ประเทศ</label>`+
                                `<select name="countries_id" id="countries_id" class="browser-default required form-control">`+
                                `<option value="" disabled selected>--Country--</option>${opCountry}`+                           
                                `</select></div><div class="col-md-6"><label for="nationality">สัญชาติ</label>`+
                                `<input type="text" class="form-control required" id="nationality" name="nationality" placeholder="Nationality" value="${data.ticket.nationality}">`+
                                `</div></div>` +  
                                `<div class="form-group row">`+
                                `<div class="col-md-6"><label for="religion">ศาสนา</label>`+
                                `<input type="text" class="form-control required" id="religion" name="religion" placeholder="Religion" value="${data.ticket.religion}">`+
                                `</div><div class="col-md-6"><label for="blood_group">กรุ๊ปเลือด</label>`+
                                `<input type="text" class="form-control required" id="blood_group" name="blood_group" placeholder="Blood Group" value="${data.ticket.blood_group}">`+
                                `</div></div>` +                          
                                `<div class="form-group row">`+
                                `<div class="col-md-6"><label for="gender">เพศ</label>`+
                                `<select name="gender" id="gender" class="browser-default required form-control">`+
                                `<option value="" disabled selected>--Gender--</option>`+
                                `<option value="ชาย" ${(data.ticket.gender == 'ชาย' ? 'selected' : '')}>ชาย/Male</option>`+
                                `<option value="หญิง" ${(data.ticket.gender == 'หญิง' ? 'selected' : '')}>หญิง/Female</option>`+
                                `</select></div><div class="col-md-6"><label for="type_souvenir_id">ไซร์เสื้อ</label>`+
                                `<select name="type_souvenir_id" id="type_souvenir_id" class="browser-default required form-control">`+
                                `<option value="" disabled selected>--Size--</option>${opSouvenir}`+ 
                                `</select></div></div>` +                          
                                `<div class="form-group row">`+
                                `<div class="col-md-6"><label for="ticket_contact_name">ชื่อ ผู้ติดต่อฉุกเฉิน</label>`+
                                `<input type="text" class="form-control required" id="ticket_contact_name" name="ticket_contact_name" placeholder="Contact Name" value="${data.ticket.ticket_contact_name}">`+
                                `</div><div class="col-md-6"><label for="ticket_contact_tel">เบอร์โทร ผู้ติดต่อฉุกเฉิน</label>`+
                                `<input type="text" class="form-control required" id="ticket_contact_tel" name="ticket_contact_tel" placeholder="Contact Tel." value="${data.ticket.ticket_contact_tel}">`+
                                `</div></div>` +
                                `<input type="hidden" name="ticket_id" value="${data.ticket.ticket_id}">`+
                                `<input type="hidden" name="year_month" value="${data.year_month}">`+
                                `</form></div></div>`,
                        buttons: {
                            cancel: {
                                label: 'ยกเลิก',
                                className: 'btn-danger'
                            },
                            success: {
                                label: "ยืนยัน",
                                className: "btn-success",
                                callback: function () {
                                    jQuery.ajax({
                                        url : '{{URL::to('backoffice_management/ticket/ticket-edit' )}}',
                                        type:"POST",
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                         }, 
                                        cache: false,
                                        data : $('#form-edit').serialize(), 
                                        success :function(data){ 
                                            if(data.status == "ok"){
                                                toastr.success( 'Update Success')
                                            } else {
                                                toastr.warning( 'Update Fail')
                                            }
                                        }
                                     });
                                }
                            }
                        }
                    })
                    $(".datepicker").datepicker({
                        zIndex: 1100,
                        language: 'th-TH',
                        format: 'yyyy-mm-dd',
                    });
                }
            });
        })
    }
    
    function formatNull(data) {
        Object.keys(data).forEach(function (key) {
            data[key] = (data[key] != null) ? data[key] : ""
        });
        return data
    }
    
    function getDate(dateTime) {
        dateTime = dateTime.substr(0,dateTime.indexOf(' '));
        return dateTime
    }
    
    function setPageInteger(){
        $('#table-search').after('<div id="nav"></div>');
        var rowsShown = 100;
        var rowsTotal = $('#table-search tbody tr').length;
        var numPages = rowsTotal / rowsShown;

        jQuery("#totalMember").text("ค้นพบ ทั้งหมด " + rowsTotal + " รายชื่อ");
        if (rowsTotal == 0) {
            jQuery("#totalMember").removeClass('text-total');
            jQuery("#totalMember").addClass('text-danger');
        } else {
            jQuery("#totalMember").removeClass('text-danger');
            jQuery("#totalMember").addClass('text-total');
        }
        for (i = 0; i < numPages; i++) {
            var pageNum = i + 1;
            $('#nav').append('<a href="#" rel="' + i + '" class="page-integer">' + pageNum + '</a> ');
        }
        $('#table-search tbody tr').hide();
        $('#table-search tbody tr').slice(0, rowsShown).show();
        $('#nav a:first').addClass('active');
        $('#nav a').bind('click', function () {

            $('#nav a').removeClass('active');
            $(this).addClass('active');
            var currPage = $(this).attr('rel');
            var startItem = currPage * rowsShown;
            var endItem = startItem + rowsShown;
            $('#table-search tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
                    css('display', 'table-row').animate({opacity: 1}, 300);
        });
    }
});
</script> 
@endsection 
