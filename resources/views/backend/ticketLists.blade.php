@extends('backend.layout.popup') 
@section('titlepage', 'จัดการตั๋ว') 
@section('topScript')
@endsection 
@section('content')
  <div class="page-header"><a class="btn btn-success btn-sm" href="{{URL::to('backoffice_management/events/add-ticket' )}}/{{$id}}"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> {{$textTypeTicket}}</a></div>
  <div id="result" class="well well-sm url" style="margin:10px 20px; display:none">ได้ทำการแก้ไขอันดับการแสดงผลเรียบร้อยแล้วค่ะ</div>
   <table class="table table-striped">
    <thead>
      <tr>
        <th width="40%">ประเภท</th>
        <th>จำนวนสต๊อกทั้งหมด</th>
        <th>ราคา(บาท)</th>
        <th>สถานะ</th>
        <th></th>  
    </tr>
    </thead>
    <tbody id="table-1">
      @foreach($tickets as $row)
      <?php 
       $ticketName    = json_decode($row->ticket_title, true);  
       $ticketPublic = $row->ticket_public; 
      ?><tr id="{{$row->ticket_id}}">
        <td>{{$ticketName['data']['th']}} <br>
          <small>วันที่เปิดขาย : {{ date("d-m-Y h:i", strtotime($row->ticket_date_start_on))}} - {{ date("d-m-Y h:i", strtotime($row->ticket_date_end_on))}}</small>
        </td>
        <td>{{$row->ticket_stock}}</td>
        <td>{{$row->ticket_price}}</td>
        <td>
           @if($ticketPublic == 1)
            <span class="label label-success">Publish</span>
            @elseif($ticketPublic == 0)
            <span class="label label-warning">Un Publish</span>
            @endif
         </td> 
        <td><a href="{{URL::to('backoffice_management/events/edit-ticket' )}}/{{$row->ticket_id}}"   class="fancybox-media btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> {{$btnTextEdit}}</a></td>
    </tr>
     @endforeach 
    </tbody>
</table>
@endsection
@section('bottomScript')
<script type="text/javascript" src="{{ URL::asset('public/resources/js/jquery.tablednd.js') }}"></script> 
<script type="text/javascript">
$(document).ready(function(){  
    jQuery('#table-1').tableDnD({
          onDrop: function(table, row) {
      var order= jQuery.tableDnD.serialize('id');
      jQuery.ajax({type: "GET",url: "{{URL::to('backoffice_management/events/dragdrop-ticket' )}}", data: order, 
       success:
         jQuery("#result").fadeIn(500).delay(5000).fadeOut(500)});
        }
    });
});
</script>
@endsection 
 