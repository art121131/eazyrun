<?php 
  use App\Http\Controllers\Frontend\ApiContreller;

    $totalAmount = '';
    $totalVat = '';
    $totalAll = '';
    $iNum = 1;
   $user = \Auth::user();
   $user_level = $user->level; 

?>@extends('backend.layout.master') 
@section('titlepage', $menuStockReport) 
@section('topScript')
 @endsection 
@section('content')
  <div class="panel-body">
  	 <div class="text-right" style="padding-top:5px"><form class="form-inline" method="get"> 
        <div class="form-group">
	        <label>กรองผลลัพท์ </label>
	         <div class="form-group">
	       <?php #==================================== Start !=  staff ===================================
	           if($user_level != 'Staff'){  ?> 
	        <select name="event_id" class="form-control" >

	              <option value="">--เลือก{{$menuEventName}}--</option> 
	              <?php
	                 $rowEvents = ApiContreller::getEventLists();
	                 foreach ($rowEvents as $rowEvent) {
	                    $event_name    = json_decode($rowEvent->event_name, true);
	                    $event_name    = $event_name['data']['lang_th'];
	                  ?>
	              <option value="{{$rowEvent->event_id}}" <?php echo ($event_id == $rowEvent->event_id)? "selected": "";?>  >{{ApiContreller::strCrop($event_name, 22)}}</option>
	              <?php }?>
	          </select>
	        <?php }#==================================== End !=  staff ===================================?>
	        </div>
	          
          <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>ค้นหา</button> 
        </div></form>
        <br></div>

       <div class="col-md-12">
            <div class="panel-cascade">
               
              <div class="row"> 
              <table class="table table-striped">
              <thead>
                <tr>
                  <th>ประเภท</th>
                  <th>จำนวนสต๊อกทั้งหมด</th>
                  <th>ชำระเงินเรียบร้อย</th>
                  <th>VIP</th>
                  <th>รอการชำระเงิน</th> 
                  <th>ทั้งหมด</th> 
                  <th>คงเหลือ</th> 
              </tr>
              </thead>
              <tbody>
             	<?php 
             		 $totalStock    = 0;
               		$totalTicketSuccess    = 0;
               		$totalTicketWait    = 0;
               		$totalTicketSucc_Wait    = 0;
               		$totalTicketTotal    = 0;
                  $totalTicketVIP = 0;
                  ?>

                  <?php 
             		    
 
 					          $type_event_stock = 4000; 

                    $type_event_id = 24; 
                    $type_event_id_normal = 28;
                    $ticketSuccess =  ApiContreller::getFuncStockReport($type_event_id, 'SUCCESS'  )+ApiContreller::getFuncStockReport($type_event_id_normal, 'SUCCESS'  );
                    $ticketWait =  ApiContreller::getFuncStockReport($type_event_id, 'WAIT'  )+ApiContreller::getFuncStockReport($type_event_id_normal, 'WAIT'  );
                    $ticketVIP =  ApiContreller::getFuncStockVIPReport($type_event_id )+ApiContreller::getFuncStockVIPReport($type_event_id_normal );
                    $ticketSucc_Wait = $ticketSuccess+$ticketWait;  
                    
                    $ticketTotal = ($type_event_stock-$ticketSucc_Wait);
                    
                      
                    $totalStock     +=   $type_event_stock;
                    $totalTicketSuccess     +=   $ticketSuccess;
                    $totalTicketWait     +=   $ticketWait;
                    $totalTicketSucc_Wait     +=   $ticketSucc_Wait;
                    $totalTicketTotal     +=   $ticketTotal;
                    $totalTicketVIP     +=   $ticketVIP;

               	?><tr>
                    <th>21km.</th>
                    <th><?php echo number_format($type_event_stock, 0, '.', ',');?></th>
                    <th><?php echo number_format($ticketSuccess-$ticketVIP, 0, '.', ',');?></th>
                    <th><?php echo number_format($ticketVIP, 0, '.', ',');?></th> 
                    <th><?php echo number_format($ticketWait, 0, '.', ',');?></th> 
                    <th><?php echo number_format($ticketSucc_Wait, 0, '.', ',');?> </th> 
                    <th><?php echo number_format($ticketTotal, 0, '.', ',');?> </th> 
                </tr>
                 <?php 
                    $type_event_id = 25; 
                    $type_event_id_normal = 29;

                    $type_event_stock = 5500; 
                    $ticketSuccess =  ApiContreller::getFuncStockReport($type_event_id, 'SUCCESS'  )+ApiContreller::getFuncStockReport($type_event_id_normal, 'SUCCESS'  );
                    $ticketWait =  ApiContreller::getFuncStockReport($type_event_id, 'WAIT'  )+ApiContreller::getFuncStockReport($type_event_id_normal, 'WAIT'  );
                    $ticketVIP =  ApiContreller::getFuncStockVIPReport($type_event_id )+ApiContreller::getFuncStockVIPReport($type_event_id_normal );
                    
                    $ticketSucc_Wait = $ticketSuccess+$ticketWait;

                    $ticketTotal = ($type_event_stock-$ticketSucc_Wait);


                    $totalStock     +=   $type_event_stock;
                    $totalTicketSuccess     +=   $ticketSuccess;
                    $totalTicketWait     +=   $ticketWait;
                    $totalTicketSucc_Wait     +=   $ticketSucc_Wait;
                    $totalTicketTotal     +=   $ticketTotal;
                    $totalTicketVIP     +=   $ticketVIP;

                ?><tr>
                    <th>10km.</th>
                    <th><?php echo number_format($type_event_stock, 0, '.', ',');?></th>
                    <th><?php echo number_format($ticketSuccess-$ticketVIP, 0, '.', ',');?></th>
                    <th><?php echo number_format($ticketVIP, 0, '.', ',');?></th> 
                    <th><?php echo number_format($ticketWait, 0, '.', ',');?></th> 
                    <th><?php echo number_format($ticketSucc_Wait, 0, '.', ',');?> </th> 
                    <th><?php echo number_format($ticketTotal, 0, '.', ',');?> </th> 
                </tr>
                 <?php 
                     $type_event_id = 26; 
                     $type_event_id_normal = 30;
 
                      $type_event_stock = 4500; 
                      $ticketSuccess =  ApiContreller::getFuncStockReport($type_event_id, 'SUCCESS'  )+ApiContreller::getFuncStockReport($type_event_id_normal, 'SUCCESS'  );
                      $ticketWait =  ApiContreller::getFuncStockReport($type_event_id, 'WAIT'  )+ApiContreller::getFuncStockReport($type_event_id_normal, 'WAIT'  );
                      $ticketVIP =  ApiContreller::getFuncStockVIPReport($type_event_id )+ApiContreller::getFuncStockVIPReport($type_event_id_normal );
                      
                      $ticketSucc_Wait = $ticketSuccess+$ticketWait;

                      $ticketTotal = ($type_event_stock-$ticketSucc_Wait);

                      $totalStock     +=   $type_event_stock;
                      $totalTicketSuccess     +=   $ticketSuccess;
                      $totalTicketWait     +=   $ticketWait;
                      $totalTicketSucc_Wait     +=   $ticketSucc_Wait;
                      $totalTicketTotal     +=   $ticketTotal;
                      $totalTicketVIP     +=   $ticketVIP;

                ?><tr>
                    <th>6km.</th>
                    <th><?php echo number_format($type_event_stock, 0, '.', ',');?></th>
                    <th><?php echo number_format($ticketSuccess-$ticketVIP, 0, '.', ',');?></th>
                    <th><?php echo number_format($ticketVIP, 0, '.', ',');?></th> 
                    <th><?php echo number_format($ticketWait, 0, '.', ',');?></th> 
                    <th><?php echo number_format($ticketSucc_Wait, 0, '.', ',');?> </th> 
                    <th><?php echo number_format($ticketTotal, 0, '.', ',');?> </th> 
                </tr>
                 <?php 
                     $type_event_id = 27;
                     $type_event_id_normal = 31; 
 
                      $type_event_stock = 1000; 
                      $ticketSuccess =  ApiContreller::getFuncStockReport($type_event_id, 'SUCCESS'  )+ApiContreller::getFuncStockReport($type_event_id_normal, 'SUCCESS'  );
                      $ticketWait =  ApiContreller::getFuncStockReport($type_event_id, 'WAIT'  )+ApiContreller::getFuncStockReport($type_event_id_normal, 'WAIT'  );
                      $ticketVIP =  ApiContreller::getFuncStockVIPReport($type_event_id )+ApiContreller::getFuncStockVIPReport($type_event_id_normal );
                     
                      $ticketSucc_Wait = $ticketSuccess+$ticketWait;

                      $ticketTotal = ($type_event_stock-$ticketSucc_Wait);

                      $totalStock     +=   $type_event_stock;
                    $totalTicketSuccess     +=   $ticketSuccess;
                    $totalTicketWait     +=   $ticketWait;
                    $totalTicketSucc_Wait     +=   $ticketSucc_Wait;
                    $totalTicketTotal     +=   $ticketTotal;
                    $totalTicketVIP     +=   $ticketVIP;

                ?><tr>
                    <th>1.8km.</th>
                    <th><?php echo number_format($type_event_stock, 0, '.', ',');?></th>
                    <th><?php echo number_format($ticketSuccess-$ticketVIP, 0, '.', ',');?></th>
                    <th><?php echo number_format($ticketVIP, 0, '.', ',');?></th> 
                    <th><?php echo number_format($ticketWait, 0, '.', ',');?></th> 
                    <th><?php echo number_format($ticketSucc_Wait, 0, '.', ',');?> </th> 
                    <th><?php echo number_format($ticketTotal, 0, '.', ',');?> </th> 
                </tr> 
                <tr>
                  <th>รวม</th>
                  <th><?php echo number_format($totalStock, 0, '.', ',');?></th>
                  <th><?php echo number_format($totalTicketSuccess-$totalTicketVIP, 0, '.', ',');?></th>
                  <th><?php echo number_format($totalTicketVIP, 0, '.', ',');?></th>
                  <th><?php echo number_format($totalTicketWait, 0, '.', ',');?></th>
                  <th><?php echo number_format($totalTicketSucc_Wait, 0, '.', ',');?></th>
                  <th><?php echo number_format($totalTicketTotal, 0, '.', ',');?></th>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div> 
  </div>
@endsection
@section('bottomScript')  
<script type="text/javascript">
jQuery(document).ready(function(){  
  
  });   
</script> 
@endsection 
 