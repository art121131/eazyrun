<?php

use App\Http\Controllers\Frontend\ApiContreller;

$user = \Auth::user();
$user_level = $user->level;
?>@extends('backend.layout.master') 
@section('titlepage', 'เพิ่มผู้ใช้งานระบบ') 
@section('topScript')
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<style>
    #toast-container{ top:60px}
    .form-horizontal .control-label {
        padding-top: 7px;
        margin-bottom: 5px;
        text-align: right;
    }
</style>
@endsection 
@section('content')
<div class="col-md-12">
    <div class="panel"> 
        <div class="panel-body">
            <form action="{{URL::to('backoffice_management/staffs/saveform')}} " method="post" name="newsForm" class="form-horizontal form-page formular"id="newsForm" role="form" autocomplete="off">
                <div class="col-lg-6 col-md-12 col-sm-12" style="padding-top: 20px;">
                    <div class="form-group">
                        <label for="title" class="col-md-3 control-label">Name :</label>
                        <div class="col-md-9">
                            <input type="text" class="required form-control input"  name="name" id="name" placeholder="Name">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="title" class="col-md-3 control-label">Username :</label>
                        <div class="col-md-9">
                            <input type="text" class="required form-control input"  name="username" id="username" placeholder="Username" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-md-3 control-label">Password :</label>
                        <div class="col-md-9">
                            <input type="password" class="required form-control input"  name="password" id="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-md-3 control-label">Re-Password :</label>
                        <div class="col-md-9">
                            <input type="password" class="required form-control input"  name="re_password" id="re_password" placeholder="Re Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-md-3 control-label">Email :</label>
                        <div class="col-md-9">
                            <input type="text" class="required form-control input"  name="email" id="email" placeholder="email">
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12" style="padding-top: 20px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                        <thead>
                            <tr align="center">
                                <td width="70%"><strong>สิทธิ์การใช้งาน</strong></td>
                                <td width="30%"><strong>ใช้งานได้</strong></td>
                            </tr>
                        </thead>
                        <tbody> 
                            <tr>
                                <td>หน้าหลัก</td>
                                <td align="center"><input type="checkbox" class="checkboxs" name="main" value="1"></td>
                            </tr> 
                            <tr>
                                <td>ยอดขายแยกตามช่องทาง</td>
                                <td align="center"><input type="checkbox" class="checkboxs" name="financial_classify" value="1"></td>
                            </tr>  
                            <tr>
                                <td>ยอดขายแยกตามสถานะ</td>
                                <td align="center"><input type="checkbox" class="checkboxs" name="stock_report" value="1"></td>
                            </tr>
                            <tr>
                                <td>ยอดขายสินค้าเพิ่มเติม</td>
                                <td align="center"><input type="checkbox" class="checkboxs" name="product_stock" value="1"></td>
                            </tr>
                            <tr>
                                <td>ลงทะเบียน Walk-in</td>
                                <td align="center"><input type="checkbox" class="checkboxs" name="walkin" value="1"></td>
                            </tr>
                            <tr>
                                <td>รายชื่อ</td>
                                <td align="center"><input type="checkbox" class="checkboxs" name="ticket_runner" value="1"></td>
                            </tr>
                            <tr>
                                <td>นำเข้ารายชื่อ</td>
                                <td align="center"><input type="checkbox" class="checkboxs" name="import_order" value="1"></td>
                            </tr>
                            <tr>
                                <td>รับของที่ระลึก</td>
                                <td align="center"><input type="checkbox" class="checkboxs" name="received" value="1"></td>
                            </tr>
                            <tr>
                                <td>ผู้ใช้งานระบบ</td>
                                <td align="center"><input type="checkbox" class="checkboxs" name="staffs" value="1"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 20px;">
                    <div class="form-group text-center">
                        <input id="actionBtn" type="submit" value="บันทึก | Save" class="btn btn-primary" />
                        <input name="action" type="hidden" id="action" value="CreateStaff" />
                        <a id="back" class="btn btn-warning" href="{{URL::to('backoffice_management/staffs')}}">ย้อนกลับ | Back</a>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</div>
@endsection
@section('bottomScript')
<script type="text/javascript" src="{{ URL::asset('public/resources/js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript">
toastr.options = {
    "closeButton": false, "debug": false, "newestOnTop": false, "progressBar": false, "positionClass": "toast-top-center", "preventDuplicates": false, "onclick": null, "showDuration": "1000", "hideDuration": "1000", "timeOut": "1000", "extendedTimeOut": "1000", "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"
}
$(document).ready(function () {
    //$('#username').val('');
    // Form Validation
    /*$('#event_id').prop('disabled', 'disabled');
     jQuery("#level").change(function () {
     var level = $(this).val();
     if (level == 'Admin') {
     $('#event_id').prop('disabled', 'disabled');
     } else {
     $('#event_id').prop('disabled', false);
     }
     });*/

    $("#newsForm").validate({
        rules: {
            required: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8
            },
            re_password: {
                equalTo: "#password"
            }
        },
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-error');
            $(element).parents('.form-group').addClass('has-success');
        },
        submitHandler: function (form) {
            jQuery("#actionBtn").attr("disabled", "disabled");
            jQuery("#back").attr("disabled", "disabled");
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                },
                url: "{{URL::to('backoffice_management/staffs/saveform')}}",
                type: "POST",
                cache: false,
                data: $('#newsForm').serialize(),
                success: function (data) {
                    if (data.status == 'ok') {
                        $('.input').val("");
                        $('.checkboxs').prop('checked', false)
                        toastr.success('Save Success')
                    } else if(data.status == 'dup'){
                        toastr.warning('Username Duplicate')
                    } else {
                        toastr.warning('Save Fail')
                    }
                    jQuery("#actionBtn").removeAttr("disabled"); 
                    jQuery("#back").removeAttr("disabled"); 
                }
            });
        }
    });

});
</script>
@endsection 
