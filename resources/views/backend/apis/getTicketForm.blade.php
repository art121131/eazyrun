<?php
  use App\Http\Controllers\Frontend\ApiContreller;
  $pre_type_event = 1;
  $pre_name= '';
  $f_name = '';
  $l_name = '';
  $username = '';
  $citizen_id = '';
  $gender = '';
  $birthday = ''; 
  $nationality = 'Thai'; 
  $pre_birthday = '0'; 
  $iNumArr = 1;
  $getCount = '';
  $rateVatFrontend = $events->event_rate_backend;
  $formDataArray    = json_decode($events['event_form_additional'], true);   

  for($numTicket=0; $numTicket <  count($inputTicket) ; $numTicket++) { //Start for count($ticket);
      if($inputTicket[$numTicket] != '0'){ //Start Chk ticket  
       
         $ticketID       = $inputTicket[$numTicket]; 

        $conutticketID  = intval(substr($ticketID,0, 2)); //ตัวหน้าเป็นจำนวน
        $ticketTypeID   = substr($ticketID,3); //ตัวหลังเป็น  ticket id

         if($type_payment == 'promotion'){
            $conutticketID = $conutticketID*2;
        } 
         
         
        for($iTicket=1; $iTicket <= $conutticketID; $iTicket++) { // Get count ticket
          
          //========== Get ApiContreller  TicketType ====================
          $rowTicket = ApiContreller::getTicketType($ticketTypeID);
          
          $ticket_name  = json_decode($rowTicket->type_event_title, true);  
          $ticket_name  = $ticket_name['data']['lang_th'];

          $priceTicket = $rowTicket->type_event_price; 
          $vat_price = ($priceTicket * $rateVatFrontend)/100;
          $total_price = $priceTicket + $vat_price; 
          //========== End ApiContreller  TicketType ====================
           //======
         $pre_name= '';
         $f_name = '';
         $l_name = '';
         $username = '';
         $citizen_id = '';
         $gender = '';
         $birthday = ''; 
         $nationality = 'Thai'; 
         $pre_birthday = '0';  
        //======
?><div  id="div_box_<?php echo $pre_type_event;?>"  class="row"> 
  <div class="collapsible-header <?php echo ($pre_type_event == 1)? "active":"";?>">
    <h5><span class="number-badge"><?php echo $pre_type_event;?></span> <strong><?php  echo $ticket_name;?></strong><span class="btn_canecl_ticket right  " id="box_<?php echo $pre_type_event;?>">ปิดรายการนี้ X</span></h5>
  </div>
  <div class="collapsible-body" style="display:block"> 
      <?php //================================ Start Form ================================?>
    <div class="row">
        <div class="input-field col s6 l6">
          <select name="pre_name[{{$iNumArr}}]" id="pre_name_<?php echo $pre_type_event;?>" class="browser-default required">
            <option value="" disabled selected>คำนำหน้า/Title *</option>
            <option value="ด.ช." >ด.ช./Master</option>
            <option value="ด.ญ." >ด.ญ./Miss</option>
            <option value="Mr." <?php echo ($pre_type_event == 1 && $pre_name  == '1'  && $getCount <= 0)? "selected":"";?>>นาย/Mr.</option>
            <option value="Mrs." <?php echo ($pre_type_event == 1 && $pre_name  == '2' && $getCount <= 0)? "selected":"";?>>นาง/Mrs.</option>
            <option value="Miss" <?php echo ($pre_type_event == 1 && $pre_name  == '3' && $getCount <= 0)? "selected":"";?>>นางสาว/Miss</option>
          </select>
        </div> 
      </div>
      <div class="row">
        <div class="input-field col s12 l6">
          <input type="text" class="validate eng_only required"  name="f_name[{{$iNumArr}}]" id="f_name_<?php echo $pre_type_event;?>" value="<?php echo $f_name?>">
          <label for="f_name_<?php echo $pre_type_event;?>" class="f_name_<?php echo $pre_type_event;?> <?php echo ($pre_type_event == 1 && $f_name != '')? "active":"";?>">ชื่อ/First Name *</label>
        </div>
        <div class="input-field col s12 l6">
          <input type="text" class="validate eng_only required"  name="l_name[{{$iNumArr}}]" id="l_name_<?php echo $pre_type_event;?>" value="<?php echo $l_name?>">
          <label for="l_name_<?php echo $pre_type_event;?>" class="l_name_<?php echo $pre_type_event;?> <?php echo ($pre_type_event == 1 && $l_name != '')? "active":"";?>">นามสกุล/Last Name *</label>
        </div>
      </div>  
      <div class="row">
        <div class="input-field col s12 l6">
          <input type="email" class="validate required"  name="email[{{$iNumArr}}]" id="email_<?php echo $pre_type_event;?>" value="<?php echo $username?>">
          <label for="email_<?php echo $pre_type_event;?>" class="email_<?php echo $pre_type_event;?> <?php echo ($pre_type_event == 1 && $username != '')? "active":"";?>">อีเมล/Email *</label>
        </div> 
        <div class="input-field col s12 l6"> 
          <input type="text" class="validate eng_only required" name="nationality[{{$iNumArr}}]" value='<?php echo $nationality?>' id="nationality_<?php echo $pre_type_event;?>">
          <label for="nationality" class="nationality_<?php echo $pre_type_event;?> <?php echo ($nationality != '')? "active":"";?>">สัญชาติ/Nationality *</label>
        </div>
      </div> 
      <div class="row">
        <div class="input-field col s12 l6">
          <input type="text" class="validate input_citizen_id pre_type_event required"  name="citizen_id[{{$iNumArr}}]" id="citizen_id_rs_<?php echo $pre_type_event;?>" value="<?php echo $citizen_id?>">
          <label class="citizen_id_<?php echo $pre_type_event;?> <?php echo ($pre_type_event == 1 && $citizen_id != '')? "active":"";?>" for="citizen_id_rs_<?php echo $pre_type_event;?>">เลขบัตรประชาชน/ID Card or Passport No. *</label>
        <div class="citizen_id_rs_<?php echo $pre_type_event;?> error-message-citizen"></div>
        </div>
        <div class="input-field col s6 l6">
          <select  name="gender[{{$iNumArr}}]"  id="gender_<?php echo $pre_type_event;?>" class="browser-default pre_type_event required">
            <option value="" disabled selected>เพศ/Gender *</option>
            <option value="ชาย/Male" <?php echo ($pre_type_event == 1 && $gender  == '1')? "selected":"";?>>ชาย/Male</option>
            <option value="หญิง/Female" <?php echo ($pre_type_event == 1 && $gender  == '2')? "selected":"";?>>หญิง/Female</option>
          </select>
        </div>
      </div> 
      <div class="row">
        <div class="input-field col s12 l6">
          <input id="datepicker_<?php echo $pre_type_event;?>" type="date" class="datepicker_<?php echo $pre_type_event;?> required"  name="birthday[{{$iNumArr}}]" value="<?php echo  $birthday?>">
            <label for="datepicker_<?php echo $pre_type_event;?>" class="datepicker_<?php echo $pre_type_event;?> <?php echo ($pre_type_event == 1 && $username != '')? "active":"";?> truncate" >วันเดือนปีเกิด/Date of Birth *</label>
        </div>
        <div class="input-field col s6 l6">
          <div class="row">
            <div class="col l12">
              <h5 class="black-text"><span class="member_age_<?php echo $pre_type_event;?>"><?php  echo $pre_birthday;?></span> ปี/Years</h5>
              <label for="age" class="active">อายุ/Age</label>
            </div>
          </div>
        </div>
      </div>
      <?php //================================ start  ของที่ระลึก ================================?>
      <div class="row clearfix">
      @foreach($Souvenirs as $rowCateSouvenir)
       <?php   $souvenir_name = json_decode($rowCateSouvenir->souvenir_name, true); 
               $souvenir_name =  $souvenir_name['data']['lang_th'];
               $souvenir_pic =  $rowCateSouvenir->souvenir_pic;
        ?>
        <div class="col l4">
          <h5><strong><?php echo $souvenir_name;?></strong></h5>
          <p  class="pre_radio ">
            <input class="with-gap required"  type="radio" id="size-<?php echo $pre_type_event;?>-9999"  name="souvenir_type_id[{{$iNumArr}}]" value=""   />
          </p>
          <?php $TypeSouvenir = ApiContreller::getSouvenir($event_id, $rowCateSouvenir->souvenir_id);?>
          @foreach($TypeSouvenir as $rowTypeSouvenir) 
          <?php  $souvenir_type_name  = json_decode($rowTypeSouvenir->souvenir_type_name, true); 
                 $souvenir_type_name  = $souvenir_type_name['data']['lang_th']; 
                 $souvenir_type_id    = $rowTypeSouvenir->souvenir_type_id;
                 $souvenir_type_stock = $rowTypeSouvenir->souvenir_type_stock;  

           //===================================  Start Souvenir ===================================
               $souvenirSuccess  = ApiContreller::getFuncSouvenirReport($souvenir_type_id , 'SUCCESS' );
               $souvenirWait   = ApiContreller::getFuncSouvenirReport($souvenir_type_id , 'WAIT' );
               $souvenirSucc_Wait = $souvenirSuccess+$souvenirWait;
               $souvenirTotal = ($souvenir_type_stock-$souvenirSucc_Wait);

           //===================================  End Souvenir ===================================
              if($souvenirTotal >= 1 ){
           ?><p>
             <input class="with-gap required"  type="radio" id="size-<?php echo $pre_type_event;?>-<?php echo $souvenir_type_id;?>"  name="souvenir_type_id[{{$iNumArr}}]" value="<?php echo $souvenir_type_id;?>" />
             <label for="size-<?php echo $pre_type_event;?>-<?php echo $souvenir_type_id;?>"><?php echo $souvenir_type_name;?></label>
          </p><?php }else{?><p>
           <input class="with-gap"   type="radio" id="size-s" disabled="disabled"  />
           <label for="size-s"><?php echo $souvenir_type_name;?><span class="red-text">หมดแล้ว/SOLD OUT</span></label>
         </p><?php }?>
          @endforeach 
        </div> 
      @endforeach
      </div>
      <?php //================================ End  ของที่ระลึก ================================?> 
      <?php //================================ End Form ================================?> 
      <div class="clearfix"></div>
 <div style="margin-left:20px">
    <?php
 //================ Start Form ================
// ######## ######## ######## ######## ######## ######## ######## ########
// create forms in html
  $textGroup = 1;
  if($formDataArray != ''){
  foreach ($formDataArray as $index => $formData) {

    $formID = $formData["id"];
    $formType = $formData["type"];
    $formTitle = $formData["title"];
    $formValue = $formData["value"];
?> 
<div id="<?php echo $formID . '[' . $iNumArr . ']';?>" class="div_q"> 
  <strong><?php echo $formTitle;?></strong> <br>
  <?php if ($formType == "radio" || $formType == "checkbox") {?> 
    <?php
     $numInput = 1;
     foreach ($formValue as $index => $value) {
      $id_Input =  $pre_type_event.'_'.$textGroup.'_'.$numInput ; 
      if($value != ''){ //dddd
      ?>  
      <div class="form-group">
        <input   class="with-gap" id="{{$id_Input}}" type="<?php echo $formType;?>"  name="<?php echo $formID . '[' . $iNumArr . ']'; if ($formType == 'checkbox') echo '[]';?>" value="<?php echo $value;?>"<?php if ($formType == 'radio' && $index == 0) echo " checked";?>>
        <label for="{{$id_Input}}" class="{{$id_Input}}"><?php echo $value;?></label> 
      </div> 
      <?php $numInput++;  } }?>  
  <?php } else {?>
  <div class="row">
  <div class=" col s12 l6">
    <input  type="<?php echo $formType;?>" 
        name="<?php echo $formID . '[' . $iNumArr . ']';?>"
        placeholder="<?php echo $formValue;?>">
      </div>
  </div>
  <?php }?>
</div> 
<?php $textGroup++;
     }  //End foreach
  }//End if
 // ######## ######## ######## ######## ######## ######## ######## ######## 
  ?></div>
  </div> 
<input type="hidden" name="type_event_id[{{$iNumArr}}]" value="<?php echo $ticketTypeID;?>">
<input type="hidden" name="type_event_price[{{$iNumArr}}]" value="<?php echo $total_price;?>">
</div>
<?php  
        $pre_type_event++;
        $iNumArr++;  //num array
      }// Get count ticket
    }//End Chk ticket
  }//End for count($ticket);
?><input type="hidden" name="form_data" value="<?php echo htmlspecialchars(json_encode($formDataArray));?>">
<div class="form-group"> <br>
<div class="col-md-12 right">  
  <input  name="action" value="cash"  type="hidden" >
  <input type="submit" value="บันทึก" class="btn bg-primary text-white btn-lg" > 
</div> 
</div>  