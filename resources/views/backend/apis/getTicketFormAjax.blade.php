@foreach($tickets as $rowTicket)
	<?php 
   $ticketDisplay = true; 
   $ticketStatusSoldout = false;

	 @$ticketName    = json_decode($rowTicket->ticket_title, true);  
   @$ticketDes    = json_decode($rowTicket->ticket_des, true);  

	 $ticketName = $ticketName['data'][$lang];
   $ticketDes = $ticketDes['data'][$lang];

	  $priceTicket = $rowTicket->ticket_price; 
    $ticketID = $rowTicket->ticket_id; 

    $ticket_stock = $rowTicket->ticket_stock;
    $ticket_display = $rowTicket->ticket_display;

     //============== Start Count Ticket ===============//
      $ticketSuccess        =  ApiContreller::getFuncStockReport($ticketID, 'SUCCESS'  );
      $ticketWait       =  ApiContreller::getFuncStockReport($ticketID, 'WAIT'  );
      $ticketAddTicket      =  ApiContreller::getFuncStockReport($ticketID, 'ADDTICKET'  );
       $ticketSuccWaitAddticket = $ticketSuccess+$ticketWait+$ticketAddTicket; 
      $ticketTotal = ($ticket_stock-$ticketSuccWaitAddticket); 
    //============== Start Count Ticket ===============// 

     $ticket_date_start_on = $rowTicket->ticket_date_start_on;
     $ticket_date_end_on   = $rowTicket->ticket_date_end_on; 
     //======= chk ticket_display// 
    if(($ticket_display == 2) && ($dateNow >= $ticket_date_start_on) && ($dateNow <= $ticket_date_end_on)){ 
       $ticketDisplay = true;

    }else if(($ticket_display == 2 && $dateNow < $ticket_date_start_on ) || ($ticket_display == 2 && $dateNow > $ticket_date_end_on ) ){ 
       $ticketDisplay = false;
    }
   //======= end chk ticket_display//  

     if($dateNow >= $ticket_date_start_on && $dateNow <= $ticket_date_end_on && $ticketTotal >= 1 ){// End if chk date
        $ticketStatusSelector = 'OPEN';
     }else if($dateNow < $ticket_date_start_on && $dateNow < $ticket_date_end_on  ){// End if chk date
        $ticketStatusSelector = 'COMINGSOON'; 
     }else  if($dateNow > $ticket_date_end_on ||  $ticketTotal < 1 ){// End if chk date
         $ticketStatusSoldout = true;
         $ticketStatusSelector = 'SOLDOUT';
     }
   //====================

     
     $limitSelector = $ticketSelectLimit;
     if($ticketTotal <= $ticketSelectLimit){ 
         $limitSelector = $ticketTotal;
     }// Check Selector
      if($ticketDisplay == true){ 
        
       // echo $limitSelector;
	?>
  <tr> 
    <td><span class="text-secondary">{{$ticketName}}</span><br>
     @if($ticketDes != '')<span class="text-secondary txt-14">{{$ticketDes}}</span><br> @endif
    	<span class="text-danger txt-14 ">฿<?php echo number_format($priceTicket, 2, '.', ',');?></span>
    </td>
    <td class="pr-0 pl-0 text-center">
      <?php if( $ticketStatusSelector == 'OPEN' ){?>
  	   <div class="form-group">
           <div class="input-group number-spinner" >
                <span class="input-group-btn">
                    <a  data-dir="dwn" id="dw_{{$ticketID}}" class="pointer"><img src="{{ URL::asset('public/resources') }}/images/btn_down.svg" height="30"></a>
                </span>
                <input type="text" disabled name="ticketSelect[]" id="ticketSelect" class="form-control text-center inputForm" value="0" max="{{$limitSelector}}" min=0>
                <span class="input-group-btn">
                    <a  data-dir="up" id="up_{{$ticketID}}" class="pointer"><img src="{{ URL::asset('public/resources') }}/images/btn_up.svg" height="30"></a>
                </span>
            </div>
        </div>
        <?php }else if( $ticketStatusSelector == 'SOLDOUT' ){?>
         <button type="button" class="btn btn-danger">SOLD OUT</button>
        <?php }else if($ticketStatusSelector == 'COMINGSOON'  ){?> 
         <button type="button" class="btn btn-warning text-white">COMING SOON</button>
        <?php }?>
    </td>
  </tr> 
  <input name="ticket_id[]" type="hidden" value="<?php echo $ticketID?>"> 
  <input name="ticket_price[]" type="hidden" value="<?php echo $priceTicket?>">
  <input name="ticket_delivery_price[]" type="hidden" value="<?php echo $rowTicket->ticket_delivery_price?>"> 
  <input name="ticket_num[]" id="ticketID{{$ticketID}}" type="hidden" value="0"> 
  <input name="ticket_name[]" type="hidden" value="<?php echo $ticketName?>"> 
  <?php }//End ticketDisplay?>
 @endforeach 