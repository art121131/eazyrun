@extends('backend.layout.master')
@section('titlepage', 'ผู้ใช้งานระบบ') 
@section('topScript')
<style>
    table.tbl_btn_title tr>td:first-child{
        width: 71%;
    }
    table.tbl_btn_title tr>td:nth-child(2){
        width: 18%;
    }
    table.tbl_btn_title tr>td:nth-last-child{
        width: 11%;
    }
    @media (max-width: 1400px){
        table.tbl_btn_title tr>td:first-child{
            width: 66%;
        }
        table.tbl_btn_title tr>td:nth-child(2){
            width: 23%;
        }
        table.tbl_btn_title tr>td:nth-last-child{
            width: 11%;
        }
    }
    @media (max-width: 1200px){
        table.tbl_btn_title tr>td:first-child{
            width: 61%;
        }
        table.tbl_btn_title tr>td:nth-child(2){
            width: 28%;
        }
        table.tbl_btn_title tr>td:nth-last-child{
            width: 11%;
        }
    }
    @media (max-width: 996px){
        table.tbl_btn_title tr>td:first-child{
            width: 51%;
        }
        table.tbl_btn_title tr>td:nth-child(2){
            width: 38%;
        }
        table.tbl_btn_title tr>td:nth-last-child{
            width: 11%;
        }
    }
</style>
@endsection 
@section('content')
<div class="col-md-12">
    <div class="panel"> 
        <div class="panel-body"> 
{!! Form::open(array('url' => 'backoffice_management/staffs/saveform')) !!}
        <table width="100%" border="0" cellspacing="4" cellpadding="0"  class="tbl_btn_title">
            <tr>
              <td align="left"><a class="btn btn-success btn-sm " href="{{URL::to('backoffice_management/staffs/create')}}">เพิ่ม | Add New</a></td>
              <td align="left"><select name="action_up" class="form-control btn-sm " id="action_up" style="float:right">
                  <option value="" selected="selected">-เลือก-</option>
                  <option value="เผยแพร่ | Publish">อนุญาติให้ใช้งาน</option>
                  <option value="ซ่อน | Unpublish">ระงับการใช้งาน</option>
                  <option value="ลบ | Delete">ลบผู้ดูแลระบบ</option>
                </select></td>
              <td align="left"><input type="submit" name="submit" value="Apply" class="btn btn-primary btn-sm " style="margin-left:5px" /></td>
            </tr>
          </table
    <div class="table-responsive">
          <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="table table-bordered">
            <thead>
              <tr align="center">
                <td width="20%"><strong>User</strong></td>
                <td width="20%"><strong> Email</strong></td>
                <td width="13%"><strong>วันที่สร้าง/วันที่แก้ไข</strong></td>
                <td width="20%"><strong>ดูแลอีเว้นท์</strong></td>
                <td width="10%"><strong>Level</strong></td>
                <td width="12%"><strong>{{$btnTextEdit}}</strong></td>
                <td width="9%"><strong>Func.</strong></td>
              </tr>
            </thead>
            <tbody>  
            <?php   $countData = 0;?>          
            @foreach($Users as $row)
            <?php  $countData = 1;
                 $event_id = trim($row->event_id);
                $status = trim($row->status);
                $username = trim($row->username);
                $email = trim($row->email);
                $create_on = trim($row->create_on);
                $level = trim($row->level);
                $event_name = json_decode($row->event_name);
                $event_name = $event_name->data->th;
                 if($event_id != '9999999'){
            ?>
            <tr>
              <td>{{$username}} @if($status == 'N') <em>(ระงับการใช้งาน)</em> @endif</td>
              <td align="center"> {{$email}}  </td>
              <td align="center">{{date("d F Y", strtotime($create_on)) }}</td>
                 <td align="center">{{$event_name}}</td>
              <td align="center"> @if($level == 'Admin')
                Admin
                @else <em>Staff</em> @endif</td><?php $pathEdit = "backoffice_management/staffs/edit/$row->id";?>
              <td align="center"> 
                <a href="{{URL::to($pathEdit )}}" data-fancybox-type="iframe" class="fancybox-media btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> {{$btnTextEdit}}</a>
              </td>
              <td align="center"><input type="checkbox"   name="id[]"  value="{{$row->id}}"></td>
            </tr>
            <?php }?>
            @endforeach
           
            <?php if($countData == 0){?><tr>
                <td colspan="6" class="text-center">ไม่พบผลลัพธ์</td>
            </tr><?php }?>
              </tbody>            
          </table>
    </div>
    <input type="hidden" name="action" value="UpdateContentsList">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
 {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

 