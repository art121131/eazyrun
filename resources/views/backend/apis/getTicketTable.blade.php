<?php

use App\Http\Controllers\Frontend\ApiContreller;

?>

<table class="table table-bordered" id="table-search">
    <thead>
        <tr> 
            <th>No.</th>
            <th>Order No.</th>
            <th>Ticket No.</th>
            <th>ประเภทตั๋ว</th>
            <th>วันเวลา ชำระเงิน</th>
            <th>ราคาบัตร</th>
            <th>ของที่ระลึก</th>
            <th>ประเภท </th>     
            <th>ชื่อ-นามสกุล</th>
            <th>Bib No.</th>
            <th>เลขบัตรประชาชน</th>
            <th>วันเดือนปี เกิด</th>
            <th>อายุ</th>
            <th>เพศ</th> 
            <th>ประเทศ</th> 
            <th>สัญชาติ</th>  
            <th>อีเมล</th>  
            <th>เบอร์โทร</th> 
            <th>กรุ๊ปเลือด</th> 
            <th>ชื่อ ผู้ติดต่อฉุกเฉิน</th>
            <th>เบอร์โทร ผู้ติดต่อฉุกเฉิน</th>
            <th>ชื่อสมาชิกที่สมัคร</th>
            <th>วิธีการรับของ</th> 
            <th>วิธีชำระเงิน</th> 
            <th>ชื่อผู้ส่ง</th>
            <th>ที่อยู่จัดส่ง</th> 
            <th>จังหวัด</th>
            <th>รหัสไปร์ษณีย์</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $num = 1;
        $chkAll = json_encode($listOrderMonthAll);

        if ($chkAll != '' && $chkAll != '[]') {
            foreach ($listOrderMonthAll as $orderMonth) {
                if ($orderMonth['data'] != '' || $orderMonth['data'] != '[]') {
                    foreach ($orderMonth['data'] as $row_order) {

                        $countriesID = $row_order->countries_id;
                        @$typeTicketEventID = $row_order->type_event_id;
                        $birthday = date('Y-m-d', strtotime($row_order->birthday));
                        $numBirthday = ApiContreller::getAge($birthday);

                        if (( $typeTicketEventID != '0')) {
                            foreach ($tickets as $row_event_type) {
                                if ($row_event_type->ticket_id == $typeTicketEventID) {
                                    if ($eventID == 2) {
                                        @$ticket_name = $row_event_type->ticket_kilometer . 'km.';
                                    } else {
                                        @$ticket_name = json_decode($row_event_type->ticket_title, true);
                                        @$ticket_name = $ticket_name['data']['th'];
                                    }
                                }
                            }

                            if ($row_order->type_souvenir_id != 0) {
                                foreach ($typeSouvenir as $row_souvenir_type) {
                                    if ($row_souvenir_type->souvenir_type_id == $row_order->type_souvenir_id) {
                                        $souvenir_type_name = json_decode($row_souvenir_type->souvenir_type_name, true);
                                        $souvenir_type_name = $souvenir_type_name['data']['th'];
                                        if(!empty($row_souvenir_type->souvenir_id)){
                                            foreach ($souvenir as $row_souvenir){
                                                if($row_souvenir_type->souvenir_id == $row_souvenir->souvenir_id){
                                                    $souvenir_name = json_decode($row_souvenir->souvenir_name, true);
                                                    $souvenir_name = $souvenir_name['data']['th'];
                                                }
                                            }
                                        } else {
                                            $souvenir_name = '-';
                                        }
                                    }
                                }
                            } else {
                                $souvenir_name = '-';
                                $souvenir_type_name = '-';
                            }

                            if ($row_order->type_souvenir_id_vip != 0) {
                                foreach ($typeSouvenir as $row_souvenir_type) {
                                    if ($row_souvenir_type->souvenir_type_id == $row_order->type_souvenir_id_vip) {
                                        $souvenir_type_nameVip = json_decode($row_souvenir_type->souvenir_type_name, true);
                                        $souvenir_type_nameVip = $souvenir_type_nameVip['data']['th'];
                                    }
                                }
                            } else {
                                $souvenir_type_nameVip = '-';
                            }

                            $ticket_id = $row_order->ticket_id;
                            $orderNo = $row_order->order_no;
                            @$ticket_receive_type = $row_order->ticket_receive_type;
                            $orderNo = $row_order->order_no;
                            ?>

                            <tr>
                                <td>{{$num}}</td>
                                <td>'{{$orderNo}}</td>
                                <td>'{{$row_order->ticket_code}}</td>
                                <td>{{@$ticket_name}}</td>
                                <td>{{$row_order->order_resp_on}}</td>
                                <td>{{$row_order->ticket_price}}</td>
                                <td><?php echo @$souvenir_name; ?></td>
                                <td><?php echo @$souvenir_type_name; ?></td>
                                <td><?php echo (!empty($row_order->f_name) && !empty($row_order->l_name)) ? $row_order->f_name . " " . $row_order->l_name : ' - '; ?></td>
                                <td><?php
                                    echo $row_order->bib;
                                    /* $zones = explode(",", $row_order->zone);
                                      $iNum = 1;
                                      foreach ($zones as $zone) {
                                      $zone = trim($zone);
                                      if ($zone == 1) {
                                      echo $iNum . '.ทุนการศึกษาเด็ก <br>';
                                      } else if ($zone == 2) {
                                      echo $iNum . '.อุปกรณ์การแพทย์ ';
                                      }
                                      $iNum++;
                                      } <button type="button" class="btn btn-warning btn-edit" data-id="" id="edit_" >แก้ไขข้อมูล</button> */
                                    ?>
                        <spane class="btn-edit btn-warning" data-id="<?php echo $ticket_id . '-' . $orderMonth['month'] ?>">แก้ไข</spane>
                        </td>
                        <td>'<?php echo $row_order->citizen_id; ?></td>
                        <td><?php echo date('d/m/Y', strtotime($birthday)); ?></td>
                        <td>'<?php echo $numBirthday; ?></td>
                        <td><?php echo $row_order->gender; ?></td>
                        <td><?php
                            if ($countriesID == '216') {
                                echo 'Thailand (ไทย)';
                            } else if ($countriesID != '') {
                                foreach ($countries as $countrie) {
                                    if ($countrie->id == $countriesID) {
                                        echo @$countrie->country_name;
                                    }
                                }
                            }
                            ?>
                        </td>
                        <td><?php echo $row_order->nationality; ?></td>
                        <td><?php echo $row_order->email; ?></td>
                        <td>'<?php echo $row_order->tel; ?></td>
                        <td><?php echo $row_order->blood_group; ?></td>
                        <td><?php echo $row_order->ticket_contact_name; ?></td>
                        <td>'<?php echo $row_order->ticket_contact_tel; ?></td>
                        <td><?php
                            @$orderFname = $row_order->order_fname;
                            if ($orderFname != '' && $row_order->order_lname != '') {
                                echo $orderFname . ' ' . $row_order->order_lname . ' <br>' . $row_order->order_email . '<br> ' . $row_order->order_tel;
                            } else {
                                $member_id = $row_order->member_id;
                                @$rowMember = ApiContreller::getMemberDetail($member_id);
                                if ($member_id != '' && @$rowMember->username != '') {
                                    echo @$rowMember->f_name . ' ' . @$rowMember->l_name . ' <br>' . @$rowMember->username . '<br> ' . @$rowMember->tel;
                                }
                            }
                            ?>
                        </td>
                        <td><?php echo ($row_order->order_type_delivery == 'SEND') ? "จัดส่งที่บ้าน" : "รับเอง"; ?></td>
                        <td>
                            <?php 
                                $type_payment = $row_order->type_payment;
                                if($type_payment == 'paypal'){
                                    echo $textPaymentPaypal;
                                }else if($type_payment == 'WALKIN' && $row_order->psb_channel == 'CASH'){
                                    echo $textPaymentWalkinCash;
                                }else if($type_payment == 'WALKIN' && $row_order->psb_channel == 'CREDIT'){
                                    echo $textPaymentWalkinCredit;
                                }else if($type_payment == 'WALKIN' && $row_order->psb_channel == 'BARCODE'){
                                    echo $textPaymentWalkinBarcode;
                                }else if($type_payment == 'WALKIN' && $row_order->psb_channel == 'OTHER'){
                                    $testOther = $textPaymentWalkinOther." ".$row_order->order_remark;
                                    echo $testOther;
                                }else if($type_payment == '2c2p' && $row_order->psb_channel == 'Credit'){
                                    echo $textPayment2c2pCredit;
                                }else if($type_payment == '2c2p' && $row_order->psb_channel == 'Cash'){
                                    echo $textPayment2c2pCash;
                                }else{
                                    echo $textPayment2c2p;
                                }
                            ?>
                        </td>
                        <td><?php
                            $sub_district_id = $row_order->sub_district_id;
                            $district_id = $row_order->district_id;
                            $province_id = $row_order->province_id;
                            $zipcode = $row_order->zipcode;
                            $name_delivery = $row_order->name_delivery;
                            $address = $row_order->address;

                            echo ($name_delivery != '') ? $name_delivery . '  ' : '-';
                            ?> 
                        </td>
                        <td><?php
                            echo ($address != '') ? $address . ' ' : '';
                            if ($sub_district_id != '') {
                                die($sub_district_id);
                                $DISTRICT = ApiContreller::getFirstDistrict($sub_district_id);
                                echo $DISTRICT->DISTRICT_NAME . ' ';
                            }
                            if ($district_id != '') {
                                $AMPHUR = ApiContreller::getFirstAmphur($district_id);
                                echo $AMPHUR->AMPHUR_NAME . ' ';
                            }
                            ?> 
                        </td>
                        <td><?php
                            if ($province_id != '' && $province_id != '0') {
                                $PROVINCE = ApiContreller::getFirstProvince($province_id);
                                echo $PROVINCE->PROVINCE_NAME . ' ';
                            }
                            ?>
                        </td>
                        <td><?php
                            if ($zipcode != '') {
                                $ZIPCODE = ApiContreller::getFirstZipcode($zipcode);
                                echo $ZIPCODE->ZIPCODE;
                            }
                            ?>
                        </td>
                        </tr>
                        <?php
                        $num++;
                    }
                }
            }
        }
    }
    ?>
    </tbody>
</table>

<script type="text/javascript">
</script> 