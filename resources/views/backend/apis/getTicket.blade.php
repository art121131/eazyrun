 <?php  
  use App\Http\Controllers\Frontend\ApiContreller;
  $rateVatBackend = $events->event_rate_backend;
if($type_payment == 'walkin' || $type_payment == 'promotion'){?><div class="form-group">
    <label class="col-md-3 control-label">เลือกประเภทชำระเงิน</label>
    <div class="col-md-9"> 
    	 <select name="walkin_type_payment" class="form-control" id="walkin_type_payment"  style="width:300px">
	          <option value="" selected="selected">--เลือก/select--</option>
	          <option value="1">เงินสด</option> 
	          <option value="2">บัตรเครดิต</option>     
         </select> 
    </div> 
 </div> 
<?php }else{?> 
<div class="form-group">
    <label class="col-md-3 control-label">เลือกผู้สนันสนุบ</label>
    <div class="col-md-9">
     <select name="order_id" class="form-control required" id="order_id" style="width:300px">
          <option value="">เลือกผู้สนันสนุบ</option> 
          @foreach($OrderVIP as $rowVIP)
           <option value="{{$rowVIP->order_id}}">{{$rowVIP->vip_company}}</option>
           @endforeach
       </select>
    </div> 
  </div>
 <?php if($type_payment == 'vip_2'){?> 
<div class="form-group">
    <label class="col-md-3 control-label">สถานะชำระเงิน</label>
     <div class="col-md-9"> 
  	 	 <select name="status_payment" class="form-control required" id="status_payment"  style="width:300px">
	          <option value="" selected="selected">--เลือก/select--</option>
	          <option value="SUCCESS">ชำระแล้ว</option> 
	          <option value="WAIT">รอการชำระ</option>     
         </select> 
    </div> 
  </div><?php }//End vip_2?>
<?php }//End walkin?>
 <table class="table">
 <tr>
	<th width="70%"></th>
	<th width="20%">ราคา</th> 
	<th width="10%">จำนวน</th> 
</tr>
 @foreach($tickets as $rowTicket)
    <?php 
       $ticketDisplay = true; 
       $ticketStatusSoldout = false;

       @$ticket_name    = json_decode($rowTicket->type_event_title, true);  
       @$type_event_des    = json_decode($rowTicket->type_event_des, true);  

       $ticket_name = $ticket_name['data']['lang_th'];
       $type_event_des = $type_event_des['data']['lang_th'];
       
       $type_event_id = $rowTicket->type_event_id; 
       $type_event_stock = $rowTicket->type_event_stock;

      $type_event_display = $rowTicket->type_event_display; 
     
       //============== Start Count Ticket ===============//
        $ticketSuccess =  ApiContreller::getFuncStockReport($type_event_id, 'SUCCESS'  );
        $ticketWait =  ApiContreller::getFuncStockReport($type_event_id, 'WAIT'  );
        $ticketSucc_Wait = $ticketSuccess+$ticketWait; 
        $ticketTotal = ($type_event_stock-$ticketSucc_Wait); 
      //============== Start Count Ticket ===============//

       $type_event_date_start_on = $rowTicket->type_event_date_start_on;
       $type_event_date_end_on   = $rowTicket->type_event_date_end_on;

       //======= chk type_event_display// 
      if(($type_event_display == 2) && ($dateNow >= $type_event_date_start_on) && ($dateNow <= $type_event_date_end_on)){ 
         $ticketDisplay = true;

      }else if(($type_event_display == 2 && $dateNow < $type_event_date_start_on ) || ($type_event_display == 2 && $dateNow > $type_event_date_end_on ) ){ 
         $ticketDisplay = false;
      }
     //======= end chk type_event_display// 

       if($dateNow >= $type_event_date_start_on && $dateNow <= $type_event_date_end_on && $ticketTotal >= 1 ){// End if chk date
          $ticketStatusSelector = 'OPEN';
       }else if($dateNow < $type_event_date_start_on && $dateNow < $type_event_date_end_on  ){// End if chk date
          $ticketStatusSelector = 'COMINGSOON'; 
       }else  if($dateNow > $type_event_date_end_on ||  $ticketTotal < 1 ){// End if chk date
           $ticketStatusSoldout = true;
           $ticketStatusSelector = 'SOLDOUT';
       }

       $limitSelector = 10;

       if($type_payment == 'promotion'){
            $limitSelector = 1;
       }else if($ticketTotal <= $limitOrderFrontend){ 
           $limitSelector = $ticketTotal;
       }// Check Selector

      //echo $ticketStatusSelector ;
       $priceTicket = $rowTicket->type_event_price; 
       $vat_price = ($priceTicket*$rateVatBackend)/100;
       $total_price = $priceTicket+$vat_price;
       if($ticketDisplay == true){
  ?>
 
 <tr <?php if($ticketStatusSoldout == true){?>style="opacity: 0.4"<?php }?>>
 	<td><b class="title"><strong>{{$ticket_name}}</strong></b></td>
 	<td><p class="price"><?php echo $priceTicket;?>+<?php echo $vat_price;?> = <span class="blue-text"><?php echo $total_price?> THB*</span></p></td>
 	<td><?php if($ticketStatusSelector == 'OPEN'){?>
     <select name="ticket[]" class="form-control selectTicketType" id="{{$type_event_id}}">
      <option value="0" selected="">0</option>
      <?php for ($iSelector=1; $iSelector <= $limitSelector; $iSelector++) {
                 $varNumSelector =  sprintf("%'.02d", $iSelector);
        ?>
      <option value="{{$varNumSelector}}_{{$type_event_id}}">{{$iSelector}}</option> 
      <?php }#end ?>
    </select> 
    <input name="prictTicketTotal[]" type="hidden" value="<?php echo $total_price?>"> <br>
    <?php }else if($ticketStatusSelector == 'COMINGSOON'  ){?>
      <p class="red-text"><strong>OPEN SOON</strong></p>
    <?php }else if( $ticketStatusSelector == 'SOLDOUT' ){?>
       <p class="red-text"><strong>SOLD OUT</strong></p>
    <?php }?></td>
 </tr> <?php }?>
    @endforeach 
</table>
<div id="divTicketDetail"></div>