<?php 
  use App\Http\Controllers\Frontend\ApiContreller;

    $totalAmount = '';
    $totalVat = '';
    $totalAll = '';
    $iNum = 1;
   $user = \Auth::user();
   $user_level = $user->level; 

?>@extends('backend.layout.master') 
@section('titlepage', 'ยอดขายแยกตามสถานะ') 
@section('topScript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.min.css">
@endsection 
@section('content')
 <div class="col-md-12">
          <div class="panel"> 
  <div class="panel-body">
       <div class="col-md-12">
            <div class="panel-cascade">
               
              <div class="row"> 
               @if($event_id == 2)
             
              <table class="table table-striped">
              <thead>
                <tr>
                  <th>ประเภท</th>
                  <th>จำนวนสต๊อกทั้งหมด</th>
                  <th>ชำระเงินเรียบร้อย</th>  
                  <th>คงเหลือ</th> 
              </tr>
              </thead>
              <?php 
               $kmStock100 = 100; 
               $km100 =  ApiContreller::getFuncStockReport(2, 'SUCCESS'  )+ApiContreller::getFuncStockReport(8, 'SUCCESS'  )+ApiContreller::getFuncStockReport(12, 'SUCCESS'  );

               $kmStock70 = 300; 
               $km70 =  ApiContreller::getFuncStockReport(1, 'SUCCESS'  )+ApiContreller::getFuncStockReport(9, 'SUCCESS'  )+ApiContreller::getFuncStockReport(13, 'SUCCESS'  );

               $kmStock35 = 700; 
               $km35 =  ApiContreller::getFuncStockReport(3, 'SUCCESS'  )+ApiContreller::getFuncStockReport(10, 'SUCCESS'  )+ApiContreller::getFuncStockReport(14, 'SUCCESS'  );

               $kmStock13 = 400; 
               $km13 =  ApiContreller::getFuncStockReport(7, 'SUCCESS'  )+ApiContreller::getFuncStockReport(11, 'SUCCESS'  )+ApiContreller::getFuncStockReport(15, 'SUCCESS'  );

              ?>
              <tbody>
                 <tr>
                  <th>100km</th>
                  <th><?php echo number_format($kmStock100, 0, '.', ',');?></th>
                  <th><?php echo number_format($km100, 0, '.', ',');?></th>  
                  <th><?php echo number_format($kmStock100-$km100, 0, '.', ',');?></th>
              </tr> 
              <tr>
                  <th>70km</th>
                  <th><?php echo number_format($kmStock70, 0, '.', ',');?></th>
                  <th><?php echo number_format($km70, 0, '.', ',');?></th>  
                  <th><?php echo number_format($kmStock70-$km70, 0, '.', ',');?></th>
              </tr> 
               <tr>
                  <th>35km</th>
                  <th><?php echo number_format($kmStock35, 0, '.', ',');?></th>
                  <th><?php echo number_format($km35, 0, '.', ',');?></th>  
                  <th><?php echo number_format($kmStock35-$km35, 0, '.', ',');?></th>
              </tr> 
               <tr>
                  <th>13km</th>
                  <th><?php echo number_format($kmStock13, 0, '.', ',');?></th>
                  <th><?php echo number_format($km13, 0, '.', ',');?></th>  
                  <th><?php echo number_format($kmStock13-$km13, 0, '.', ',');?></th>
              </tr> 
              <tr>
                  <th>รวม</th>
                  <th><?php echo number_format($kmStock100+$kmStock70+$kmStock35+$kmStock13, 0, '.', ',');?></th>
                  <th><?php echo number_format($km13+$km35+$km70+$km100, 0, '.', ',');?></th>  
                  <th><?php echo number_format(($kmStock13-$km13)+($kmStock35-$km35)+($kmStock70-$km70)+($kmStock100-$km100), 0, '.', ',');?></th>
              </tr> 
              </tbody>
            </table>

              @endif
               <div class="text-right" style="padding-top:5px"><form class="form-inline" method="get"> 
        <div class="form-group">
          <label>กรองผลลัพท์ </label>
           <div class="form-group">
         <?php #==================================== Start !=  staff ===================================
             if($user_level != 'Staff'){  ?> 
          <select name="event_id" class="form-control" >

                <option value="">--เลือก{{$menuEventName}}--</option> 
                <?php
                   $rowEvents = ApiContreller::getEventLists();
                   foreach ($rowEvents as $rowEvent) {
                      $event_name    = json_decode($rowEvent->event_name, true);
                      $event_name    = $event_name['data']['th'];
                    ?>
                <option value="{{$rowEvent->event_id}}" <?php echo ($event_id == $rowEvent->event_id)? "selected": "";?>  >{{ApiContreller::strCrop($event_name, 22)}}</option>
                <?php }?>
            </select>
          <?php }#==================================== End !=  staff ===================================?>
          </div>
           <div class="form-group">
            <label class="sr-only" for="dateStart">วันที่</label>
            <input type="text" class="form-control" id="dateStart" placeholder="จากวันที่" name="dateStart" autocomplete="off" value="<?php echo $dateStart?>">
            <input type="text" class="form-control" id="dateEnd" placeholder="จนถึงวันที่" name="dateEnd" autocomplete="off" value="<?php echo $dateEnd?>">
          </div>   
          <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>ค้นหา</button> <a href="{{URL::to('backoffice_management/order/stock-report' )}}">ดูทั้งหมด </a>
        </div></form> 
        <br></div>
              <table class="table table-striped">
              <thead>
                <tr>
                  <th>ประเภท</th>
                  <th>จำนวนสต๊อกทั้งหมด</th>
                  <th>ชำระเงินเรียบร้อย (Online)</th> 
                  <th>ชำระเงินเรียบร้อย (Walk-in)</th> 
                  <th>รอการชำระเงิน</th> 
                  <th>รวม</th> 
                  <th>คงเหลือ</th>                
              </tr>
              </thead>
              <tbody>
             	<?php 
             		 $totalStock    = 0;
               		$totalTicketSuccess    = 0;
                        $totalTicketWalkin =0;
               		$totalTicketWait    = 0;
               		$totalTicketTotal    = 0;
               		$totalTicketAvail    = 0;
                  $totalTicketVIP = 0;

             		$rowTickets = ApiContreller::getTicketTypeLists($event_id);
                    foreach ($rowTickets as $rowTicket) {

                       $ticket_id = $rowTicket->ticket_id; 

                      $ticket_name    = json_decode($rowTicket->ticket_title, true); 
                      $ticket_name = $ticket_name['data']['th']; 
   					         
   					          $type_event_stock = $rowTicket->ticket_stock;

                      $ticketSuccess  =  ApiContreller::getFuncStockReport($ticket_id, 'SUCCESS' , $dateStart, 'WALKIN' , $dateEnd, '!=');
                      $ticketWalkin  =  ApiContreller::getFuncStockReport($ticket_id, 'SUCCESS' , $dateStart, 'WALKIN', $dateEnd );
                      $ticketWait     =  ApiContreller::getFuncStockReport($ticket_id, 'WAIT', $dateStart, null, $dateEnd ); 

                      $ticketTotal = $ticketSuccess+$ticketWalkin+$ticketWait; 
                      $ticketAvail = ($type_event_stock-$ticketTotal);

               	?><tr>
                    <th> {{$ticket_name}}</th>
                    <th><?php echo number_format($type_event_stock, 0, '.', ',');?></th>
                    <th><?php echo number_format(@$ticketSuccess, 0, '.', ',');?></th>
                    <th><?php echo number_format(@$ticketWalkin, 0, '.', ',');?></th> 
                    <th><?php echo number_format(@$ticketWait, 0, '.', ',');?></th> 
                    <th><?php echo number_format(@$ticketTotal, 0, '.', ',');?> </th> 
                    <th><?php echo number_format(@$ticketAvail, 0, '.', ',');?> </th> 
                    
                </tr>
                 <?php 
                 		$totalStock     +=   $type_event_stock;
                 		$totalTicketSuccess     +=   $ticketSuccess;
                                $totalTicketWalkin += $ticketWalkin;
                 		$totalTicketWait     +=   $ticketWait;
                 		$totalTicketTotal     +=   $ticketTotal;
                 		$totalTicketAvail     +=   $ticketAvail; 
               
                }// End foreach?> 
                <tr>
                  <th>รวม</th>
                  <th><?php echo number_format($totalStock, 0, '.', ',');?></th>
                  <th><?php echo number_format($totalTicketSuccess, 0, '.', ',');?></th> 
                  <th><?php echo number_format($totalTicketWalkin, 0, '.', ',');?></th> 
                  <th><?php echo number_format($totalTicketWait, 0, '.', ',');?></th>
                  <th><?php echo number_format($totalTicketTotal, 0, '.', ',');?></th>
                 <th><?php echo number_format($totalTicketAvail, 0, '.', ',');?></th>
              </tr>
              </tbody></table>

             
            </div>
            </div>
          </div> 
  </div>
              </div> 
  </div>
@endsection
@section('bottomScript')  
<script src="{{ URL::asset('public/resources/js/bof/jquery-ui-1.10.3.custom.min.js') }}"></script>  
<script type="text/javascript">
jQuery(document).ready(function(){  
      $("#dateStart").datepicker({
          minDate: '2017-08-01',
        maxDate: 0,
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        onClose: function (selectedDate) {
            $("#dateEnd").datepicker("option", "minDate", selectedDate);
        }
    });

    $("#dateEnd").datepicker({
        minDate: '2017-08-01',
        maxDate: 0,
        changeMonth: true,
        dateFormat: 'yy-mm-dd',
        onClose: function (selectedDate) {
            $("#dateStart").datepicker("option", "maxDate", selectedDate);
        }
    });
  });   
</script> 
@endsection 
 