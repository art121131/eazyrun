<?php 
   use App\Http\Controllers\Frontend\ApiContreller; 

   $row_event_type = ApiContreller::getTicketType($TicketForms->type_event_id);
   $ticket_name    = json_decode($row_event_type->ticket_title, true);
   $ticket_name    = $ticket_name['data']['th'];

    //======

     if($TicketForms->type_souvenir_id != 0){
       $row_souvenir_type = ApiContreller::getSouvenirName(  $TicketForms->type_souvenir_id);
       $souvenir_type_name = json_decode($row_souvenir_type->souvenir_type_name, true); 
       $souvenir_type_name = $souvenir_type_name['data']['th']; 

       $row_souvenir = ApiContreller::getCateSouvenir(  $row_souvenir_type->souvenir_id);
       $souvenir_name = json_decode($row_souvenir->souvenir_name, true); 
      $souvenir_name =  $souvenir_name['data']['th']; 
    }else{
      $souvenir_name = '-';  
      $souvenir_type_name = '-'; 
     }
     //==========
      if($TicketForms->gender =="ชาย/Male"){
	      $gender = 'M';
	    }else{
	      $gender = 'F';
	    } 

	     $birthday = date('Y-m-d', strtotime($TicketForms->birthday));
	     $age      =   ApiContreller::getAge($birthday);  

	    // Start group
	    if($TicketForms->type_event_id =="6"){
	       if($gender == 'F'){
	                if($age <=29   ){
	                   $group  = "อายุไม่เกิน 29 ปี";
	                }else if($age <=39){
	                    $group  = "อายุ 30-39 ปี";
	                }else if($age <=49){
	                    $group  = "อายุ 40-49 ปี";
	                }else if($age >= 50){
	                    $group  = "อายุ 50 ปีขึ้นไป";
	                } 
	             }#end gender female
	             if($gender == 'M'){
	                if($age <=29   ){
	                     $group  = "อายุไม่เกิน 29 ปี";
	                }else if($age <=39){
	                     $group  = "อายุ 30-39 ปี";
	                }else if($age <=49){
	                     $group  = "อายุ 40-49 ปี";
	                }else if($age <=59){
	                     $group  = "อายุ 50-59 ปี";
	                }else if($age >= 60){
	                     $group  = "อายุ 60 ปีขึ้นไป";
	                }
	              
	            }#end gender female

	    }else{
	      if($gender == 'F'){
	              if($age <=15   ){
	                 $group  = "อายุไม่เกิน 15 ปี";
	              }else if($age <=39){
	                 $group  = "อายุ 16-39 ปี";
	              }else if($age <=49){
	                 $group  = "อายุ 40-49 ปี";
	               }else if($age <=59){
	                 $group  = "อายุ 50-59 ปี";
	              }else if($age >= 60){
	                $group  = "อายุ 60 ปีขึ้นไป";
	              }
	            
	          }#end gender female

	           if($gender == 'M'){
	              if($age <=15   ){
	                 $group  = "อายุไม่เกิน 15 ปี";
	              }else if($age <=39){
	                 $group  = "อายุ 16-39 ปี";
	              }else if($age <=49){
	                 $group  = "อายุ 40-49 ปี";
	               }else if($age <=59){
	                 $group  = "อายุ 50-59 ปี";
	              }else if($age >= 60){
	                $group  = "อายุ 60 ปีขึ้นไป";
	              }
	          }#end gender female
	    } 
	    // End group  
	    //21/m/34
	    
	    #ticket_print_count;
	   $print_count = $TicketForms->ticket_print_count;
     //==========
?><html>
<head> 
<style type="text/css">
body{padding: 0; margin: 0; font-size: 18px; font-family: tahoma}
.main{width: 7.5cm; margin: auto;  }
.recive_name{ border: 1px solid #000; padding:30px 10px; margin: 5px}
.date{text-align: center; font-size: 12px}
#bcTarget{ text-align: center;  overflow: hidden!important;}
</style>
</head>
<body >
<div class="main img">
 <div>Firstname: <?php echo $TicketForms->f_name;?> </div>
 <div>Lastname: <?php echo $TicketForms->l_name;?></div>
 
 <div>Shirt: <?php  echo   $souvenir_name; ?></div>
 <div>Size: <?php echo $souvenir_type_name;?></div> 
 <div class="recive_name"></div>
 <div class="date">ผู้รับ</div> 
<div id="bcTarget"><img src="https://chart.googleapis.com/chart?chs=150x150&amp;cht=qr&amp;chl=<?php echo $TicketForms->ticket_code;?>&amp;choe=UTF-8" alt="QR code" style="margin-top: -15px;"></div>
<div class="date" style="margin-top: -15px;"><?php echo date("d/m/Y H:i:s")?></div>
<?php if($print_count > 1 ){?><div class="date" style="text-align:center;">Re Print : <?php echo $print_count-1?></div> <?php }?>
</div>

 </div>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/jQuery.2.1.3.js') }}"></script> 
<script type="text/javascript" src="{{ URL::asset('public/resources/js/bof/jquery-barcode.js') }}"></script> 
<script type="text/javascript">
 
</script>
</body>
</html>
 