<?php 
  use App\Http\Controllers\Frontend\ApiContreller;

    $totalAmount = '';
    $totalVat = '';
    $totalAll = '';
    $iNum = 1;
   $user = \Auth::user();
   $user_level = $user->level; 

?>@extends('backend.layout.master') 
@section('titlepage', 'ยอดขายสินค้าเพิ่มเติม') 
@section('topScript')
 @endsection 
@section('content')
 <div class="col-md-12">
          <div class="panel"> 
  <div class="panel-body">
  	 <div class="text-right" style="padding-top:5px"><form class="form-inline" method="get"> 
        <div class="form-group">
	        <label>กรองผลลัพท์ </label>
	         <div class="form-group">
	       <?php #==================================== Start !=  staff ===================================
	           if($user_level != 'Staff'){  ?> 
	        <select name="event_id" class="form-control" >

	              <option value="">--เลือก{{$menuEventName}}--</option> 
	              <?php
	                 $rowEvents = ApiContreller::getEventLists();
	                 foreach ($rowEvents as $rowEvent) {
	                    $event_name    = json_decode($rowEvent->event_name, true);
	                    $event_name    = $event_name['data']['th'];
	                  ?>
	              <option value="{{$rowEvent->event_id}}" <?php echo ($event_id == $rowEvent->event_id)? "selected": "";?>  >{{ApiContreller::strCrop($event_name, 22)}}</option>
	              <?php }?>
	          </select>
	        <?php }#==================================== End !=  staff ===================================?>
	        </div>
	          
          <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>ค้นหา</button> 
        </div></form>
        <br></div>

       <div class="col-md-12">
          <div class="panel-cascade">
             
            <div class="row"> 
            <table class="table table-striped">
              <thead>
                <tr>
                  <th style="width:50%">รายการ</th>  
                  <th>จำนวน</th>  
              </tr>
              </thead>
              <tbody>
                <?php 
                  $AllTotalPaypal = 0; 
                  $AllTotalPSB = 0; 
             

                  $rowProducts = ApiContreller::getProductTypeLists($event_id);
                  foreach ($rowProducts as $rowProduct) {
                      
                    $productMame    = json_decode($rowProduct->product_name, true); 
                    $productMame = $productMame['data']['th']; 
                    $productID = $rowProduct->product_id; 
                
                    $countProduct = ApiContreller::getFuncProductStock($productID,  $event_id );
                ?>
                <tr>
                  <td>{{$productMame}}</td>  
                  <td>{{$countProduct}}</td> 
                </tr>
                <?php
                 /*  $AllTotalPaypal+=$PricePaypal;
                   $AllTotalPSB +=$PricePsb;  */

                 }// End foreach?>
                 <tr>
                  <th>รวม</th>
             
                  <th></th>  
              </tr>  
              </tbody>
            </table>
          </div>
          </div>
        </div> 
  </div>
              </div> 
  </div>
@endsection
@section('bottomScript')  
<script type="text/javascript">
jQuery(document).ready(function(){  
  
  });   
</script> 
@endsection 
 