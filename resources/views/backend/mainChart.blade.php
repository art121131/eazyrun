<?php

use App\Http\Controllers\Frontend\ApiContreller; ?>
@extends('backend.layout.master') 
@section('titlepage', 'หน้าหลัก') 
@section('topScript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.min.css">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>
    .loading {
        margin:auto; 
        margin-top:200px; 
        margin-bottom:200px;
    }
</style>
@endsection 
@section('content')
<div class="col-lg-4 col-md-12 col-sm-12">
    <div class="panel"> 
        <div class="panel-body"> 
            <div class="row" style="padding:10px 10px 0px 10px;">
                <div class="text-center">
                    <div class="col-md-12 col-sm-12">
                        <p style="color: #495057;">ยอดขายวันนี้</p>
                        <h4 id="price-day"><b>Calculating...</b></h4>
                    </div>               
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-12 col-sm-12">
    <div class="panel"> 
        <div class="panel-body"> 
            <div class="row" style="padding:10px 10px 0px 10px;">
                <div class="text-center">
                    <div class="col-md-12 col-sm-12">
                        <p style="color: #495057;">ยอดขายเดือนนี้</p>
                        <h4 id="price-month"><b>Calculating...</b></h4>
                    </div>              
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-12 col-sm-12">
    <div class="panel"> 
        <div class="panel-body"> 
            <div class="row" style="padding:10px 10px 0px 10px;">
                <div class="text-center">
                    <div class="col-md-12 col-sm-12">
                        <p style="color: #495057;">ยอดขายทั้งหมด</p>
                        <h4 id="price-all"><b>Calculating...</b></h4>
                    </div>               
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6 col-md-12 col-sm-12">
    <div class="panel"> 
        <div class="panel-body"> 
            <div class="row" style="padding:10px 10px 0px 10px;">
                <div class="text-left">
                    <div class="col-md-12 col-sm-12">
                        <p style="font-size:16px"><b>เพศ</b></p>
                        <div class="loading text-center">
                            <p><b>Loading...</b></p>
                            <img src="{{URL::asset('public/resources/images/loading-cicle.gif')}}">
                        </div>
                        <canvas id="pieChart" width="400" height="400" style="padding: 50px; display:none;"></canvas>
                    </div>               
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6 col-md-12 col-sm-12">
    <div class="panel"> 
        <div class="panel-body"> 
            <div class="row" style="padding:10px 10px 0px 10px;">
                <div class="text-left">
                    <div class="col-md-12 col-sm-12">
                        <p style="font-size:16px"><b>อายุ</b></p>
                        <div class="loading text-center">
                            <p><b>Loading...</b></p>
                            <img src="{{URL::asset('public/resources/images/loading-cicle.gif')}}">
                        </div>
                        <canvas id="barChart"  width="400" height="400" style="padding: 50px; display:none; "></canvas>
                    </div>               
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('bottomScript')  
<script src="{{ URL::asset('public/resources/js/bof/jquery-ui-1.10.3.custom.min.js') }}"></script> 
<script src="{{ URL::asset('public/resources/js/Chart.js') }}"></script> 
<script type="text/javascript">
jQuery(document).ready(function () {
    
    jQuery.ajax({
       url : `{{URL::to('backoffice_management/main/detial-chart')}}`,
       type:"GET",
       cache: false,   
       success :function(data){ 
           $(".loading").hide()
           $("#barChart, #pieChart").show()
           $('#price-day').text(Intl.NumberFormat('en-US', {minimumFractionDigits: 2}).format(parseFloat(data.price.day)));
           $('#price-month').text(Intl.NumberFormat('en-US', {minimumFractionDigits: 2}).format(parseFloat(data.price.month)));
           $('#price-all').text(Intl.NumberFormat('en-US', {minimumFractionDigits: 2}).format(parseFloat(data.price.all)));
           pieChart(data.gender)
           barChart(data.age)
        }
     });
});
function pieChart(data) {
    var ctx = document.getElementById("pieChart").getContext('2d');
    var male = parseInt(data.male);
    var female = parseInt(data.female);
    var etc = parseInt(data.etc);
    var total = male + female + etc
    var malePercent = male / total * 100
    var femalePercent = female / total * 100
    var etcPercent = etc / total * 100
    var pieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: [`ชาย (${male} คน)`, `หญิง (${female} คน)`, `ไม่ระบุ (${etc} คน)`],
            datasets: [{
                    label: '# of Votes',
                    data: [malePercent, femalePercent, etcPercent],
                    backgroundColor: [
                        'rgba(255, 206, 86, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(188, 188, 188, 1)'
                    ],

                    borderWidth: 1
                }]
        },
        options: {
            legend: {
                labels: {
                    fontColor: 'black',
                    fontSize: 16
                }
            }
        }
    });
}
function barChart(data) {
    var a10 = parseInt(data.a10);
    var a20 = parseInt(data.a20);
    var a30 = parseInt(data.a30);
    var a40 = parseInt(data.a40);
    var a50 = parseInt(data.a50);
    var a60 = parseInt(data.a60);
    var aEtc = parseInt(data.etc);
    var ctx = document.getElementById("barChart").getContext('2d');
    var barChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: [''],
            datasets: [{
                    label: `ต่ำว่า 19 ปี (${a10} คน)`,
                    backgroundColor: 'rgba(255, 206, 86, 1)',
                    data: [a10]
                },{
                    label: `20 - 29 (${a20} คน) `,
                    backgroundColor: 'rgba(54, 162, 235, 1)',
                    data: [a20]
                },{
                    label: `30 - 39 (${a30} คน) `,
                    backgroundColor: 'rgba(75, 192, 192, 1)',
                    data: [a30]
                },{
                    label: `40 - 49 (${a40} คน) `,
                    backgroundColor: 'rgba(255, 159, 64, 1)' ,
                    data: [a40]
                },{
                    label: `50 - 59 (${a50} คน) `,
                    backgroundColor: 'rgba(255, 99, 132, 1)',
                    data: [a50]
                },{
                    label: `60 ขึ้นไป (${a60} คน) `,
                    backgroundColor: 'rgba(153, 102, 255, 1)',
                    data: [a60]
                },{
                    label: `ไม่ระบุ (${aEtc} คน) `,
                    backgroundColor: 'rgba(188, 188, 188, 1)',
                    data: [aEtc]
                }]
        },
        options: {
            scales: {
                xAxes: [{
                        stacked: false,
                         ticks: {
                        beginAtZero:true,
                        responsive: false,
                        maintainAspectRatio: true
                        }
                    }]
            },
            legend: {
                position: 'right',
                labels: {
                    fontColor: 'black',
                    fontSize: 16
                }
            }
        }
    });
}
</script> 
@endsection 
