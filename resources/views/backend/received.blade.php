 <?php  
  use App\Http\Controllers\Frontend\ApiContreller;
 	$user_name = '';
 	$user_id = '';
 	$user_level = '';
  $user = '';
   
 	$user = \Auth::user(); 
	 
	if($user == '' ){ 
		$path = url('/')."/login";
		header("Location:".$path);
		exit();
	}

	  $user_name = $user->name; 
  	$user_id = $user->id; 
 	  $user_level = $user->level;  

    $langTH = true;
    $langEN = "false";
   
?>

@extends('backend.layout.master') 
@section('titlepage', $menuReceived) 
@section('topScript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.min.css">
<style>
    @media (min-width: 768px){
        .input__keyword_order, .input__keyword{
            min-width: 300px;
        }
    }
</style>
@endsection 
@section('content')                 
<div class="col-md-12">
  <div class="panel"> 
    <div class="panel-body"> 
      <div style="width: auto; margin:0 auto 40px; text-align:center "> <br><br>
          <form class="form-inline">
              <div class="form-group">
                <input type="text" class="form-control input__keyword_order" placeholder="Order No.">
            </div>
            <div class="form-group">
                <label>Or&nbsp;</label>
                <input type="text" class="form-control input__keyword" placeholder=" ชื่อ-นามสกุล, เลขบัตรประชาชน, อีเมล" >
            </div> 
        </form> 
      </div>
      <div class="content-table"><p class="text-center">กรุณากรอกคำค้นหา</p></div>
    </div>  
  </div>
</div>

@endsection
@section('bottomScript')
<script type="text/javascript" src="{{ URL::asset('public/resources/js/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/printer.js') }}"></script>
<script type="text/javascript">
$(function() {

  $("[data-toggle='offcanvas']").click(function(e) {
        e.preventDefault();

        //If window is small enough, enable sidebar push menu
        if ($(window).width() <= 992) {
            $('.row-offcanvas').toggleClass('active');
            $('.left-side').removeClass("collapse-left");
            $(".right-side").removeClass("strech");
            $('.row-offcanvas').toggleClass("relative");
        } else {
            //Else, enable content streching
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
            

        }
    });
    //============== ============== ============== ==============
    fnBtnRecive(); 
     function fnBtnRecive(){  
          $('.input__keyword_order').keypress(function (e) {

            var event_id = $('#event_id').val();

          if (e.which == 13) {

           var keyword_order = $(this).val(); 
            if(keyword_order.length >= 2){ 
              jQuery.ajax({
                url : '{{URL::to('backoffice_management/order/received-search-ticket' )}}',
                type:"GET",
                cache: false,
                data : "keyword_order="+keyword_order+"&event_id="+event_id,    
                success :function(data){ 
                    $(".content-table").html(data); 
                    fnBtnRecive();
                    jQuery(".btnPrint").printPage();
                 }
              });
            }
              return false;     
          }
      }); 

      
       $('.input__keyword').keypress(function (e) {

         var event_id = $('#event_id').val(); 
        if (e.which == 13) {

          var keyword = $(this).val(); 
          if(keyword.length >= 2){ 
              jQuery.ajax({
                url : '{{URL::to('backoffice_management/order/received-search-ticket' )}}',
                type:"GET",
                cache: false,
                data : "keyword="+keyword+"&event_id="+event_id,    
                success :function(data){  
                    $(".content-table").html(data); 
                    fnBtnRecive(); 
                    jQuery(".btnPrint").printPage();
                 }
              });
            }
            return false;     
        }
      }); 
         jQuery(".receive_confirm").click(function(){  
        var IDbtnOrder = jQuery(this).attr("id");     
        var ticketID = IDbtnOrder.substring(8); 
        var monthYear = jQuery(this).attr("data-id");  

         bootbox.dialog({
                title: "รับของที่ระลึก?",
                message: '<div class="row">  ' +
                    '<div class="col-md-12"> ' +
                    '<form class="form-horizontal" id="form-confirm"> ' +

                    '<div class="form-group"> ' +
                    '<label class="col-md-4 control-label" for="ticket_receive_type-0">How awesome is this?</label> ' +
                    '<div class="col-md-4"> <div class="radio"> <label for="awesomeness-0"> ' +
                    '<input type="radio" name="ticket_receive_type" id="ticket_receive_type-0" value="1" checked="checked"> ' +
                    'รับด้วยตัวเอง </label> ' +
                    '</div><div class="radio"> <label for="ticket_receive_type-1"> ' +
                    '<input type="radio" name="ticket_receive_type" id="ticket_receive_type-1" value="2"> รับแทน </label> ' +
                    '</div> ' +
                    '</div> </div>' +

                    '<div class="form-group"> ' +
                    '<label class="col-md-4 control-label" for="name">ชื่อผู้รับแทน</label> ' +
                    '<div class="col-md-6"> ' +
                    '<input id="ticket_receive_by" name="ticket_receive_by" type="text" placeholder="Your name" class="form-control input-md"> ' +
                    '<span class="help-block">ชื่อ-นามสกุล  ผู้รับแทน</span> </div> ' +
                    '</div> ' +  
                    '<div class="form-group"> ' +
                    '<label class="col-md-4 control-label" for="name">หมายเหตุ</label> ' +
                    '<div class="col-md-6"> ' +
                    '<textarea id="remark" name="remark" placeholder="Remark" class="form-control input-md" rows="3"></textarea> ' +
                    ' </div></div> ' +  

                    '</form> </div>  </div>',
                buttons: {
                    success: {
                        label: "รับของที่ระลึก",
                        className: "btn-success",
                        callback: function () {
                            //var ticket_receive_by = $('#ticket_receive_by').val();
                            //var ticket_receive_type = $("input[name='ticket_receive_type']:checked").val();
  
                             jQuery.ajax({
                                url : '{{URL::to('backoffice_management/order/received-ticket-submit' )}}',
                                type:"GET",
                                cache: false,
                                //data : "ticketID="+ticketID+"&ticket_receive_by="+ticket_receive_by+"&ticket_receive_type="+ticket_receive_type+"&monthYear="+monthYear, 
                                data : "ticketID="+ticketID+"&monthYear="+monthYear+"&"+$('#form-confirm').serialize(), 
                                success :function(data){ 

                                   $("#receive_"+ticketID).hide();  
                                   $(".div__cancel_"+ticketID).show();
                                   jQuery(".btnPrint").printPage();
                                   $( "#"+ticketID ).trigger( "click" );

                                   
                                 }
                              });
                        }
                    }
                }
            }
        );  
      });  

      jQuery(".receive_cancel").click(function(){  
         var IDbtnOrder = jQuery(this).attr("id");     
         var ticketID = IDbtnOrder.substring(7);
         var monthYear = jQuery(this).attr("data-id"); 
        bootbox.confirm("ยกเลิกรับของที่ระลึก", function(result) {          
            if(result === true){ 
                jQuery.ajax({
                    url : "{{URL::to('backoffice_management/order/cancel-receive' )}}",
                    type:"GET",
                    cache: false,
                    data : "ticketID="+ticketID+"&monthYear="+monthYear,    
                    success :function(data){ 
                        $("#receive_"+ticketID).show();  
                        $(".div__cancel_"+ticketID).hide();
                    }
                });  
            }
        }); 
      });  
   
     }//fnBtnRecive
     //============== ============== ============== ==============
    jQuery(".btnPrint").printPage();

  });
</script>
 @endsection 
