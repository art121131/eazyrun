<?php

use App\Http\Controllers\Frontend\ApiContreller;

$user = \Auth::user();
$user_level = $user->level;
?><div class="table-responsive">
<table  class="table table-striped table-hover table-striped display"  >
    <thead>
        <tr>
            <th>Order No.</th>
            <th>ชื่อ-นามสกุล</th>
            <th>เลขบัตรประชาชน</th> 
            <th>อีเมล</th> 
            <th>ประเภทตั๋ว</th>
            <th>ของที่ระลึก</th> 
            <th class="text-center">สถานะ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($listOrderMonthAll as $row) {
            $data_id = $row['month'];
            if ($row['data'] != '' && $row['data'] != '[]') {
                foreach ($row['data'] as $row_order){
                $row_event_type = ApiContreller::getTicketType($row_order->type_event_id);
                $ticket_name = json_decode($row_event_type->ticket_title, true);
                $ticket_name = $ticket_name['data']['th'];

                //======

                if ($row_order->type_souvenir_id != 0) {
                    $row_souvenir_type = ApiContreller::getSouvenirName($row_order->type_souvenir_id);
                    $souvenir_type_name = json_decode($row_souvenir_type->souvenir_type_name, true);
                    $souvenir_type_name = $souvenir_type_name['data']['th'];

                    $row_souvenir = ApiContreller::getCateSouvenir($row_souvenir_type->souvenir_id);
                    $souvenir_name = json_decode($row_souvenir->souvenir_name, true);
                    $souvenir_name = $souvenir_name['data']['th'];
                } else {
                    $souvenir_name = '-';
                    $souvenir_type_name = '-';
                }
                $ticket_id = $row_order->ticket_id;

                @$ticket_receive_type = $row_order->ticket_receive_type;
                $remark = $row_order->remark;
            
            ?>
            <tr>
                <td><?php echo $row_order->order_no; ?></td>
                <td><?php echo $row_order->f_name; ?> <?php echo $row_order->l_name; ?></td>
                <td><?php echo $row_order->citizen_id; ?></td> 
                <td><?php echo $row_order->email; ?></td> 
                <td><?php echo $ticket_name ?></td>
                <td> <?php echo $souvenir_name; ?>
                    <br><?php echo $souvenir_type_name; ?>
                </td> 
                <td>
                    <button type="button" class="btn btn-danger  receive_confirm" data-id="<?php echo $data_id ?>" id="receive_<?php echo $ticket_id ?>" <?php if ($row_order->recive_souvenir == '1') { ?>style="display:none"<?php } ?>>รับของที่ระลึก</button>
                    <div class="div__cancel div__cancel_<?php echo $ticket_id ?>" <?php if ($row_order->recive_souvenir == '0') { ?>style="display:none"<?php } ?>>
                        <a href="{{URL::to('backoffice_management/order/print-ticket' )}}/<?php echo $ticket_id."/".$data_id ?>" id="<?php echo $ticket_id ?>" class=" btn  btn-primary btnPrint"><span class="fa fa-print" aria-hidden="true"></span> Print</a>
                        <h5> <i class="fa fa-check"></i> รับแล้ว</h5>
                        <?php
                        if ($ticket_receive_type == '1') {
                            echo "รับด้วยตัวเอง";
                        } else if ($ticket_receive_type == '2') {
                            echo "รับแทน<br>" . $row_order->ticket_receive_by;
                        }
                        if(!empty($remark)){
                            echo "<br>".$remark;
                        }
                        ?>
                        <br><button type="button" class="btn btn-warning receive_cancel" data-id="<?php echo $data_id ?>" id="cancel_<?php echo $ticket_id ?>">ยกเลิกรับของที่ระลึก</button>
                    </div> 
                </td> 
            </tr> 
        <?php }}} ?>
    </tbody> 
</table>
    </div>