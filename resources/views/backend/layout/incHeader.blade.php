<nav class="navbar navbar-expand navbar-light bg-white">
    <a class="sidebar-toggle d-flex mr-2" data-toggle="toggle" data-target="#menu-left">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </a>
    <div class="navbar-collapse">
        <ul class="nav-profile">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                    <span>{{$user_name}}&nbsp;<i class="caret"></i></span>
                </a>
                <ul class="dropdown-menu">                  
                    <li class="user-footer"> 
                            <a href="{{URL::to('backoffice_management/staffs/logout' )}}" class="pt-5"><i class="fa fa-sign-out" aria-hidden="true"></i>Sign Out</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
