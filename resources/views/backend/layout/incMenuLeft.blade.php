<?php 
    $per = $user->permission_menu;
    if(!empty($per)){
        $per = json_decode($per);
    }
    $user_level = trim($user_level);
    $url = substr(strstr($_SERVER['REQUEST_URI'], 'backoffice_management/'),22);

?>
<nav class="sidebar" id="menu-left">
    <div class="sidebar-content">
        <a class="sidebar-brand" href="{{(!empty($per->main)&&$per->main==1) ? URL::to('backoffice_management/main' ) : '#'}}" class="logo"> Organizer Center</a>
        <ul class="sidebar-nav">
            <?php if(!empty($per->main)&&$per->main==1){ ?>
            <li class="sidebar-item"><a href="{{URL::to('backoffice_management/main' )}}" class="sidebar-link <?php echo (preg_match('/main/', $url)? 'activeMenu' : '') ?>"><i class="fa fa-home"></i><span class="text-menu">หน้าหลัก</span></a></li>
            <?php }
                if(!empty($per->financial_classify)&&$per->financial_classify==1){ 
            ?>
            <li class="sidebar-item"><a href="{{URL::to('backoffice_management/order/financial-classify' )}}" class="sidebar-link <?php echo (preg_match('/financial-classify/', $url)? 'activeMenu' : '') ?>"><i class="fa fa-file-text-o"></i><span class="text-menu">ยอดขายแยกตามช่องทาง</span></a></li>
            <?php } 
                if(!empty($per->stock_report)&&$per->stock_report==1){
            ?>
            <li class="sidebar-item"><a href="{{URL::to('backoffice_management/order/stock-report' )}}" class="sidebar-link <?php echo (preg_match('/stock-report/', $url)? 'activeMenu' : '') ?>"><i class="fa fa-history"></i><span class="text-menu">ยอดขายแยกตามสถานะ</span></a></li>
            <?php } 
                if(!empty($per->product_stock)&&$per->product_stock==1){
            ?>
            <li class="sidebar-item"><a href="{{URL::to('backoffice_management/order/product-stock' )}}" class="sidebar-link <?php echo (preg_match('/product-stock/', $url)? 'activeMenu' : '') ?>"><i class="fa fa-shopping-cart"></i><span class="text-menu">ยอดขายสินค้าเพิ่มเติม</span></a></li> 
            <?php } 
                if(!empty($per->walkin)&&$per->walkin==1){
            ?>
            <li class="sidebar-item"><a href="{{URL::to('backoffice_management/walkin' )}}" class="sidebar-link <?php echo (preg_match('/walkin/', $url)? 'activeMenu' : '') ?>"><i class="fa fa-pencil-square-o"></i><span class="text-menu">ลงทะเบียน Walk-in</span></a></li> 
            <?php } 
                if(!empty($per->ticket_runner)&&$per->ticket_runner==1){
            ?>
            <li class="sidebar-item"><a href="{{URL::to('backoffice_management/ticket/ticket-runner' )}}" class="sidebar-link <?php echo (preg_match('/ticket-runner/', $url)? 'activeMenu' : '') ?>"><i class="fa fa-users"></i><span class="text-menu">รายชื่อ</span></a></li> 
            <?php } 
                if(!empty($per->import_order)&&$per->import_order==1){
            ?>
            <li class="sidebar-item"><a href="{{URL::to('backoffice_management/import/import-order' )}}" class="sidebar-link <?php echo (preg_match('/import-order/', $url)? 'activeMenu' : '') ?>"><i class="fa fa-upload"></i><span class="text-menu">นำเข้ารายชื่อ</span></a></li> 
            <?php } 
                if(!empty($per->received)&&$per->received==1){
            ?>
            <li class="sidebar-item"><a href="{{URL::to('backoffice_management/order/received' )}}" class="sidebar-link <?php echo (preg_match('/received/', $url)? 'activeMenu' : '') ?>"><i class="fa fa-search"></i><span class="text-menu">รับของที่ระลึก</span></a></li>
            <?php } 
                if(!empty($per->staffs)&&$per->staffs==1){
            ?>
            <li class="sidebar-item"><a href="{{URL::to('backoffice_management/staffs' )}}" class="sidebar-link <?php echo (preg_match('/staffs/', $url)? 'activeMenu' : '') ?>"><i class="fa fa-cog"></i><span class="text-menu">ผู้ใช้งานระบบ</span></a></li> 
            <?php } ?>
        </ul>
    </div>
</nav>
