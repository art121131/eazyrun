 <?php  
 	$user_name = '';
 	$user_id = '';
 	$user_level = '';
    $user = '';
   
 	  $user = \Auth::user(); 
	 
	if($user == '' ){ 
		  $path = url('/')."/login";
		   header("Location:".$path);
		 exit();
	}

	$user_name = $user->name; 
  	$user_id = $user->id; 
 	$user_level = $user->level;  
   
?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Backoffice | {{$metaTitle}}</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link href="{{ URL::asset('public/resources/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('public/resources/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />  
<link href="{{ URL::asset('public/resources/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" /> 
@yield('topScript') 
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<body class="skin-blue">  
 <div class="wrapper">  
    <aside class="right-side" style="margin-left:0"> 
        <section class="content-header">
            <h1>@yield('titlepage') </h1> 
        </section><br>
         <section>
             @yield('content')  
        </section>
    </aside>
</div>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/jQuery.2.1.3.js') }}"></script> 
<script type="text/javascript" src="{{ URL::asset('public/resources/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/backoffice.js') }}"></script>
@yield('bottomScript')
</body>
</html>