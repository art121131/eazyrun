 <?php  
 	$user_name = '';
 	$user_id = '';
 	$user_level = '';
    $user = '';
   
 	  $user = \Auth::user(); 
	 
	if($user == '' ){ 
		$path = url('/')."/login_admin";
		header("Location:".$path);
		exit();
	}

	$user_name = $user->name; 
  	$user_id = $user->id; 
 	$user_level = $user->level;  

    $langTH = true;
    $langEN = "false";
   
?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Backoffice | {{$metaTitle}}</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link href="{{ URL::asset('public/resources/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('public/resources/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('public/resources/css/AdminLTE.css') }}?v=111" rel="stylesheet" type="text/css" />
@yield('topScript') 
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<body class="skin-blue"> 
<div class="wrapper">
    <div class="d-flex">
        @include('backend.layout.incMenuLeft') 
        <div class="main">  
            @include('backend.layout.incHeader')
            <main class="content">
		<div class="container-fluid">
                    <h1 class="h3">@yield('titlepage')</h1>
                    <div class="row">
                     @yield('content')
                    </div>
                </div>
            </main>
        </div>
    </div>
 </div>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/jQuery.2.1.3.js') }}"></script> 
<script type="text/javascript" src="{{ URL::asset('public/resources/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/resources/js/backoffice.js') }}"></script>
<script type="text/javascript">
$(function() {
    
  $("[data-toggle='toggle']").click(function(e) {
        e.preventDefault();

        $('#menu-left').toggleClass('toggled');
        //If window is small enough, enable sidebar push menu
        /*if ($(window).width() <= 992) {
            $('.row-offcanvas').toggleClass('active');
            $('.left-side').removeClass("collapse-left");
            $(".right-side").removeClass("strech");
            $('.row-offcanvas').toggleClass("relative");
        } else {
            //Else, enable content streching
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        }*/
    });
  });
</script>
@yield('bottomScript')
</body>
</html>