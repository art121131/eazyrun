<?php

use App\Http\Controllers\Frontend\ApiContreller; ?>
@extends('backend.layout.master') 
@section('titlepage', 'นำเข้ารายชื่อ') 
@section('topScript')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>
    #toast-container{ top:60px}
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        border: 1px solid #ddd;
        border-radius: 8px;
    }

    .btns {
        border: none;
        padding: 5px 15px;
        border-radius: 0px 8px 8px 0px;
        font-size: 16px;
        font-weight: bold;
        border: 2px solid #5bc0de;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .label-display {
        height: 30px;
        padding: 8px 12px;
        min-width: 300px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: none;
        border-radius: 8px 0px 0px 8px;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        display: inline-block;
    }
    .text-total {
        color: #0080FF;
        font-weight: bold;
    }
    .table-bordered>thead>tr>th {
        text-align: center;
        white-space: nowrap;
    }
    #table-search tbody tr {
        display: none;
    }
    .page-integer {
        color: #3c8dbc;
        border: 1px solid #D1D1D1;
        padding: 1px 3px;
    }
    .active {
        color: #ffffff;
        background-color: #3c8dbc;
    }
    #example-image{
        z-index: 100;
        width: 100%;
        height: 100%;
    }
    .img-blog {
        display: none;
        top: 5%;
        left: 50%;
        transform: translate(-50%, 0);
        position: absolute;
        text-align: right;
        z-index: 200;
    }
    
    .img-ex {
        padding: 5px;
        border: 1px solid gray;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
        box-shadow: 0 3px 3px gray;
        background-color: white;
    }
    
    .link-text {
        cursor:pointer;
        text-decoration: underline;
        color : blue;
    }
    
    .loading {
        margin:auto; 
        margin-top:200px; 
        margin-bottom:200px;
    }
    
    @media (max-width: 768px){
        .form-inline .form-group {
            display: inline-block;
            margin-bottom: 15px;
            width: auto;
            vertical-align: middle;
        }
        .form-inline .form-control {
            display: inline-block;
            width: 100%;
        }
        .form-inline label{
            margin-bottom: 15px;
        }
    }
</style>
@endsection 
@section('content')
<div class="col-md-12">
    <div class="panel"> 
        <div class="panel-body"> 
            <div class="row">
                <div class="text-center" style="padding:5px 0px 10px 0px">
                    <div class="form-inline form-group">
                        <div class="form-group">
                            <div class="upload-btn-wrapper">
                                <input class="label-display" id="label-display"></input>
                                <button class="btns btn-info">เลือกไฟล์</button>
                                <input class="form-control" name="file-upload" id="file-upload" type="file" value="">
                            </div>                        
                        </div>
                    </div>
                    <div class="form-group" id="example-group">
                        <div class="col-md-offset-4 col-md-6 text-left">
                            <p><b>ข้อกำหนดในการอัพโหลดไฟล์</b></p>
                            <p>1. จะต้องเป็นไฟล์สกุล xls หรือ xlsx เท่านั้น</p>
                            <p>2. สามารถอัพเดทได้แค่ Bib No. เท่านั้น</p>
                            <p>
                                3. รูปแบบของเอกสาร คอลัมน์แรกจะต้องเป็น Ticket No. และ คอลัมน์ถัดไปจะเป็น Bib No. 
                                <span class="link-text" id="show-img">ดูตัวอย่าง</span> &nbsp;
                                <span class="link-text" id="load-btn">ดาวน์โหลดไฟล์รายชื่อ</span>
                            </p>
                        </div>
                    </div>  
                    <div class="loading text-center" style="display:none;">
                        <p><b>Loading...</b></p>
                        <img src="{{URL::asset('public/resources/images/loading-cicle.gif')}}">
                    </div>
                    <div class="form-group" id="btn-group">
                        <button type="submit" id="btn-submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> ยืนยัน</button>&nbsp;&nbsp;
                        <button type="submit" id="btn-cancel" class="btn btn-danger"><i class="fa fa-close" aria-hidden="true"></i> ยกเลิก</button>
                    </div>
                </div>
            </div>
            <div id="reviewData"></div>
        </div>
    </div>
</div>
</div>
@endsection
@section('bottomScript')  
<script src="{{ URL::asset('public/resources/js/bof/jquery-ui-1.10.3.custom.min.js') }}"></script> 
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript">
toastr.options = {
    "closeButton": false, "debug": false, "newestOnTop": false, "progressBar": false, "positionClass": "toast-top-center", "preventDuplicates": false, "onclick": null, "showDuration": "3000", "hideDuration": "1000", "timeOut": "1000", "extendedTimeOut": "1000", "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"
}
jQuery(document).ready(function () {
    $('#btn-group').hide()
    $('#example-image').hide()
    $("#load-btn").click(function () {
        window.open("{{URL::to('backoffice_management/import/download-example')}}")
    });
    $('#file-upload').change(function (e) {
        if (e.target.files[0] != undefined) {
            $("#label-display").val(e.target.files[0].name);
            if (e.target.files[0].name.match(/.xls/g)) {
                var file_data = $('#file-upload').prop('files')[0];
                var form_data = new FormData();
                form_data.append('fileUpload', file_data);
                $('#example-group').hide()
                $('.loading').show()
                jQuery.ajax({
                    url: "{{URL::to('backoffice_management/import/import-order-review' )}}",
                    type: "POST",
                    processData: false,
                    contentType: false,
                    data: form_data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data.status.match(/OK/i)) {
                            $('#btn-group').show()
                            $('.loading').hide()
                            var genData = ""
                            if (data.not.length > 0) {
                                genData += `<div class='text-left text-danger' style='padding:5px 0px 10px 10px'><span><b>ไม่พบ จำนวน ${data.not.length} รายชื่อ</b></span></div>`
                                        + `<div class='' id="div-search">`
                                data.not.map((d, i) => {
                                    d = formatNull(d)
                                    genData += `<div class='text-left' style='padding:5px 0px 10px 20px;'><span><b>Ticket No.</b> ${d.A} <b>Bib No.</b> ${d.B}</span></div>`
                                });
                                genData += `</div><br/>`
                            }
                            if (data.found.length > 0) {
                                genData += `<div class='text-left text-total' style='padding:5px 0px 10px 10px'><span>ค้นพบ ทั้งหมด ${data.found.length} รายชื่อ</span></div>`
                                        + `<div class="table-responsive"><table class="table table-bordered" id="table-search"><thead><tr>`
                                        + `<th>No.</th><th>Order No.</th><th>Ticket No.</th><th>ระยะ</th><th>ราคาบัตร</th><th>ของที่ระลึก</th>`
                                        + `<th>ประเภท</th><th>ชื่อ-นามสกุล</th><th>Bib No.</th><th>เลขบัตรประชาชน</th></thead><tbody>`

                                data.found.map((d, i) => {
                                    d = formatNull(d)
                                    d.ticket_title = (d.ticket_title != '') ? JSON.parse(d.ticket_title) : '';
                                    d.souvenir_type_name = (d.souvenir_type_name != '') ? JSON.parse(d.souvenir_type_name) : '';
                                    d.souvenir_name = (d.souvenir_name != '') ? JSON.parse(d.souvenir_name) : '';
                                    genData += `<tr style="display: table-row;"><td>${i + 1}</td><td>${d.order_no}</td><td>${d.ticket_code}</td>`
                                            + `<td>${(d.ticket_title != '' ? d.ticket_title.data.th : '-')}</td>`
                                            + `<td>${d.ticket_price}</td><td>${(d.souvenir_name != '' ? d.souvenir_name.data.th : '-')}</td>`
                                            + `<td>${(d.souvenir_type_name != '' ? d.souvenir_type_name.data.en : '-')}</td><td>${d.f_name} ${d.l_name}</td>`
                                            + `<td>${d.bib}</td><td>${d.citizen_id}</td></tr>`
                                })
                                genData += `</tbody></table></div>`
                            }
                            $("#reviewData").html(genData);
                            addPageInteger()
                        } else {
                            toastr.warning('ไฟล์ไม่ตรงกับรูปแบบข้อกำหนด : กรุณาเลือกไฟล์ใหม่ !')
                            $('.loading').hide()
                            $('#example-group').show()
                            $('#btn-group').hide()
                            $('#reviewData').empty()
                            $("#label-display, #file-upload").val('');
                        }
                    }
                });
            }
        } else {
            $('#example-group').show()
            $('#btn-group').hide()
            $('#reviewData').empty()
            $("#label-display, #file-upload").val('');
        }
    });
    $("#btn-submit").click(function () {
        var file_data = $('#file-upload').prop('files')[0];
        if (file_data != undefined) {
            if (document.getElementById('file-upload').files[0].name.match(/.xls/g)) {
                var form_data = new FormData();
                form_data.append('fileUpload', file_data);
                jQuery.ajax({
                    url: "{{URL::to('backoffice_management/import/import-order-save' )}}",
                    type: "POST",
                    processData: false,
                    contentType: false,
                    data: form_data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data == 1) {
                            toastr.success('Import Success')
                            $('#example-group').show()
                            $('#btn-group').hide()
                            $('#reviewData').empty()
                            $("#label-display, #file-upload").val('');
                        } else {
                            toastr.error('Import Fail !')
                        }
                    }
                });
            } else {
                toastr.warning('ไฟล์ไม่ถูกต้อง : กรุณาเลือกไฟล์ใหม่ !')
            }
        } else {
            toastr.warning('ไม่พบไฟล์ : กรุณาเลือกไฟล์เอกสาร !')
        }
    });

    $("#btn-cancel").click(function () {
        $('#example-group').show()
        $('#btn-group').hide()
        $('#reviewData').empty()
        $("#label-display, #file-upload").val('');
    });
    
    $("#show-img").click(function () {
        $('.wrapper').after('<div id="example-image" class="modal-backdrop in"></div>');
        $('.wrapper').after(`<div class="img-blog">
                <i class="fa fa-times fa-2x" aria-hidden="true" onClick="hideImg()" style="cursor:pointer;"></i><br/>
                <img src="<?php echo URL::asset('public/resources/images/example-excel.png')?>" class="img-ex" alt="Example Excel">
            </div>`);
        $(".img-blog").slideDown(500)
    });

    function formatNull(data) {
        Object.keys(data).forEach(function (key) {
            data[key] = (data[key] != null) ? data[key] : ""
        });
        return data
    }

    function addPageInteger() {
        //-----------Page Integer for table review --------
        $('#table-search').after('<div id="nav"></div>');
        var rowsShown = 100;
        var rowsTotal = $('#table-search tbody tr').length;
        var numPages = rowsTotal / rowsShown;
        for (i = 0; i < numPages; i++) {
            var pageNum = i + 1;
            $('#nav').append('<a href="#" rel="' + i + '" class="page-integer">' + pageNum + '</a> ');
        }
        $('#table-search tbody tr').hide();
        $('#table-search tbody tr').slice(0, rowsShown).show();
        $('#nav a:first').addClass('active');
        $('#nav a').bind('click', function () {
            $('#nav a').removeClass('active');
            $(this).addClass('active');
            var currPage = $(this).attr('rel');
            var startItem = currPage * rowsShown;
            var endItem = startItem + rowsShown;
            $('#table-search tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
                    css('display', 'table-row').animate({opacity: 1}, 300);
        });
        //-----------Page Integer for list not found --------
        $('#div-search').after('<div id="nav2"></div>');
        var rowsShown2 = 20;
        var rowsTotal2 = $('#div-search div').length;
        var numPages2 = rowsTotal2 / rowsShown2;
        for (i = 0; i < numPages2; i++) {
            var pageNum2 = i + 1;
            $('#nav2').append('<a href="#" rel="' + i + '" class="page-integer">' + pageNum2 + '</a> ');
        }
        $('#div-search div').hide();
        $('#div-search div').slice(0, rowsShown2).show();
        $('#nav2 a:first').addClass('active');
        $('#nav2 a').bind('click', function () {
            $('#nav2 a').removeClass('active');
            $(this).addClass('active');
            var currPage2 = $(this).attr('rel');
            var startItem2 = currPage2 * rowsShown2;
            var endItem2 = startItem2 + rowsShown2;
            $('#div-search div').css('opacity', '0.0').hide().slice(startItem2, endItem2).
                    css('display', 'block').animate({opacity: 1}, 300);
        });
    }

});
function hideImg() {
    $(".img-blog").slideUp(500, function () {
        $("#example-image").hide(0);
        $(".img-blog").remove();
        $("#example-image").remove();
    });
}
</script> 
@endsection 
