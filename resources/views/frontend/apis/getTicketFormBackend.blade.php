<?php

use App\Http\Controllers\Frontend\ApiContreller;

@$ticketName = json_decode($tickets->ticket_title, true);
$ticketName = $ticketName['data'][$lang];

@$formField = json_decode($events->event_field_required, true);

$ticketID = $tickets->ticket_id;

$menuMessages = ApiContreller::getMenuMeaasge();

foreach ($menuMessages as $menuMessage) {
    $textDetail = json_decode($menuMessage->text_menu_detail, true);
    $textMenuId = $menuMessage->text_menu_id;

    if ($textMenuId == 21) {
        $textTicketName = $textDetail['data'][$lang];
    }
    if ($textMenuId == 22) {
        $textGender = $textDetail['data'][$lang];
    }
    if ($textMenuId == 23) {
        $textInformationTicket = $textDetail['data'][$lang];
    }
    if ($textMenuId == 26) {
        $textMale = $textDetail['data'][$lang];
    }
    if ($textMenuId == 27) {
        $textFemale = $textDetail['data'][$lang];
    }
}

$pre_name = '';
$f_name = '';
$l_name = '';
$username = '';
$citizen_id = '';
$gender = '';
$birthday = '';
$nationality = 'Thai';
$birthdate = '';
$tel = '';
$blood_group = '';
$country = '';
$religion = '';
$ticket_contact_name = '';
$ticket_contact_tel = '';
?><div id="ticket_{{$ticketBoxID}}_{{$countTicket}}" class="mb-1 col-lg-6 col-md-12 col-sm-12" style="margin-bottom : 10px">
    <div class="card pl-3 pr-3 pt-0 pb-0" >
        <table class="table no-border-top">
            <thead>
                <tr> 
                    <th class="w-50 pl-0"> {{$textTicketName}} {{$ticketNumber}}</th>
                    <th class="pr-0 text-right">{{$ticketName}}</th> 
                </tr>
            </thead>
            <tbody> 
                <tr>  
                    <td class="pr-0 pl-0" colspan="2">

                        <div class="row">
                            <div class="col-md-6">
                                <select name="pre_name[{{$totalPlus}}]" id="pre_name_<?php echo $totalPlus; ?>" class="browser-default required form-control">
                                    <option value="" disabled selected>คำนำหน้า/Title *</option> 
                                    <option value="Mr." <?php echo ($totalPlus == 1 && $pre_name == '1' && $getCount <= 0) ? "selected" : ""; ?>>นาย/Mr.</option>
                                    <option value="Mrs." <?php echo ($totalPlus == 1 && $pre_name == '2' && $getCount <= 0) ? "selected" : ""; ?>>นาง/Mrs.</option>
                                    <option value="Miss" <?php echo ($totalPlus == 1 && $pre_name == '3' && $getCount <= 0) ? "selected" : ""; ?>>นางสาว/Miss</option>
                                </select>
                            </div>  
                            @if($ticketID == 6)
                                <div class="col-md-6">
                                    <input type="text" class="validate form-control eng_only required  is-valid bib" id="member_bib_<?php echo $totalPlus; ?>" name="member_bib[<?php echo $totalPlus; ?>]" placeholder="BIB No. เฉพาะตัวเลข 4 หลัก" maxlength='4'>   
                                </div>
                            @endif
                        </div><br>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="f_name_<?php echo $totalPlus; ?>">ชื่อ/First Name  *</label>
                                <input type="text" class="form-control eng_only required"  name="f_name[{{$totalPlus}}]" id="f_name_<?php echo $totalPlus; ?>" value="<?php echo $f_name ?>" placeholder="ชื่อ/First Name  *">
                            </div>
                            <div class="col-md-6">
                                <label for="l_name_<?php echo $totalPlus; ?>">นามสกุล/Last Name  *</label>
                                <input type="text" class="form-control eng_only required"  name="l_name[{{$totalPlus}}]" id="l_name_<?php echo $totalPlus; ?>" value="<?php echo $l_name ?>" placeholder="นามสกุล/Last Name *">
                            </div> 
                        </div><br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="citizen_id_<?php echo $totalPlus; ?> " for="citizen_id_rs_<?php echo $totalPlus; ?>">เลขบัตรประชาชน/ID Card or Passport No. *</label>
                                <input type="text" class="form-control input_citizen_id pre_type_event required"  name="citizen_id[{{$totalPlus}}]" id="citizen_id_rs_<?php echo $totalPlus; ?>" value="<?php echo $citizen_id ?>"  placeholder="เลขบัตรประชาชน/ID Card or Passport No. *">
                                <div class="citizen_id_rs_<?php echo $totalPlus; ?> error-message-citizen"></div>
                            </div>
                            <div class="col-md-6">
                                <label for="email_<?php echo $totalPlus; ?>">อีเมล/Email  *</label>
                                <input type="email" class="form-control required"  name="email[{{$totalPlus}}]" id="email_<?php echo $totalPlus; ?>" value="<?php echo $username ?>"  placeholder="อีเมล/Email  *">
                            </div> 
                        </div><br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="tel_<?php echo $totalPlus; ?>" >เบอร์มือถือ/Tel. *</label>
                                <input type="tel" class="form-control required"  name="tel[{{$totalPlus}}]" id="tel_rs_<?php echo $totalPlus; ?>" value="<?php echo $tel ?>"  placeholder="เบอร์มือถือ/Tel. *">
                                <div class="tel_rs_<?php echo $totalPlus; ?> error-message-tel"></div>
                            </div>
                            <div class="col-md-6">
                                <label for="birthdate_<?php echo $totalPlus; ?>">วันเกิด/Birthdate *</label>
                                <input type="text" class="form-control required datepicker"  name="birthdate[{{$totalPlus}}]" id="birthdate_<?php echo $totalPlus; ?>" value="<?php echo $birthdate ?>"  placeholder="วันเกิด/Birthdate *">
                            </div> 
                        </div><br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="country_<?php echo $totalPlus; ?>" >ประเทศ/Country *</label>
                                <select name="country[{{$totalPlus}}]" id="country_<?php echo $totalPlus; ?>" class="browser-default required form-control">
                                    <option value="" disabled selected>ประเทศ/Country *</option> 
                                    <?php 
                                    foreach ($countries as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value->id; ?>" <?php echo ($country == $value->id) ? "selected" : ""; ?> ><?php echo $value->country_name; ?></option>
                                    <?php } ?>
                                </select>
                                <!--<input type="text" class="form-control required"  name="country[{{$totalPlus}}]" id="country_<?php echo $totalPlus; ?>" value="<?php echo $country ?>"  placeholder="ประเทศ/Country *">-->
                            </div>
                            <div class="col-md-6"> 
                                <label for="nationality_<?php echo $totalPlus; ?>">สัญชาติ/Nationality *</label>
                                <input type="text" class="form-control required" name="nationality[{{$totalPlus}}]" value='<?php echo $nationality ?>' id="nationality_<?php echo $totalPlus; ?>"  placeholder="สัญชาติ/Nationality *">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6"> 
                                <label for="religion_<?php echo $totalPlus; ?>">ศาสนา/Religion *</label>
                                <input type="text" class="form-control required" name="religion[{{$totalPlus}}]" value='<?php echo $religion ?>' id="religion_<?php echo $totalPlus; ?>"  placeholder="ศาสนา/Religion *">
                            </div>
                            <div class="col-md-6">
                                <label for="blood_group_<?php echo $totalPlus; ?>">กรุ๊ปเลือด/Blood Group *</label>
                                <input type="text" class="form-control required"  name="blood_group[{{$totalPlus}}]" id="blood_group_<?php echo $totalPlus; ?>" value="<?php echo $blood_group ?>"  placeholder="กรุ๊ปเลือด/Blood Group *">
                            </div> 
                        </div> <br>
                        <div class="row">
                            <div class="col-md-6"> 
                                <label for="gender_<?php echo $totalPlus; ?>">เพศ/Gender *</label>
                                <select  name="member_gender[{{$totalPlus}}]"  id="gender_<?php echo $totalPlus; ?>" class="browser-default form-control required">
                                    <option value="" disabled selected>เพศ/Gender *</option>
                                    <option value="ชาย" <?php echo ($totalPlus == 1 && $gender == 'ชาย') ? "selected" : ""; ?>>ชาย/Male</option>
                                    <option value="หญิง" <?php echo ($totalPlus == 1 && $gender == 'หญิง') ? "selected" : ""; ?>>หญิง/Female</option>
                                </select>
                            </div>
                            @if($formField['data']['souvenir'] == 'true')
                            <?php $numSouvenirs = 1; ?>
                            @foreach($souvenirs as $souvenir)
                            <div class="col-md-6" style="overflow: hidden;">
                                <?php
                                $souvenirName = json_decode($souvenir->souvenir_name, true);
                                $souvenirName = $souvenirName['data'][$lang];
                                $souvenirTicketID = $souvenir->souvenir_ticket_id;
                                $ticketSync = 'ID' . $ticketID . 'T';
                                if ($souvenirTicketID == 'ALL' || (strpos($souvenirTicketID, $ticketSync) !== false)) {
                                    ?> 	
                                    <label class="col-md-12 col-sm-12 col-xs-12" for="member_souvenir_type_id_<?php echo $totalPlus; ?>" style="padding-left:0px">{{$souvenirName}}</label>
                                        <label class="radio-hide col-md-12 col-sm-12 col-xs-12">
                                            <input class="required" type="radio" name="member_souvenir_type_id[{{$totalPlus}}]" autocomplete="off" value="" >
                                        </label>
                                        @foreach($typeSouvenir as $rowTypeSouvenir) 
                                        <?php
                                        if ($rowTypeSouvenir->souvenir_id == $souvenir->souvenir_id) {
                                            $souvenirTypeName = json_decode($rowTypeSouvenir->souvenir_type_name, true);
                                            $souvenirTypeName = $souvenirTypeName['data'][$lang];
                                            $souvenirTypeID = $rowTypeSouvenir->souvenir_type_id;
                                            $souvenirTypeStock = $rowTypeSouvenir->souvenir_type_stock;
                                            ?><label class=" ">
                                                <input class="required" type="radio" name="member_souvenir_type_id[{{$totalPlus}}]"  value="<?php echo $souvenirTypeID; ?>"  > {{$souvenirTypeName}}
                                            </label><br>
                                            <?php
                                            $numSouvenirs++;
                                        }
                                        ?>
                                        @endforeach 
                                    <?php } ?>
                                    </div>	
                                    @endforeach  
                                    @endif
                        </div><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="ticket_contact_name_<?php echo $totalPlus; ?>" >ชื่อ ผู้ติดต่อฉุกเฉิน/Contact Name *</label>
                                    <input type="text" class="form-control required"  name="ticket_contact_name[{{$totalPlus}}]" id="ticket_contact_name_<?php echo $totalPlus; ?>" value="<?php echo $ticket_contact_name ?>"  placeholder="ชื่อ ผู้ติดต่อฉุกเฉิน/Contact Name *">
                                </div>
                                <div class="col-md-6"> 
                                    <label for="ticket_contact_tel_<?php echo $totalPlus; ?>">เบอร์โทร ผู้ติดต่อฉุกเฉิน/Contact Tel. *</label>
                                    <input type="text" class="form-control required" name="ticket_contact_tel[{{$totalPlus}}]" value='<?php echo $ticket_contact_tel ?>' id="ticket_contact_tel_<?php echo $totalPlus; ?>"  placeholder="เบอร์โทร ผู้ติดต่อฉุกเฉิน/Contact Tel. *">
                                </div>
                            </div><br>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    <input type="hidden" name="member_ticket_id[{{$totalPlus}}]" value="<?php echo $tickets->ticket_id; ?>">
    <input type="hidden" name="member_ticket_price[{{$totalPlus}}]" value="<?php echo $tickets->ticket_price; ?>"> 
</div>

<script type="text/javascript">
jQuery(document).ready(function () {
    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        language: 'th-TH',
        format: 'yyyy-mm-dd',
    });
});
</script> 