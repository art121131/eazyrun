 <?php
 	use App\Http\Controllers\Frontend\ApiContreller;
  	@$ticketName    = json_decode($tickets->ticket_title, true);
	$ticketName = $ticketName['data'][$lang];


	@$formField    = json_decode($events->event_field_required , true);
	 $formDataArray    = json_decode($events->event_form_additional, true);

	$ticketID = $tickets->ticket_id;

    $menuMessages = ApiContreller::getMenuMeaasge();
    $iNumArr = 1;
    $pre_type_event = 1;

  foreach ($menuMessages as $menuMessage)
  {
    $textDetail = json_decode($menuMessage->text_menu_detail, true);
    $textMenuId =  $menuMessage->text_menu_id;
     if($textMenuId == 46){
      $textEmail =  $textDetail['data'][$lang] ;
    }
     if($textMenuId == 48){
      $textMobile =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 49){
      $textFname =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 50){
      $textLname =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 21){
      $textTicketName =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 22){
      $textGender =  $textDetail['data'][$lang] ;
    }
     if($textMenuId == 23){
      $textInformationTicket =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 26){
      $textMale =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 27){
      $textFemale =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 60){
      $menuFriendList =  $textDetail['data'][$lang] ;
    }
     if($textMenuId == 66){
      $textMemberTitle =  $textDetail['data'][$lang] ;
    }
     if($textMenuId == 68){
      $textCitizenID  =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 69){
      $textBirthDay  =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 70){
      $textAge  =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 71){
      $textCountry  =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 72){
      $textNationality  =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 73){
      $textReligion  =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 74){
      $textEmergencyName  =  $textDetail['data'][$lang] ;
    }
    if($textMenuId == 75){
      $textEmergencyTel  =  $textDetail['data'][$lang] ;
    }
     if($textMenuId == 77){
      $textBloodGroup  =  $textDetail['data'][$lang] ;
    }
      if($textMenuId == 88){
      $textSelect   =  $textDetail['data'][$lang] ;
    }
     if($textMenuId == 89){
      $textMr   =  $textDetail['data'][$lang] ;
    }
     if($textMenuId == 90){
      $textMrs   =  $textDetail['data'][$lang] ;
    }
     if($textMenuId == 91){
      $textMiss   =  $textDetail['data'][$lang] ;
    }
      if($textMenuId == 142){
        $textEngonly =  $textDetail['data'][$lang] ;
      }
  }

  $birthdate = '2010-01-01';
  $country = '216';
  $nationality = 'Thai';

 ?><div id="ticket_{{$ticketBoxID}}_{{$countTicket}}" class="mb-1">
	 <div class="card pl-3 pr-3 pt-0 pb-0" >
	 	<table class="table no-border-top">
	  <thead>
	    <tr>
	      <th class="w-50 pl-0"> <span class="btn btn-secondary btn-sm">{{$textTicketName}} {{$ticketNumber}}</span></th>
	      <th class="pr-0 text-right">{{$ticketName}}</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <td class="pr-0 pl-0" colspan="2">
          @if($friendsCount > 0)
          <div class="mb-2">
           <div class="form-row">
             <div class="col">
               <select name="friend[<?php echo $totalPlus;?>]" id="friend_<?php echo $totalPlus;?>" class="form-control friend_id">
                  <option value="">{{$menuFriendList}}</option>
                  <option value="1">{{ @Session::get('_NAME')}}</option>
                 @foreach($friends as $friend)
                  <option value="{{$friend->friends_id}}">{{$friend->pre_name}} {{$friend->f_name}} {{$friend->l_name}}</option>
                 @endforeach
               </select>
             </div>
           </div>
         </div>
         @endif
	      	 <div class="mb-2">
    				<div class="form-row">
    					 <label class="txt-16 text-secondary" for="pre_name_<?php echo $totalPlus;?>">{{$textMemberTitle}}</label>
    				    <select name="pre_name[<?php echo $totalPlus;?>]" id="pre_name_<?php echo $totalPlus;?>" class="form-control required">
    						<option value="" disabled selected>{{$textSelect}}</option>
    						<option value="Mr.">{{$textMr}}</option>
    						<option value="Mrs.">{{$textMrs}}</option>
    						<option value="Miss">{{$textMiss}}</option>
    					</select>
    				</div>
    			</div>
	      	<div class="mb-2">
	      		<div class="form-row">
				    <div class="col">
				    	<label for="f_name_<?php echo $totalPlus;?>" class="txt-16 text-secondary">{{$textFname}} *</label>
				      <input type="text" class="validate form-control eng_only required" id="f_name_<?php echo $totalPlus;?>" name="member_f_name[<?php echo $totalPlus;?>]" placeholder="{{$textEngonly}} *">
				    </div>
				    <div class="col">
				    	<label for="l_name_<?php echo $totalPlus;?>" class="txt-16 text-secondary">{{ $textLname}} *</label>
				      <input type="text" class="validate form-control eng_only required" id="l_name_<?php echo $totalPlus;?>" name="member_l_name[<?php echo $totalPlus;?>]" placeholder="{{$textEngonly}} *">
				    </div>
				  </div>
				</div>

			   <div class="mb-2">
	      	<div class="form-row">
				    <div class="col">
				    	<label for="citizen_id<?php echo $totalPlus;?>" class="txt-16 text-secondary">{{$textCitizenID}} *</label>
				      	<input type="text" class="validate form-control required citizen_id" id="citizen_id<?php echo $totalPlus;?>" name="citizen_id[<?php echo $totalPlus;?>]" placeholder="{{$textCitizenID}} *">
				   		 <div class="citizen_id<?php echo $totalPlus;?> error-message-citizen text-danger"></div>
				    </div>
				    <div class="col">
				    	<label for="email<?php echo $totalPlus;?>" class="txt-16 text-secondary">{{$textEmail}}  *</label>
				      <input type="email" class="validate form-control  required" id="email<?php echo $totalPlus;?>" name="email[<?php echo $totalPlus;?>]" placeholder="{{$textEmail}} *">
				    </div>
				  </div>
				</div>

				<div class="mb-2">
		      		<div class="form-row">
					    <div class="col">
					    	<label for="tel<?php echo $totalPlus;?>" class="txt-16 text-secondary">{{$textMobile}} *</label>
					      <input type="tel" class="validate form-control required" id="tel<?php echo $totalPlus;?>" name="tel[<?php echo $totalPlus;?>]" placeholder="{{$textMobile}} *">
					    </div>
					    <div class="col">
				    	 <div class="form-group">
						    <label for="blood_group<?php echo $totalPlus;?>" class="txt-16 text-secondary"> {{$textBloodGroup}}</label><br>
						    <input type="text" class="form-control required" id="blood_group<?php echo $totalPlus;?>" name="blood_group[<?php echo $totalPlus;?>]"  placeholder="{{$textBloodGroup}} *">
	 					 </div>
				    </div>
				  </div>
				</div>

				<div class="mb-2">
					<div class="form-row">
 				        <div class="form-group txt-16 text-secondary">
						    <label for="birthdate">{{$textBirthDay}}</label>
				                <?php
				                // echo $birthdate;
				                $birthday_d = date('d', strtotime($birthdate));
				                $birthday_m = date('m', strtotime($birthdate));
				                $birthday_y = date('Y', strtotime($birthdate));
				            ?>
				            <div class="row">
				              <div class='col'>
				                <select name="birthday_d[<?php echo $totalPlus;?>]" class="form-control" id="days_<?php echo $totalPlus;?>">
				                  <option value="" disabled selected>{{$textSelect}}</option>
				                  <?php for($day=1;$day<=31;$day++){?><option value="{{sprintf('%02d', $day) }}" <?php echo ($day == $birthday_d)? "selected" : "" ;?>>{{$day}}</option><?php }?>
				                </select>
				              </div>
				              <div class='col' style="">
				                <select name="birthday_m[<?php echo $totalPlus;?>]" class="form-control" id="months_<?php echo $totalPlus;?>">
				                  <option value="" disabled selected>{{$textSelect}}</option>
				                  <?php for($month=1;$month<=12;$month++){

				                          $monthName = date('M', strtotime('2010-'.$month));
				                          if($lang == 'th'){
				                            $monthNum = date('n', strtotime('2010-'.$month));
				                            $monthName = ApiContreller::getMonthThai($monthNum);
				                          }
				                   ?><option value="{{sprintf('%02d', $month) }}" <?php echo ($month == $birthday_m)? "selected" : "" ;?>>{{$monthName}}</option><?php }?>
				                </select>
				              </div>
				              <div class='col' style="">
				                <select name="birthday_y[<?php echo $totalPlus;?>]" class="form-control birthday_y" id="years_<?php echo $totalPlus;?>">
				                  <option value="" disabled selected>{{$textSelect}}</option>
				                  <?php for($years=2017; $years>=1910; $years--){
				                          $yearsName = $years;
				                          if($lang == 'th'){
				                            $yearsName = $years+543;
				                          }
				                   ?><option value="{{$years}}" <?php echo ($years == $birthday_y)? "selected" : "" ;?>>{{$yearsName}}</option><?php }?>
				                </select>
				              </div>
				            </div>
      	 					</div>
      					</div>
      				</div>

	      		 <div class="mb-2">
		      		<div class="form-row">
					    <div class="col">
					    	<label class="txt-16 text-secondary" for="country<?php echo $totalPlus;?>">{{$textCountry}}</label><br>
						   <select name="country[<?php echo $totalPlus;?>]" class="form-control required" id="country<?php echo $totalPlus;?>">
							<option value="" disabled selected>{{$textCountry}}</option>
							 <?php
							 	 @$Countries = ApiContreller::getCountries();
							 ?>@foreach($Countries as $Countrie)<?php $sel = ($Countrie->id == $country)? "selected=\"selected\"" : "";?><option value="{{$Countrie->id}}" <?php echo $sel; ?>>{{$Countrie->country_name}}</option>@endforeach
						</select>
					    </div>
					    <div class="col">
				    	 <div class="form-group">
						     <label class="txt-16 text-secondary" for="nationality<?php echo $totalPlus;?>">{{$textNationality}}</label><br>
						    <input type="text" class="form-control required" name="nationality[<?php echo $totalPlus;?>]" value='<?php echo $nationality?>' id="nationality<?php echo $totalPlus;?>">
	 					 </div>
				    </div>
				  </div>
				</div>

	      	<div class="mb-2">
      		  <div class="form-row">
				    <div class="col">
				    	 <label class="txt-16 text-secondary" for="religion<?php echo $totalPlus;?>">{{$textReligion}}</label><br>
					     <input type="text" class="form-control required" name="religion[<?php echo $totalPlus;?>]" id="religion<?php echo $totalPlus;?>" value="">
				     </div>
				   </div>
	      		<div class="form-row">
				     <div class="col">
  				    	<h4 class="txt-16 text-secondary">{{$textGender}}</h3>
  			      	<div data-toggle="buttons">
  			      		 <label class="radio-hide"><input class="required" type="radio" name="member_gender[{{$totalPlus}}]" autocomplete="off" value=""></label>
  						  <label class="btn btn-outline-primary" id="div_m_gendar_<?php echo $countTicket;?>">
  						    <input class="required" type="radio" name="member_gender[{{$totalPlus}}]" id="m_gendar_<?php echo $countTicket;?>" autocomplete="off" value="ชาย">{{$textMale}}
  						  </label>
  						  <label class="btn btn-outline-primary" id="div_f_gendar_<?php echo $countTicket;?>">
  						    <input class="required " type="radio" name="member_gender[{{$totalPlus}}]" id="f_gendar_<?php echo $countTicket;?>" autocomplete="off" value="หญิง">{{$textFemale}}
  						  </label>
  						</div>
				    </div>
				  </div>
	      </div>
	      	@if($formField['data']['souvenir'] == 'true')
	      	<div class="mb-2">
	      		<div data-toggle="buttons">
	      		<?php $numSouvenirs = 1;?>
	      		 @foreach($souvenirs as $souvenir)
	       <?php   $souvenirName = json_decode($souvenir->souvenir_name, true);
	               $souvenirName =  $souvenirName['data'][$lang];
	             	 $souvenirTicketID  = $souvenir->souvenir_ticket_id;
                 $souvenirPic = $souvenir->souvenir_pic;
	             	 $ticketSync = 	'ID'.$ticketID.'T';
	             	 if($souvenirTicketID == 'ALL' || (strpos($souvenirTicketID, $ticketSync) !== false && ($ticketID != 20 && $ticketID != 21))){

	        ?><h4 class="txt-16 text-secondary">{{$souvenirName}} @if($souvenirPic != '') <a href="{{ URL::asset('public/resources') }}/uploads/event/{{$souvenirPic}}" target="_blank"><img src="{{ URL::asset('public/resources') }}/images/ic_info.svg" height="20"/></a>@endif</h4><label class="radio-hide"><input class="required" type="radio" name="member_souvenir_type_id[{{$totalPlus}}]" autocomplete="off" value=""></label>
				   @foreach($typeSouvenir as $rowTypeSouvenir)
				   <?php
				   	 if($rowTypeSouvenir->souvenir_id == $souvenir->souvenir_id) {
					   	 $souvenirTypeName  = json_decode($rowTypeSouvenir->souvenir_type_name, true);
		                 $souvenirTypeName  = $souvenirTypeName['data'][$lang];
		                 $souvenirTypeID    = $rowTypeSouvenir->souvenir_type_id;
		                 $souvenirTypeStock = $rowTypeSouvenir->souvenir_type_stock;

	          	?><label class="btn btn-outline-primary"><input class="required" type="radio" name="member_souvenir_type_id[{{$totalPlus}}]" autocomplete="off" value="<?php echo $souvenirTypeID;?>">{{$souvenirTypeName}}</label><?php $numSouvenirs++;
				  		 } ?>
				 	 @endforeach
				 	 <?php }?>
				   @endforeach
				  </div>
	      </div>
	      	@endif
	       	<div class="mb-2">
	      		<div class="form-row">
				    <div class="col">
				    	<label class="txt-16 text-secondary" for="ticket_contact_name<?php echo $totalPlus;?>">{{$textEmergencyName}}</label><br>
					   <input type="text" class="form-control required" name="ticket_contact_name[<?php echo $totalPlus;?>]" id="ticket_contact_name<?php echo $totalPlus;?>" value="">
				     </div>
				     <div class="col">
				    	<label class="txt-16 text-secondary" for="ticket_contact_tel<?php echo $totalPlus;?>">{{$textEmergencyTel}}</label><br>
					   <input type="tel" class="form-control required" name="ticket_contact_tel[<?php echo $totalPlus;?>]" id="ticket_contact_tel<?php echo $totalPlus;?>" value="">
				     </div>
				  </div>
			   </div>
	      	@if($ticketID == 6)
	      	<div class="mb-2">
	      		<h4 class="txt-16 text-secondary">BIB No.</h3>
	      		<div  data-toggle="buttons">
				 	  <div class="col">
				      <input type="text" class="validate form-control eng_only required  is-valid bib" id="member_bib_<?php echo $totalPlus;?>" name="member_bib[<?php echo $totalPlus;?>]" placeholder="เฉพาะตัวเลข 4 หลัก" maxlength='4'>
				    </div>
				   </div>
	      	</div>
	      	@endif
	      	<br>
          <?php
          /*
         //================ Start Form ================
        // ######## ######## ######## ######## ######## ######## ######## ########
        // create forms in html
          //echo json_encode($formDataArray);

          $textGroup = 1;
          if($formDataArray != ''){
          foreach ($formDataArray as $index => $formData) {

            $formID = $formData["id"];
            $formType = $formData["type"];
            $formTitle = $formData["title"];
            $formValue = $formData["value"];
        ?>
        <div id="<?php echo $formID . '[' . $totalPlus . ']';?>" class="div_q">
          <h4 class="txt-16 text-secondary"><?php echo $formTitle;?></h4>
          <?php if ($formType == "radio" || $formType == "checkbox") {?>
           <div class="form-group">
            <?php
             $numInput = 1;
             foreach ($formValue as $index => $value) {
              $id_Input =  $pre_type_event.'_'.$textGroup.'_'.$numInput ;
              if($value != ''){ //dddd
              ?>

                <input class="with-gap" id="{{$id_Input}}" type="<?php echo $formType;?>"  name="<?php echo $formID . '[' . $totalPlus . ']'; if ($formType == 'checkbox') echo '[]';?>" value="<?php echo $value;?>"<?php if ($formType == 'radio' && $index == 0) echo " checked";?>>
                <label for="{{$id_Input}}" class="text-secondary {{$id_Input}}"><?php echo $value;?></label>
              <br>
              <?php $numInput++;  } }?>
              </div>
          <?php } else {?>
          <div class="row">
          <div class=" col s12 l6">
            <input  type="<?php echo $formType;?>"
                name="<?php echo $formID . '[' . $totalPlus . ']';?>"
                placeholder="<?php echo $formValue;?>">
              </div>
          </div>
          <?php }?>
        </div>
        <?php $textGroup++;
             }  //End foreach
          }//End if
          */
         // ######## ######## ######## ######## ######## ######## ######## ########
          ?>
	      </td>
	    </tr>
	  </tbody>
	</table>
	</div>
	<input type="hidden" name="member_ticket_id[{{$totalPlus}}]" value="<?php echo $tickets->ticket_id;?>">
	<input type="hidden" name="member_ticket_price[{{$totalPlus}}]" value="<?php echo $tickets->ticket_price;?>">
	<?php /*<div class="text-secondary txt-14"><img src="{{ URL::asset('public/resources') }}/images/ic_info.svg" height="16"> {{$textInformationTicket}}</div> */?>
</div>
