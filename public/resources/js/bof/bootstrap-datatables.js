// JavaScript Document
/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
    return {
        "iStart":         oSettings._iDisplayStart,
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
}
 
/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {

    "bootstrap": {
        "fnInit": function( oSettings, nPaging, fnDraw ) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function ( e ) {
                e.preventDefault();
                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                    fnDraw( oSettings );
                }
            };
            
            $(nPaging).addClass('pagination  pull-right').append(
                '<ul class="pagination pagination-inverse pull-right">'+
                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
                '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
        },
 
        "fnUpdate": function ( oSettings, fnDraw ) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
 
            if ( oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            }
            else if ( oPaging.iPage <= iHalf ) {
                iStart = 1;
                iEnd = iListLength;
            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }
  
            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                // Remove the middle elements
                $('li:gt(0)', an[i]).filter(':not(:last)').remove();
 
                // Add the new list items and their event handlers
                for ( j=iStart ; j<=iEnd ; j++ ) {
                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                        .insertBefore( $('li:last', an[i])[0] )
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                            fnDraw( oSettings );
                        } );
                }
 
                // Add / remove disabled classes from the static elements
                if ( oPaging.iPage === 0 ) {
                    $('li:first', an[i]).addClass('disabled');
                } else {
                    $('li:first', an[i]).removeClass('disabled');
                }
 
                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                    $('li:last', an[i]).addClass('disabled');
                } else {
                    $('li:last', an[i]).removeClass('disabled');
                }
            }

            //============== ============== ============== 
              jQuery('.switch_options').each(function() {
                
                  //This object
                  var obj = jQuery(this);
          
                  var enb = obj.children('.switch_enable'); //cache first element, this is equal to ON
                  var dsb = obj.children('.switch_disable'); //cache first element, this is equal to OFF
                  var input = obj.children('input'); //cache the element where we must set the value
                  var input_val = obj.children('input').val(); //cache the element where we must set the value
          
                  /* Check selected */
                  if( 0 == input_val ){
                      dsb.addClass('selected');
                  }
                  else if( 1 == input_val ){
                      enb.addClass('selected');
                  }
          
                  //Action on user's click(ON)
                  enb.on('click', function(){
                      jQuery(dsb).removeClass('selected'); //remove "selected" from other elements in this object class(OFF)
                      jQuery(this).addClass('selected'); //add "selected" to the element which was just clicked in this object class(ON) 
                      jQuery(input).val(1).change(); //Finally change the value to 1
                  });
          
                  //Action on user's click(OFF)
                  dsb.on('click', function(){
                    
                      jQuery(enb).removeClass('selected'); //remove "selected" from other elements in this object class(ON)
                      jQuery(this).addClass('selected'); //add "selected" to the element which was just clicked in this object class(OFF) 
                      jQuery(input).val(0).change(); // //Finally change the value to 0
                  }); 
              });  
              jQuery(".switch_options").click(function(){
          //alert("The paragraph was clicked.");
          
          var IDbtnSwitch = jQuery(this).attr("id");
          var productID = IDbtnSwitch.substring(10);    
          var varCheckbox = jQuery("#checkBox_"+productID).val();
          //alert(varCheckbox);
          jQuery.ajax({
                url : 'ajaxjQuery/updateProduct.php',
                type:"POST",
                cache: false,
                data : "productID="+productID+"&varCheckbox="+varCheckbox,    
                success :function(data){        
                   
                  }
              }); //End jQuery.ajax  
          
        });
            //============== ============== ==============


        }
    }
} );
$('document').ready(function(){
	
	//data tablealert('xxxxxxxxxxxx');
	$('#example').dataTable({
		"sPaginationType": "bootstrap"
	});
});