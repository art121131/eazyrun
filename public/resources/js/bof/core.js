// Author: Vijay Kumar
// Template: Cascade - Flat & Responsive Bootstrap Admin Template
// Version: 1.0
// Bootstrap version: 3.0.0
// Copyright 2013 bootstrapguru
// www: http://bootstrapguru.com
// mail: support@bootstrapguru.com
// You can find our other themes on: https://bootstrapguru.com/themes/



// jQuery $('document').ready(); function 
$('document').ready(function(){

	$(document).skylo('start');

	$('.nav-input-search').focus();

  	// Sidebar dropdown
	$('ul.nav-list').accordion();
 	
	$('.settings-toggle').click(function(e){
		e.preventDefault();
		$('.right-sidebar').toggleClass('right-sidebar-hidden');
	});

	$('.left-sidebar .nav > li > ul > li.active').parent().css('display','block');
	
	$('.left-sidebar .nav > li a span').hover(function(){
		var icon=$(this).parent().find('i');
		icon.removeClass('animated shake').addClass('animated shake');
		var wait = window.setTimeout( function(){
			icon.removeClass('animated shake');
			
		},
			1300
		);
	});
	//$(".site-holder").niceScroll({cursorcolor:"#54728c"});  // The document page (html)
	//$(".left-sidebar").niceScroll({cursorcolor:"#54728c"});  // The document page (html)

	 //$(".left-sidebar").niceScroll({cursorcolor:"#54728c"});  // The document page (html)
	 $(".right-sidebar-holder").niceScroll({cursorcolor:"#54728c"});  // The document page (html)


	$('.btn-nav-toggle-responsive').click(function(){
		$('.left-sidebar').toggleClass('show-fullsidebar');
	});
 

		if($('.site-holder').hasClass('mini-sidebar'))
		{    
			$('.sidebar-holder').tooltip({
		      selector: "a",
		      container: "body",
		      placement: "right"
		    });
		    $('li.submenu').tooltip('destroy');
		 }
		 else
		 {
			$('.sidebar-holder').tooltip('destroy');
		 }

	$('.show-info').click(function(){
		$('.page-information').toggleClass('hidden');
	});


	// PANELS

	// panel close
	$('.panel-close').click(function(e){
		e.preventDefault();
		$(this).parent().parent().parent().parent().fadeOut();
	});

	$('.panel-minimize').click(function(e){
		e.preventDefault();
		var $target = $(this).parent().parent().parent().next('.panel-body');
		if($target.is(':visible')) { $('i',$(this)).removeClass('fa-chevron-up').addClass('fa-chevron-down'); }
		else { $('i',$(this)).removeClass('fa-chevron-down').addClass('fa-chevron-up'); }
		$target.slideToggle();
	});
	$('.panel-settings').click(function(e){
		e.preventDefault();
		$('#myModal').modal('show');
	});

	$('.fa-hover').click(function(e){
		e.preventDefault();
		var valued= $(this).find('i').attr('class');
		$('.modal-title').html(valued);
		$('.icon-show').html('<i class="' + valued + ' fa-5x "></i>&nbsp;&nbsp;<i class="' + valued + ' fa-4x "></i>&nbsp;&nbsp;<i class="' + valued + ' fa-3x "></i>&nbsp;&nbsp;<i class="' + valued + ' fa-2x "></i>&nbsp;&nbsp;<i class="' + valued + ' "></i>&nbsp;&nbsp;');
		$('.modal-footer span.icon-code').html('"' + valued + '"');
		$('#myModal').modal('show');
	});


	//Todo List
	$('.finish').click(function(){
		$(this).parent().toggleClass('finished');
		$(this).toggleClass('fa-square-o');
	});

	//Button Print
	$('.btn-print').click(function(){
	 	window.print();
	});
	
	//Faq Toggle 
	$('.faq-list li').click(function(){
		$(this).find('i.fa-plus-square').toggleClass('fa-minus-square');
	});


	
	$(document).skylo('end');

});


//Todo Table
	$('.todo-table i.finish-task').click(function(){
		$(this).toggleClass('fa-square-o');
		$(this).parent().parent().toggleClass('finish');
		
	});

	$('.todo-table .header-row i.finish-task').click(function(){
		$(this).parent().parent().parent().find('tr').toggleClass('finish');
		$(this).parent().parent().parent().find('tr td i.finish-task').toggleClass('fa-square-o');
	});

	$('.todo-table .btn-delete').click(function(){
		confirm('Are you sure to delete this item?');
		$(this).parent().parent().fadeOut();
	});
  
//List Group 
    $(function() {
			$('.sortable').sortable();
			
	});

	$('.demo-list-group a').click(function(){
		$('.demo-list-group a').removeClass('active');
		$(this).addClass('active');
      
	});


	 

		

//left side bar search box
function displayResult(item, val, text) {
    //alert(val);
    //$('.alert').show().php('You selected <strong>' + val + '</strong>: <strong>' + text + '</strong>');
    window.location.replace(val);
}
 