 jQuery(function($){
	
	$('select').material_select();
	
	$('.collapsible').collapsible({
      accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

  $('.matchHeight>div').matchHeight({
    byRow: true,
    property: 'height',
    target: null,
    remove: false
  });
  $(".button-collapse").sideNav();
});		