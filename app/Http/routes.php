<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//==================Frontend ==================
 
//==================Backend ==================
Route::controller('login_admin', 'Backend\LoginController');
Route::controller('api', 'Frontend\ApiContreller');


Route::group(['middleware' => ['web']], function () {
        Route::controller('backoffice_management/main', 'Backend\MainController');
	Route::controller('backoffice_management/staffs', 'Backend\StaffContreller');
	Route::controller('backoffice_management/order', 'Backend\OrderContreller');
	Route::controller('backoffice_management/walkin', 'Backend\WalkinContreller');
	Route::controller('backoffice_management/page', 'Backend\PageContreller');
	Route::controller('backoffice_management/ticket', 'Backend\TicketContreller');
        Route::controller('backoffice_management/import', 'Backend\ImportContreller');
	//Route::get('backoffice_management/genexcel/{event_id}/{typeexport}{type_payment}/{status_payment}/{dateStart}/{dateEnd}', 'Backend\OrderContreller@genExcel');

});
//==================Backend ==================
