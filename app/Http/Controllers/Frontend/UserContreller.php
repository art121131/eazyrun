<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input, Redirect, DB; 
use App\Models\Events;
use App\Models\Ticket;
use App\Models\Souvenir;
use App\Models\TypeSouvenir;
use App\Models\Information;
use App\Models\Banner;
use App\Models\Member;
use App\Models\Friends;

use Session;
use Mail;

class UserContreller extends Controller
{ 
	public function getPreFixPass(){ 
      return 'fsdfsd21243fgf';
    }
    public function getLastPass(){ 
      return 'gdfg434@@??fsdfsdf';
    }

    public function getGenPawWordEaZy($lang = null){ //gen-paw-word-ea-zy

        $genPass =  'REf59efte';
        $_Config_Key_Pre  = $this->getPreFixPass();
        $_Config_Key_Last =  $this->getLastPass();

       $password       = $_Config_Key_Pre.$genPass.$_Config_Key_Last;
     echo  $password       = hash('sha256', $password); 

    }//getGenPawWordEaZy
	public function getLogin($lang = null){ 

		$errMessage = ""; 
		return    \View::make('frontend.loginUser')-> with('lang', $lang)-> with('errMessage', $errMessage);
	}//getLogin

	public function getCreate($lang = null){
		  return    \View::make('frontend.createUser')-> with('lang', $lang);
	}//getLogin



	public function postLogin( ){ 
		$_Config_Key_Pre  = $this->getPreFixPass();
        $_Config_Key_Last =  $this->getLastPass();

       $email          = Input::get('cemail'); 
       $password       = Input::get('password'); 
       $lang          = Input::get('lang'); 
       $password       = $_Config_Key_Pre.$password.$_Config_Key_Last;
       $password       = hash('sha256', $password); 

       //========
        $countMember =  Member::where('member_id','!=', '')
                            ->where('username', $email)
                            ->where('password' , $password)
                            ->select('member_no')
                            ->count();   
         if( $countMember != 0 ){                     
            $Member = Member::where('member_id','!=', '')
                        ->where('username' , $email) 
                        ->where('password' , $password) 
                        ->select('member_no' , 'username', 'f_name', 'l_name'  ) 
                        ->first();

             Session::put('_MEMBERNO',$Member->member_no);
             Session::put('_EMAIL', $Member->username);
             Session::put('_NAME', $Member->f_name.' '.$Member->l_name);
            
            return Redirect::to($lang.'/users/profile'); 
        }else{
            $errMessage = "ERROR"; 
            return    \View::make('frontend.loginUser')-> with('lang', $lang)-> with('errMessage', $errMessage);
              
        }                    
	}//postLogin
    public function postChkLogin( ){ 
        $_Config_Key_Pre  = $this->getPreFixPass();
        $_Config_Key_Last =  $this->getLastPass();

       $email          = Input::get('cemail'); 
       $password       = Input::get('password'); 
       $lang          = Input::get('lang'); 
       $password       = $_Config_Key_Pre.$password.$_Config_Key_Last;
       $password       = hash('sha256', $password); 

       //========
        $countMember =  Member::where('member_id','!=', '')
                            ->where('username', $email)
                            ->where('password' , $password)
                            ->select('member_no')
                            ->count();   
         if( $countMember != 0 ){                     
            $Member = Member::where('member_id','!=', '')
                        ->where('username' , $email) 
                        ->where('password' , $password) 
                        ->select('member_no' , 'username', 'f_name', 'l_name'  ) 
                        ->first();

             Session::put('_MEMBERNO',$Member->member_no);
             Session::put('_EMAIL', $Member->username);
             Session::put('_NAME', $Member->f_name.' '.$Member->l_name);
                
             
            return Redirect::to($lang.'/users/profile'); 
        }else{
            return 'Fail'; 
        }                    

    }//postChkLogin

     public function postChkLoginPayment( ){ 
        $_Config_Key_Pre  = $this->getPreFixPass();
        $_Config_Key_Last =  $this->getLastPass();

       $email          = Input::get('cemail'); 
       $password       = Input::get('password'); 
       $lang          = Input::get('lang'); 
       $password       = $_Config_Key_Pre.$password.$_Config_Key_Last;
       $password       = hash('sha256', $password); 

       //========
        $countMember =  Member::where('member_id','!=', '')
                            ->where('username', $email)
                            ->where('password' , $password)
                            ->select('member_no')
                            ->count();   
         if( $countMember != 0 ){                     
            $Member = Member::where('member_id','!=', '')
                        ->where('username' , $email) 
                        ->where('password' , $password) 
                        ->select('member_no' , 'username', 'f_name', 'l_name'  ) 
                        ->first();

             Session::put('_MEMBERNO',$Member->member_no);
             Session::put('_EMAIL', $Member->username);
             Session::put('_NAME', $Member->f_name.' '.$Member->l_name);
                
             
             return 'SUCCESS'; 
        }else{
            return 'Fail'; 
        }                    

    }//postChkLogin

	public function postConnectFb(){ 
		
		$status = 'FAIL';
		$inputAll = Input::all(); 
		@$email = $inputAll['email'];
		@$firstName = $inputAll['first_name'];
		@$lastName = $inputAll['last_name'];
		@$idFb = $inputAll['id'];
		@$name = $inputAll['name']; 

		if($idFb != '' && $name != ''){ 
		
			if($email == '' || $email == null){

				$user = $this->checkExistUserByFacebookId($idFb); 
	    		if($user == null){
	    			$email = $idFb;
	    		} 
			}else {
	    		 $user = $this->checkExistUserByEmail($email); 
	    		if($user != null){
		    		if($user->member_fb_id == ""){ // update account when not have facebook id. 

	                 Member::where('username', $email)
	                 		->update(array('member_fb_id' => $idFb ,'member_fb_email' => $email   )); 
		    		}
	    		}
	    	}

			 
	   		 if($user != ''){
	   		 	 Member::where('username', $user)
	                 	->update(array('lastlogin_date_on' => date('Y-m-d H:i:s')   )); 
	   		  
	   		 }else{
	   		 	    $Member = new Member(); 
		        
		            $Member->f_name             = $firstName; 
		            $Member->l_name             = $lastName;   
		            $Member->username           = $email; 
		            $Member->member_fb_email    = $email; 
		            $Member->member_fb_id       = $idFb; 
		            $Member->create_date_on     =  date('Y-m-d H:i:s'); 
		            $Member->lastlogin_date_on  =  date('Y-m-d H:i:s');  
		            $Member->save(); //Add to DB  

		             $member =  DB::table('tbl_member')
	                            ->select('member_id')
	                            ->where('username', $email)
	                            ->first();  

	                $memberID = $member->member_id; 
	                $member_no = "C".date("y").$memberID;

	                 Member::where('member_id', $memberID)
	                 		->update(array('member_no' => $member_no   )); 

                     //================================== Sendmail ==================================
                     $data = array(
                         'name' => $firstName.' '.$lastName,
                         'email' => $email,
                         'password' => ''   
                        ); 
                            
                        $user = array(
                          'email'=> $email, 
                          'form_email'=> 'EazyRun Account Information'
                        );

                        Mail::send('emails.memberRegister', $data, function ($message)use ($user) { 
                            $message->from('noreply@eazyrun.com',  $user['form_email']);  
                            $message->to($user['email'], $user['email'])->subject($user['form_email']);  
                        }); 
                     //================================== Sendmail ==================================

	   		 }

            Member::where('username', $email)
                    ->where('member_fb_id', $idFb)
                    ->update(array('lastlogin_date_on' => date('Y-m-d H:i:s')   )); 

	   		 $Member = Member::where('member_id','!=', '')
	                        ->where('username' , $email) 
	                        ->where('member_fb_id' , $idFb) 
	                        ->select('member_no' , 'username', 'f_name', 'l_name'  ) 
	                        ->first();

	         Session::put('_MEMBERNO',$Member->member_no);
	         Session::put('_EMAIL', $Member->username);
	         Session::put('_NAME', $Member->f_name.' '.$Member->l_name);
            
             $status = 'SUCCESS';
		 }//End if
 		return  $status;
	}//postConnectFb

	private function checkExistUserByFacebookId($facebook_id){
 		$user =   Member::where('member_fb_id','=',$facebook_id)->first();
 		return $user;
 	}//checkExistUserByFacebookId

 	private function checkExistUserByEmail($email){
 		$user =  Member::where('username','=',$email)->first();
 		return $user;
 	}//checkExistUserByEmail

    public function getLogout($lang){ 

        Session::forget('_MEMBERNO');
        Session::forget('_EMAIL');
        Session::forget('_NAME'); 

        return Redirect::to($lang.'/main'); 
    }
     public function postChkEmail(){ 
     	$email_user = '';
		$email_host = '';

        $email     = Input::get('email');  

        if ($email == ""){
             return "Please Enter your e-mail";
         }else{ 
            $countMember =  DB::table('tbl_member')
                            ->where('username', $email)
                            ->where('member_level', '=', 'online')
                            ->count();       
             
              if( $countMember > 0 ){   
                
                $msgReturn  = "<span class=\"error\">Email นี้ได้เป็นสมาชิก EazyRun แล้ว <br>ท่านสามารถเข้าสู่ระบบหรือกดลืมรหัสผ่าน/Already Used</span>";
                $msgReturn .= '<input type="hidden" name="error_email" value="error" class="error_email">'; 
                return $msgReturn;
              } else{ 
              	//return "<span class=\"error\">อีเมลนี้ใช้ในการสมัครได้</span>";
               
              }       
            //************
            } //end email ""

    }// End getChkEmail

  
    public function postCreate($lang){ 
    	//echo $land;

    	$_Config_Key_Pre = $this->getPreFixPass();
        $_Config_Key_Last =  $this->getLastPass();
        $url          = Input::get('url'); 
        $email          = Input::get('cemail'); 
        $password       = Input::get('password'); 

        $password       = $_Config_Key_Pre.$password.$_Config_Key_Last;
        $password       = hash('sha256', $password); 
        
        $fname     = Input::get('fname'); 
        $lname     = Input::get('lname'); 

         $GetIP = $_SERVER['REMOTE_ADDR'];

         $countMember =  DB::table('tbl_member')
                            ->where('username', $email)
                            ->count();  

         if( $countMember == 0 && $email != '' && $password != ''   && $fname != ''  && $lname != '' && $url == ''){
         
         	$Member = new Member(); 
         
            $Member->f_name             = $fname; 
            $Member->l_name             = $lname;   
            $Member->username           = $email; 
            $Member->password           = $password;  
       
            $Member->ip                 =  $GetIP; 
            $Member->create_date_on     =  date('Y-m-d H:i:s'); 
            $Member->lastlogin_date_on  =  date('Y-m-d H:i:s');  
            $Member->save(); //Add to DB 

            $member =  DB::table('tbl_member')
                        ->select('member_id')
                        ->where('username', $email)
                        ->where('f_name', $fname)
                        ->first();  

            $memberID = $member->member_id;  

            $member_no = "C".date("y").$memberID;

             Member::where('member_id', $memberID)
             ->update(array('member_no' => $member_no   )); 

              //================================== Sendmail ==================================
             $data = array(
                 'name' => $fname.' '.$lname,
                 'email' => $email,
                 'password' => ''   
                ); 
                    
                $user = array(
                  'email'=> $email, 
                  'form_email'=> 'EazyRun Account Information'
                );

                Mail::send('emails.memberRegister', $data, function ($message)use ($user) { 
                    $message->from('noreply@eazyrun.com',  $user['form_email']);  
                    $message->to($user['email'], $user['email'])->subject($user['form_email']);  
                }); 
             //================================== Sendmail ==================================

            //=======

             Session::put('_MEMBERNO',$member_no);
             Session::put('_EMAIL', $email);
             Session::put('_NAME', $fname.' '.$lname); 

             return Redirect::to($lang.'/users/profile'); 
         }else{
              return Redirect::to($lang.'/users/login'); 
         	//echo 'ซ้ำนะ';
         	exit();
         }                 
    }//postCreate
   
    public function getFormMember($lang){
        
        $filterSelectRegister  = Input::get('filterSelectRegister'); 

        return    \View::make('frontend.apis.getFormMemberPaymentPage')-> with('filterSelectRegister', $filterSelectRegister);
    }//getFormMemberPayment

    public function getFormMemberDetail($lang){
        
        $_MEMBERNO = Session::get('_MEMBERNO');
        $_EMAIL = Session::get('_EMAIL');
        $_NAME = Session::get('_NAME');

        if ($_MEMBERNO == '' || $_EMAIL == ''  || $_NAME == '') { 
            return Redirect::to($lang.'/users/login'); 
            exit();
        }else{  //End  Chk 

             $members = Member::where('member_no' , $_MEMBERNO)
                                ->where('username' , $_EMAIL) 
                                ->first(); 

            return \View::make('frontend.apis.getMemberDetailPayment')-> with('members', $members) ; 
        
        }//End Chk
 
    }//getFormMemberPayment

     public function getHistory($lang){
        //    echo 'history';
         $_MEMBERNO = Session::get('_MEMBERNO');
        $_EMAIL = Session::get('_EMAIL');
        $_NAME = Session::get('_NAME');

        if ($_MEMBERNO == '' || $_EMAIL == ''  || $_NAME == '') { 
           return  Redirect::to($lang.'/users/login');  
           exit();
         }else{ //End  Chk 

            $members = Member::where('member_no' , $_MEMBERNO)
                        ->where('username' , $_EMAIL) 
                        ->first();

             $memberID = $members->member_id;
           //======================== History ===================== //
            $tableNameOrder = 'tbl_order_'.date('y_m');              
            $orders = DB::select("select * from ".$tableNameOrder."
                                  where  member_id = '".$memberID."'  
                                    and (status_payment = 'SUCCESS' or (status_payment = 'WAIT' and psb_channel ='cash') )
                                    and status_payment != 'FAIL'
                                    and event_id != '1'
                                    order by order_id desc ");
                                     
            //======================== History ===================== //

        return \View::make('frontend.history')-> with('lang', $lang)-> with('orders', $orders) ;
    }
     }//getHistory

       public function getProfile($lang){
        //    echo 'history';
          $_MEMBERNO = Session::get('_MEMBERNO');
        $_EMAIL = Session::get('_EMAIL');
        $_NAME = Session::get('_NAME');

        if ($_MEMBERNO == '' || $_EMAIL == ''  || $_NAME == '') { 
            return Redirect::to($lang.'/users/login'); 
            exit();
        }else{  //End  Chk 

             $members = Member::where('member_no' , $_MEMBERNO)
                        ->where('username' , $_EMAIL) 
                        ->first();

                     
         

         return \View::make('frontend.profile')-> with('lang', $lang)
                                                -> with('rowMember', $members) ;
        }
     }//getHistory

        public function getEdit($lang = null){

        $_MEMBERNO = Session::get('_MEMBERNO');
        $_EMAIL = Session::get('_EMAIL');
        $_NAME = Session::get('_NAME');

        if ($_MEMBERNO == '' || $_EMAIL == ''  || $_NAME == '') { 
           return  Redirect::to($lang.'/users/login');  
           exit();
         }else{ //End  Chk 

            $members = Member::where('member_no' , $_MEMBERNO)
                        ->where('username' , $_EMAIL) 
                        ->first();

            return   \View::make('frontend.editUser')-> with('lang', $lang)-> with('rowMember', $members);;

        }
    }//getEdit

    public function postEditProfile($lang){ 
         $_MEMBERNO = Session::get('_MEMBERNO');
        $_EMAIL = Session::get('_EMAIL');
        $_NAME = Session::get('_NAME');

        if ($_MEMBERNO == '' || $_EMAIL == ''  || $_NAME == '') { 
             return  Redirect::to($lang.'/users/login');  
            exit();
        }else{  //End  Chk 
            $_Config_Key_Pre = $this->getPreFixPass();
            $_Config_Key_Last =  $this->getLastPass();

            $GetIP = $_SERVER['REMOTE_ADDR'];
            
            $email          = Input::get('email');  
            $password       = Input::get('password'); 

            $password       = $_Config_Key_Pre.$password.$_Config_Key_Last;
            $password       = hash('sha256', $password); 

            $pre_name     = Input::get('pre_name'); 
            $fname     = Input::get('fname'); 
            $lname     = Input::get('lname'); 
            $citizen_id     = Input::get('citizen_id'); 

            $birthdate     = Input::get('birthdate'); 
            $gender     = Input::get('gender'); 
            $member_age     = Input::get('member_age'); 
            $country     = Input::get('country'); 

            $nationality     = Input::get('nationality'); 

            $blood_group     = Input::get('blood_group'); 
            $religion     = Input::get('religion'); 
            
            $tel     = Input::get('tel'); 
            $address     = Input::get('address'); 
            $province     = Input::get('province'); 
            $district     = Input::get('district'); 
            $sub_district     = Input::get('sub_district'); 
            $zipcode     = Input::get('zipcode'); 
            $changePW     = Input::get('changePW'); 

             Member::where('member_no', $_MEMBERNO)
              ->where('username', $_EMAIL)
              ->update(array('pre_name' => $pre_name ,
                            'f_name' => $fname ,
                            'l_name' => $lname ,
                            'tel' => $tel ,
                            'countries_id' => $country ,
                            'nationalities' => $nationality , 
                            'birthday' => $birthdate,
                            'gender' => Input::get('gender'),
                            'address' => Input::get('address'),
                            'province_id' => $province ,
                            'citizen_id' => $citizen_id , 

                            'blood_group' => $blood_group ,
                            'religion' => $religion ,

                            'district_id' => $district ,
                            'sub_district_id' => $sub_district ,
                            'zipcode' => $zipcode , 

                            'update_date_on' => date("Y-m-d H:i:s")));

              if($changePW == 'YES'){
                  Member::where('member_no', $_MEMBERNO)
                        ->where('username', $_EMAIL)
                        ->update(array('password' => $password , 
                                       'update_date_on' => date("Y-m-d H:i:s")));

              }//End if
             return  Redirect::to($lang.'/users/profile');  
            exit();
        }
     }//End post Edit
    
      public function getOrders($lang, $RETURNINV ){
           
      
        if (Session::get('_MEMBERNO') == '' ||  Session::get('_EMAIL') == ''  || Session::get('_NAME') == '') { 
             return  Redirect::to($lang.'/users/login'); 
            exit();
        }else{  //End  Chk 

             $_MEMBERNO = Session::get('_MEMBERNO');
             $_EMAIL = Session::get('_EMAIL');
             $_NAME = Session::get('_NAME');

          //=============== 

             $members = Member::where('member_no' , $_MEMBERNO)
                        ->where('username' , $_EMAIL) 
                        ->first();

             $memberID = $members->member_id;
            
              $friends = Friends::where('member_id','!=', '')
                              ->where('member_id' , $memberID) 
                              ->get();

           //======================== History ===================== //
           $year = substr($RETURNINV, 0,2);
         $month = substr($RETURNINV, 2,2);

         $dbTableName = 'tbl_order_'.$year.'_'.$month; 
       
          $orders = DB::table($dbTableName) 
                       ->where('order_no',  $RETURNINV) 
                       ->get(); 
          foreach ($orders as $order) {
              $orderID = $order->order_id; 
              $eventID = $order->event_id;
              $memberID = $order->member_id;
          }//foreach   
      
       
         $events = Events::where('tbl_event.event_status', '=', '1')
                        ->where('tbl_event.event_id' , $eventID)  
                        ->leftjoin('tbl_dealer', 'tbl_dealer.dealer_id', '=', 'tbl_event.dealer_id')
                        ->select('tbl_event.event_date_key_finish' ,'tbl_event.event_name' , 'tbl_event.event_url' , 'tbl_event.event_details', 'tbl_event.event_description', 'tbl_event.event_date_on' , 'tbl_event.event_id' ,  'tbl_event.event_status', 'tbl_dealer.dealer_name',
                                'tbl_event.event_place_name', 'tbl_event.event_background', 'tbl_event.event_img' , 'tbl_event.event_logo' ,'tbl_event.event_form_additional'  , 'tbl_event.event_rate_psb', 'tbl_event.event_rate_paypal'  , 'tbl_event.event_ticket_select_limit' )
                        ->orderBy('tbl_event.event_id', 'desc')
                        ->first();
 
         $tableNameTicket = 'tbl_order_ticket_'.$year.'_'.$month; 
         $orderTickets = DB::table($tableNameTicket) 
                                 ->where('order_id' , $orderID) 
                                 ->where('event_id' , $eventID) 
                                 ->get(); 

          $tableNameProduct = 'tbl_order_product_'.$year.'_'.$month; 
          $orderProducts = DB::table($tableNameProduct) 
                                 ->where('order_id' , $orderID) 
                                 ->where('event_id' , $eventID);
          $orderProductCount =   $orderProducts->count(); 
          $orderProducts =   $orderProducts->get();
     

          $members = Member::where('member_id','!=', '')
                              ->where('member_id' , $memberID)
                              ->select(  'f_name', 'l_name', 'username', 'tel'  ) 
                              ->first();

         return    \View::make('frontend.orderDetail')-> with('lang', $lang)
                                                -> with('members', $members)
                                                -> with('events', $events)
                                                -> with('orders', $orders)
                                                -> with('orderTickets', $orderTickets)
                                                -> with('orderProducts', $orderProducts)
                                                -> with('orderProductCount', $orderProductCount)
                                                -> with('friends', $friends) ;               
        }

      }//getOrder

       public function getFriends($lang ){
           
      
            if (Session::get('_MEMBERNO') == '' ||  Session::get('_EMAIL') == ''  || Session::get('_NAME') == '') { 
                 return  Redirect::to($lang.'/users/login'); 
                exit();
            }else{  //End  Chk 

                 $_MEMBERNO = Session::get('_MEMBERNO');
                 $_EMAIL = Session::get('_EMAIL');
                 $_NAME = Session::get('_NAME');
                 
                 $members = Member::where('member_no' , $_MEMBERNO)
                            ->where('username' , $_EMAIL) 
                            ->first();

                 $memberID = $members->member_id;

                 $friends = Friends::where('member_id','!=', '')
                              ->where('member_id' , $memberID) 
                              ->get();
                 return    \View::make('frontend.friendLists')-> with('lang', $lang)-> with('friends', $friends);
            }

        }
     public function getFriendsCreate($lang ){
          
        return    \View::make('frontend.createFriend')-> with('lang', $lang);
     }//getFriendsCreate


      public function postFriendsCreate($lang ){
        //echo $lang;

         if (Session::get('_MEMBERNO') == '' ||  Session::get('_EMAIL') == ''  || Session::get('_NAME') == '') { 
                 return  Redirect::to($lang.'/users/login'); 
                exit();
        }else{  //End  Chk 

             $_MEMBERNO = Session::get('_MEMBERNO');
             $_EMAIL = Session::get('_EMAIL');
             $_NAME = Session::get('_NAME');
             
             $members = Member::where('member_no' , $_MEMBERNO)
                        ->where('username' , $_EMAIL) 
                        ->first();


            $pre_name     = Input::get('pre_name'); 
            $fname     = Input::get('fname'); 
            $lname     = Input::get('lname'); 
            $citizen_id     = Input::get('citizen_id');
            $email     = Input::get('email'); 

            $birthdate     = Input::get('birthdate'); 
            $gender     = Input::get('gender'); 
            $name_delivery     = Input::get('name_delivery'); 
            $country     = Input::get('country'); 

            $nationality     = Input::get('nationality'); 
            $religion     = Input::get('religion'); 
            $tel            = Input::get('tel'); 
            $address     = Input::get('address'); 
            $province     = Input::get('province'); 
            $district     = Input::get('district'); 
            $sub_district     = Input::get('sub_district'); 
            $zipcode     = Input::get('zipcode'); 
 
            $friends = new Friends(); 
            $friends->member_id           = $members->member_id; 
            $friends->pre_name           = $pre_name; 
            $friends->f_name              = $fname; 
            $friends->l_name             = $lname;   
            $friends->email             = $email;  
            $friends->tel                = $tel; 
            $friends->citizen_id         = $citizen_id; 
            $friends->countries_id         = $country; 
            $friends->nationalities         = $nationality; 
            $friends->religion         = $religion; 
            $friends->birthday         = $birthdate;  
            $friends->gender         = $gender;  
            $friends->name_delivery    = $name_delivery; 
            $friends->address         = $address; 
            $friends->province_id         = $province; 
            $friends->district_id         = $district; 
            $friends->sub_district_id    = $sub_district; 
            $friends->zipcode            = $zipcode;  
            $friends->create_date_on     =  date('Y-m-d H:i:s');  
            $friends->save(); //Add to DB 
            
            return  Redirect::to($lang.'/users/friends');  
        }
      } //postFriendsCreate

      public function getFriendsEdit($lang, $friendsID ){

        if (Session::get('_MEMBERNO') == '' ||  Session::get('_EMAIL') == ''  || Session::get('_NAME') == '') { 
                 return  Redirect::to($lang.'/users/login'); 
                exit();
        }else{  //End  Chk 
            $_MEMBERNO = Session::get('_MEMBERNO');
             $_EMAIL = Session::get('_EMAIL');
             $_NAME = Session::get('_NAME');
             
             $members = Member::where('member_no' , $_MEMBERNO)
                        ->where('username' , $_EMAIL) 
                        ->first();

          $friends = Friends::where('friends_id', $friendsID ) 
                        ->where('member_id', $members->member_id) 
                        ->first();

         return   \View::make('frontend.editFriend')-> with('lang', $lang)-> with('friends', $friends);

       }
     }//getFriendsCreate

     public function postFriendsEdit($lang ){
        //echo $lang;

         if (Session::get('_MEMBERNO') == '' ||  Session::get('_EMAIL') == ''  || Session::get('_NAME') == '') { 
                 return  Redirect::to($lang.'/users/login'); 
                exit();
        }else{  //End  Chk 
            $pre_name     = Input::get('pre_name'); 
            $fname     = Input::get('fname'); 
            $lname     = Input::get('lname'); 
            $citizen_id     = Input::get('citizen_id');
            $email     = Input::get('email'); 

            $birthdate     = Input::get('birthdate'); 
            $gender     = Input::get('gender'); 
            $name_delivery     = Input::get('name_delivery'); 
            $country     = Input::get('country'); 

            $nationality     = Input::get('nationality'); 
            $religion     = Input::get('religion'); 
            $tel            = Input::get('tel'); 
            $address     = Input::get('address'); 
            $province     = Input::get('province'); 
            $district     = Input::get('district'); 
            $sub_district     = Input::get('sub_district'); 
            $zipcode     = Input::get('zipcode'); 
            $friend_id     = Input::get('friend_id'); 

              $_MEMBERNO = Session::get('_MEMBERNO');
             $_EMAIL = Session::get('_EMAIL');
             $_NAME = Session::get('_NAME');
             
             $members = Member::where('member_no' , $_MEMBERNO)
                        ->where('username' , $_EMAIL) 
                        ->first();


             Friends::where('friends_id', $friend_id) 
                     ->where('member_id', $members->member_id) 
                      ->update(array('pre_name' => $pre_name ,
                                    'f_name' => $fname ,
                                    'l_name' => $lname ,
                                    'tel' => $tel ,
                                    'countries_id' => $country ,
                                    'nationalities' => $nationality , 
                                    'birthday' => $birthdate,
                                    'gender' =>  $gender,
                                    'address' => $address,
                                    'province_id' => $province , 
                                    'name_delivery' => $name_delivery ,
                                    'email' => $email ,
                                    'religion' => $religion , 
                                    'district_id' => $district ,
                                    'sub_district_id' => $sub_district ,
                                    'zipcode' => $zipcode , 

                                    'update_date_on' => date("Y-m-d H:i:s")));

             return  Redirect::to($lang.'/users/friends');  

        }

    }

     public function postForgetPassword(){ 
        $email       = Input::get('email'); 
        $url       = Input::get('url');  

       $countMember =  Member::where('member_id','!=', '')
                            ->where('username', $email) 
                            ->select('member_id')
                            ->count();   
        
       if( $countMember >  0 ){   
            $_Config_Key_Pre = $this->getPreFixPass();
            $_Config_Key_Last =  $this->getLastPass(); 

            $newPassword = str_random(8);
            $password     = $_Config_Key_Pre.$newPassword.$_Config_Key_Last;
            $password     = hash('sha256', $password); 
       
             Member::where('username', $email)
              ->update(array('password' => $password , 
                            'update_date_on' => date("Y-m-d H:i:s")));

            $Member = Member::where('member_id','!=', '')
                        ->where('username' , $email)  
                        ->select( 'f_name', 'l_name'  ) 
                        ->first();  

             $to_name = $Member['f_name'].' '.$Member['l_name'];
             $newPassword = $newPassword;

            $data = array(
             'to_name' => $to_name , 
             'newPassword' => $newPassword 
          );     
                
            $user = array(
              'email'=> $email, 
              'form_email'=> 'EazyRun Account Information'
            );

            Mail::send('emails.forgetPassword', $data, function ($message)use ($user) { 
                $message->from('noreply@eazyrun.com',  $user['form_email']);  
                $message->to($user['email'], $user['email'])->subject('Eazyrun - ลืมรหัสผ่าน/ Eazyrun - Forget Password');  
            });             

            $status = 'SUCCESS';           
        }else{
            $status = 'FAIL';  
        }//End
        
        return $status;
    }//postForgetPassword 


}
