<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use Input, Redirect, DB;
use App\Models\Events;
use App\Models\Ticket;
use App\Models\Souvenir;
use App\Models\TypeSouvenir;
use App\Models\Information;
use App\Models\Product;
use App\Models\Member;
use App\Models\Friends;
use App\Models\DiscountCode;
use Session;
use Mail;

/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class OrderContreller extends Controller
{
	 public function getPreFixPass(){
      return 'fsdfsd21243fgf';
    }
    public function getLastPass(){
      return 'gdfg434@@??fsdfsdf';
    }

    public function orderPayment($lang){

			$_MEMBERNO = Session::get('_MEMBERNO');
			$_EMAIL = Session::get('_EMAIL');
			$_NAME = Session::get('_NAME');

			if ($_MEMBERNO == '' || $_EMAIL == ''  || $_NAME == '') {
				 return  Redirect::to($lang.'/main');
				 exit();
			 }

      $eventID    = Input::get('eventID');


      $tickets = Ticket::where('ticket_public', '=', '1')
                  ->where('ticket_frontend_public' , '=', '1')
                  ->where('event_id' , $eventID)
                  ->orderBy('ticket_order_by', 'asc')
                  ->get();

      $products = Product::where('product_publish', '=', '1')
                      ->where('event_id' , $eventID)
                      ->orderBy('product_orderby', 'asc');
      $products =  $products->get();

      $inputAll = Input::all();
      $inputAll = json_encode($inputAll);

       $events = Events::where('tbl_event.event_status', '=', '1')
                    ->where('tbl_event.event_id' , $eventID)
                    ->leftjoin('tbl_dealer', 'tbl_dealer.dealer_id', '=', 'tbl_event.dealer_id')
                    ->orderBy('tbl_event.event_id', 'desc')
                    ->first();


      //=================== Add Database Start===========================//
       $t       = microtime(true);
       $micro   = sprintf("%03d",($t - floor($t)) * 10000);

       $orderNumber     = date('ymdhis').$micro;
       $orderSess =  str_random(12);

         //******************* Add order Start *******************//
         $tableNameOrder = 'tbl_order_'.date('y_m'); // tbl_order_17_08 tbl_order_2017_08

         DB::table($tableNameOrder)->insert(
              ['status_payment' => 'ADDTICKET',
               'type_payment'   => '',
               'order_session'  => $orderSess,
               'order_no'  => $orderNumber,
               'event_id'       => $eventID,
               'member_id'      => 0,
               'order_create_on' => date("Y-m-d H:i:s"),
               'order_resp_on' => date("Y-m-d H:i:s")
               ]
          ); //Add to database
         /*  */
           $Order = DB::table($tableNameOrder)
                          ->where('order_session', $orderSess)
                          ->select('order_id' )
                          ->first();
            $orderID = $Order->order_id;
         //******************* Add order End  *******************//

         //******************* Add ticket Start *******************//
         $memberTicketID = Input::get('member_ticket_id');

        $memberFname = Input::get('member_f_name');
        $memberLname = Input::get('member_l_name');
        $memberGender = Input::get('member_gender');
        $memberSouvenirTypeID = Input::get('member_souvenir_type_id');
        $memberSouvenirTypeVIPID = Input::get('member_souvenir_type_id_vip');
        $memberTicketPrice = Input::get('member_ticket_price');
        $memberTicketID = Input::get('member_ticket_id');
        $memberBIB = Input::get('member_bib');
        $memberZone = Input::get('member_zone');
        $memberZone2 = Input::get('member_zone2');
        $memberCitizenID = Input::get('citizen_id');
        $memberEmail = Input::get('email');
        $memberBloodGroup     = Input::get('blood_group');
        $memberPreName     = Input::get('pre_name');

        $birthday_d     = Input::get('birthday_d');
        $birthday_m     = Input::get('birthday_m');
        $birthday_y     = Input::get('birthday_y');
        $country     = Input::get('country');
        $religion     = Input::get('religion');
        $nationality     = Input::get('nationality');

        $ticket_contact_name     = Input::get('ticket_contact_name');
        $ticket_contact_tel     = Input::get('ticket_contact_tel');
        $tel     = Input::get('tel');

          $tableNameTicket = 'tbl_order_ticket_'.date('y_m');

          $orderTicketPrice = 0 ;
          for($numTicket=0; $numTicket <=  20; $numTicket++) {
             if(@$memberTicketID[$numTicket] != ''){
                   $t       = microtime(true);
                   $micro   = sprintf("%03d",($t - floor($t)) * 10000);

                   $ticketRefNo  = 'T'.date('ymdhis').$micro.sprintf("%02d",$eventID );


                   $birthdayAll = @$birthday_y[$numTicket].'-'.@$birthday_m[$numTicket].'-'.@$birthday_d[$numTicket];

                  DB::table($tableNameTicket)->insert(
                      ['order_id'     => $orderID,
                       'event_id'     => $eventID,
                       'ticket_price'       => @$memberTicketPrice[$numTicket],
                       'type_event_id'       => @$memberTicketID[$numTicket],
                       'type_souvenir_id'       => @$memberSouvenirTypeID[$numTicket],
                       'type_souvenir_id_vip'       => @$memberSouvenirTypeVIPID[$numTicket],

                       'ticket_contact_tel'         => @$ticket_contact_tel[$numTicket],
                       'ticket_contact_name'         => @$ticket_contact_name[$numTicket],

                       'blood_group'         => @$memberBloodGroup[$numTicket],
                       'pre_name'         => @$memberPreName[$numTicket],
                       'tel'         => @$tel[$numTicket],

                       'countries_id'         => @$country[$numTicket],
                       'nationality'    =>  @$nationality[$numTicket],
                       'religion'    =>  @$religion[$numTicket],

                       'ticket_code'       => @$ticketRefNo,
                       'f_name'         => @$memberFname[$numTicket],
                       'bib'          => @$memberBIB[$numTicket],

                       'l_name'         => @$memberLname[$numTicket],
                       'gender'         => @$memberGender[$numTicket],
                       'citizen_id'      => @$memberCitizenID[$numTicket],
                       'birthday'       =>  @$birthdayAll,
                       'email'          =>  @$memberEmail[$numTicket],

                       'form_additional' =>  @$memberFormAdditional[$numTicket],
                       ]
                  );
                  //echo  @$memberTicketID[$numTicket].'<br>';

                  $orderTicketPrice  = $orderTicketPrice+@$memberTicketPrice[$numTicket] ;
             }

          }//End for
          /* */
         //******************* Add ticket End  *******************//

         //******************* Add product Start *******************//

           $memberProductID    = Input::get('member_product_id');
           $memberProductPrice = Input::get('member_product_price');
           $memberProductCate  = Input::get('product_cate');

           $tableNameProduct = 'tbl_order_product_'.date('y_m'); //tbl_order_product_17_08
           $countMin = 0 ;
           $orderProductPrice = 0 ;
           for($numTicket=0; $numTicket <=  20; $numTicket++) {
                if(@$memberProductID[$numTicket] != ''){
                       $countMin = 1;
                      $t       = microtime(true);
                      $micro   = sprintf("%03d",($t - floor($t)) * 10000);

                      $productRefNo  = 'T'.date('ymdhis').$micro.sprintf("%02d",$eventID );

                     DB::table($tableNameProduct)->insert(
                      ['order_id'     => $orderID,
                       'event_id'     => $eventID,
                       'product_code'     => $productRefNo,
                       'product_cate'   => @$memberProductCate[$numTicket],
                       'product_id'   => @$memberProductID[$numTicket],
                       'order_price'  => @$memberProductPrice[$numTicket]
                       ]
                  );

                  $orderProductPrice  = $orderProductPrice+@$memberProductPrice[$numTicket]  ;
                }//End if

           }//end for

           $orderPrice = $orderProductPrice+$orderTicketPrice;
           if($countMin == 1){
              $priceDeliveryProduct =  (60+(count($memberProductID)-1)*35);
           }else{
              $priceDeliveryProduct = 0;
           }


         //******************* Add product End *******************//

      //=================== Add Database End ============================//

       return  \View::make('frontend.payment')-> with('lang', $lang)
                                              -> with('orderSess', $orderSess)
                                              -> with('orderNumber', $orderNumber)
                                              -> with('priceDeliveryProduct', $priceDeliveryProduct)
                                              -> with('events', $events)
                                              -> with('orderPrice', $orderPrice)
                                              -> with('tickets', $tickets)
                                              -> with('products', $products)
                                              -> with('inputAll', $inputAll);

    }//paymentOrder


}
