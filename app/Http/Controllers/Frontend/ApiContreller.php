<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input, Redirect, DB;
use App\Models\Events;
use App\Models\Ticket;
use App\Models\Souvenir;
use App\Models\TypeSouvenir;
use App\Models\Member;
use App\Models\Product;
use App\Models\Friends;
use App\Models\TextMenu;
use App\Models\Lang;


class ApiContreller extends Controller
{
    public function postTicketForm($event_id){   //Get event Detail
           $_MEMBERNO = Session::get('_MEMBERNO');
        $_EMAIL = Session::get('_EMAIL');
        $_NAME = Session::get('_NAME');

        if ($_MEMBERNO == '' || $_EMAIL == ''  || $_NAME == '') {
        //   return  Redirect::to('user/login/'.$event_id);
        //   exit();
        }//End  Chk


        $Member = Member::where('member_id','!=', '')
                    ->where('username' , $_EMAIL)
                    ->where('member_no' , $_MEMBERNO)
                    ->first();

        $citizen_id =  $Member->citizen_id;


        $inputTicket = Input::get('ticket');
        $prictTicketTotal = Input::get('prictTicketTotal');

         $events = Events::where('event_status', '!=', '2')
                              ->where('event_id' , $event_id)
                              ->select('event_name' , 'event_details', 'event_description', 'event_date_on' , 'event_id' ,
                                   'event_place_name', 'event_background', 'event_img' , 'event_logo', 'event_form_additional')
                              ->orderBy('event_id', 'desc')
                              ->first();

        $Souvenirs = Souvenir::where('souvenir_public', '=', '1')
                            ->where('souvenir_frontend_public', '=', '1')
                            ->where('event_id' , $event_id)
                            ->orderBy('souvenir_order_by', 'asc')
                            ->get();
        //========
        $Order = DB::select("select tbl_ticket.ticket_id from tbl_order
                            left join tbl_ticket
                            on(tbl_order.order_id = tbl_ticket.order_id)
                             where tbl_ticket.citizen_id = '".$citizen_id."'
                             and tbl_order.event_id = '".$event_id."'
                             and tbl_order.status_payment   = 'SUCCESS'
                             order by  tbl_order.order_no desc");

        $chkOrder = json_encode($Order);

        if($chkOrder == '[]' || $chkOrder == ''){
            $getCount = 0;
        }else{
            $getCount = 1;
        }
        //========

        return \View::make('frontend/apis.getTicketForm')
                            -> with('events', $events)
                            -> with('getCount', $getCount)
                            -> with('row_customer', $Member)
        					-> with('Souvenirs', $Souvenirs)
                            -> with('inputTicket', $inputTicket)
							-> with('prictTicketTotal', $prictTicketTotal)
							-> with('event_id', $event_id);
        //getTicketForm

    }//End event Detail

    public function postTicketFormWalkin(){   //Get event Detail

        $event_id = Input::get('event_id');
        $inputTicket = Input::get('ticket');
        $prictTicketTotal = Input::get('prictTicketTotal');
        $type_payment = Input::get('type_payment');

         $events = Events::where('event_status', '!=', '2')
                              ->where('event_id' , $event_id)
                              ->select('event_name' , 'event_details', 'event_description', 'event_date_on' , 'event_id' ,
                                   'event_place_name', 'event_background', 'event_img' , 'event_logo', 'event_form_additional', 'event_rate_backend')
                              ->orderBy('event_id', 'desc')
                              ->first();

        $Souvenirs = Souvenir::where('souvenir_public', '=', '1')
                            ->where('event_id' , $event_id)
                            ->orderBy('souvenir_order_by', 'asc')
                            ->get();
        //========

        //========

        return \View::make('backend/apis.getTicketForm')
                            -> with('events', $events)
                            -> with('Souvenirs', $Souvenirs)
                            -> with('inputTicket', $inputTicket)
                            -> with('prictTicketTotal', $prictTicketTotal)
                            -> with('type_payment', $type_payment)
                            -> with('event_id', $event_id);
        //getTicketForm

    }//End event Detail

     public function postSelectPayment($event_id){
     	print_r($ticket_id);
     }
    //=================================== Start Api func, ==========================================
     public function getApiProvince(){
        $PROVINCE_ID     = Input::get('PROVINCE_ID');

         $Amphurs =  DB::table('tbl_amphur')
                            ->where('PROVINCE_ID', $PROVINCE_ID)
                            ->orderBy('AMPHUR_NAME', 'asc')
                            ->get();
         return \View::make('frontend/apis.getAmphur')-> with('Amphurs', $Amphurs);

     }//getApiProvince

      public function getApiDistrict(){
        $amphur_ID     = Input::get('amphur_ID');

         $Districts =  DB::table('tbl_district')
                            ->where('AMPHUR_ID', $amphur_ID)
                            ->orderBy('DISTRICT_NAME', 'asc')
                            ->get();
         return \View::make('frontend/apis.getDistrict')-> with('Districts', $Districts);

     }//getApiDistrict
      public function getApiZipcode(){
        $DISTRICT_ID     = Input::get('DISTRICT_ID');

         $Zipcodes =  DB::table('tbl_zipcode')
                            ->where('DISTRICT_ID', $DISTRICT_ID)
                            ->orderBy('ZIPCODE', 'asc')
                            ->get();
         return \View::make('frontend/apis.getZipcode')-> with('Zipcodes', $Zipcodes);

     }//getApiProvince
      public function getGetCitizen($event_id){   //Get event Detail

         $varCitizen     = Input::get('varCitizen');
          $ticketID     = Input::get('ticketID');

         if($varCitizen != ''){

             $year = date('y');
            $month = date('m');

            $dbTableName = 'tbl_order_'.$year.'_'.$month;
            $dbTableTicketName = 'tbl_order_ticket_'.$year.'_'.$month;

            $countResult = DB::table($dbTableName.' as tbl_order')
                    ->join($dbTableTicketName.' as tbl_ticket', 'tbl_ticket.order_id', '=', 'tbl_order.order_id')
                    ->select(  'tbl_ticket.ticket_id' )

                    ->where(  'tbl_ticket.ticket_id' , '!=' ,$ticketID)
                    ->where(  'tbl_order.status_payment' , 'SUCCESS')
                    ->where(  'tbl_ticket.citizen_id' , $varCitizen)
                    ->where(  'tbl_order.event_id' , $event_id)
                    ->groupBy('tbl_ticket.ticket_id')
                    ->count();

               if($countResult >= 1){
                    return 'เลขบัตรประชาชนนี้ได้ลงทะเบียนแล้วไม่สามารถลงทะเบียนซ้ำได้/The ID card you entered has already been registered. <input type="hidden" name="error_citizenid" value="error" class="error_citizenid">';
               }  //
         }//End

      }//getGetCitizen

        public function getFriends(){
             $friends_id     = Input::get('friendsID');

           return  $friends = Friends::where('friends_id', $friends_id)
                                    ->first();

        }//getGetFriends
    //=================================== End Api func, ==========================================
    //=================================== Start Func Static ==========================================
       public static function getEventName($event_id){

        return $Events = Events:: where('event_id' , $event_id)
                            ->select('event_name')
                            ->orderBy('event_id', 'desc')
                            ->first();
      }//getEventName



     public static function getEventLists(){

        return $Events = Events::where('event_status', '!=', '2')
                            ->orderBy('event_id', 'desc')
                            ->get();
     }//getEventLists

    public static function getTicketTypeLists($event_id = null){

        return $Ticket = Ticket::where('ticket_public', '!=', '2')
                            ->where('event_id' , $event_id)
                            ->orderBy('ticket_order_by', 'asc')
                            ->get();
     }
     public static function getProductTypeLists($event_id = null){

        return $products = Product::where('product_publish', '!=', '2')
                            ->where('event_id' , $event_id)
                            ->orderBy('product_orderby', 'asc')
                            ->get();
     }

     public static function getTicketType($ticket_id){

   		return $tickets = Ticket::where('ticket_public', '!=', '2')
		                    ->where('ticket_id' , $ticket_id)
		                    ->orderBy('ticket_order_by', 'asc')
		                    ->first();
     }
     public static function getCateSouvenir($souvenir_id){

        return $Souvenir = Souvenir::where('souvenir_public', '=', '1')
                            ->where('souvenir_id' , $souvenir_id)
                            ->orderBy('souvenir_id', 'asc')
                            ->first();
     }

     public static function getCateSouvenirLists($event_id){

        return $Souvenir = Souvenir::where('souvenir_public', '=', '1')
                            ->where('event_id' , $event_id)
                            ->orderBy('souvenir_order_by', 'asc')
                            ->get();
     }

     public static function getSouvenir($event_id, $souvenir_id){

        return $TypeSouvenir = TypeSouvenir::where('souvenir_type_public', '=', '1')
                            ->where('souvenir_id' , $souvenir_id)
                            ->where('event_id' , $event_id)
                            ->orderBy('souvenir_type_order_by', 'asc')
                            ->get();
     }

     public static function getSouvenirName(  $type_souvenir_id){

        return $TypeSouvenir = TypeSouvenir::where('souvenir_type_public', '=', '1')
                            ->where('souvenir_type_id' , $type_souvenir_id)
                             ->orderBy('souvenir_type_order_by', 'asc')
                            ->orderBy('souvenir_type_id', 'asc')
                            ->first();
     }

      public static function getProductName(  $product_id){

        return $Product = Product::where('product_publish', '=', '1')
                            ->where('product_id' , $product_id)
                            ->first();
     }


    public static function getCountries(){
        return $Countries =   DB::table('tbl_countries')
                            ->select('id', 'country_name')
                            ->orderBy('country_name', 'asc')
                            ->get();
     }
     public static function getProvince(){
        return $Province =   DB::table('tbl_province')
                             ->select('PROVINCE_ID', 'PROVINCE_NAME')
                            ->orderBy('PROVINCE_NAME', 'asc')
                            ->get();
     }
      public static function getAmphur(){
        return $Amphur =  DB::table('tbl_amphur')
                            ->select('AMPHUR_ID', 'AMPHUR_NAME')
                            ->orderBy('AMPHUR_NAME', 'asc')
                            ->get();
     }
     public static function getDistrict(){
        return $District =  DB::table('tbl_district')
                             ->select('DISTRICT_ID', 'DISTRICT_NAME')
                            ->orderBy('DISTRICT_NAME', 'asc')
                            ->get();
     }
     public static function getZipcode(){
        return $Zipcode =  DB::table('tbl_zipcode')
                            ->select('ZIPCODE_ID', 'ZIPCODE')
                            ->orderBy('ZIPCODE', 'asc')
                            ->get();
     }
      public static function getAmphurByProvince($province_id){
        return $Amphur =  DB::table('tbl_amphur')
                            ->where('PROVINCE_ID', $province_id)
                            ->select('AMPHUR_ID', 'AMPHUR_NAME')
                            ->orderBy('AMPHUR_NAME', 'asc')
                            ->get();
     }
     public static function getDistrictByAmpuur($ampuurID){
        return $District =  DB::table('tbl_district')
                            ->where('AMPHUR_ID', $ampuurID)
                            ->select('DISTRICT_ID', 'DISTRICT_NAME')
                            ->orderBy('DISTRICT_NAME', 'asc')
                            ->get();
     }

    public static function getZipcodeBySubDistrict($iDISTRICT_ID){
        return $Zipcode =  DB::table('tbl_zipcode')
                            ->select('ZIPCODE_ID', 'ZIPCODE')
                            ->where('DISTRICT_ID', $iDISTRICT_ID)
                            ->orderBy('ZIPCODE', 'asc')
                            ->get();
     }


     //=======
      public static function getFirstCountrie($countries_id){
        return $Countries =   DB::table('tbl_countries')
                            ->where('id', $countries_id)
                            ->orderBy('country_name', 'asc')
                            ->first();
     }
     public static function getFirstProvince($province_id){
        return $Province =   DB::table('tbl_province')
                            ->where('PROVINCE_ID', $province_id)
                            ->orderBy('PROVINCE_NAME', 'asc')
                            ->first();
     }
      public static function getFirstAmphur($district_id){
        return $Amphur =  DB::table('tbl_amphur')
                            ->where('AMPHUR_ID', $district_id)
                            ->orderBy('AMPHUR_NAME', 'asc')
                            ->first();
     }
     public static function getFirstDistrict($sub_district_id){
        return $District =  DB::table('tbl_district')
                             ->where('DISTRICT_ID', $sub_district_id)
                            ->orderBy('DISTRICT_NAME', 'asc')
                            ->first();
     }
     public static function getFirstZipcode($zipcode){
        return $Zipcode =  DB::table('tbl_zipcode')
                             ->where('ZIPCODE_ID', $zipcode)
                            ->orderBy('ZIPCODE', 'asc')
                            ->first();
     }

      public static function getStaffName($id){
        return $Staff =  DB::table('tbl_staff')
                             ->where('id', $id)
                             ->select('name')
                            ->orderBy('id', 'asc')
                            ->first();
     }
     //=======
      public static function getAge($birthday) {

        $year           = date('Y');
        $birthdayYear   = date('Y', strtotime($birthday));

        return(floor($year-$birthdayYear));
      }

       public static function getMemberDetail($member_id){

        return $Member =  DB::table('tbl_member')
                            ->where('member_id', $member_id)
                            ->select('f_name','l_name','tel','username','member_no')
                             ->first();
     }
     public static  function strCrop($txt,$num) { #ข้อความ,จำนวน
        if(strlen($txt) >= $num ) {
            $txt = iconv_substr($txt, 0, $num,"UTF-8")."...";
        }
        return $txt;
    }
      public static function getOrderListsByMember($member_id){

        return $OrderLists =  DB::table('tbl_order')
                            ->where('member_id', $member_id)
                            ->orderBy('order_id', 'desc')
                            ->get();
     }


     public static function getFuncFinancialClassifyProduct($productID = null, $type_payment = null, $dateStart = null){
        $totalPrice = 0;
        $sqlDateSelect = '';


        if($dateStart != ''){
            $sqlDateSelect =   "and DATE_FORMAT(tbl_order.order_resp_on,'%Y-%m-%d') BETWEEN '".$dateStart."' AND '".$dateStart."'";
            $tableMonth = date('y_m', strtotime($dateStart));
        }else{
             $tableMonth = date('y_m');
        }

        $tableNameOrder = 'tbl_order_'.$tableMonth;
        $tableNameProduct = 'tbl_order_product_'.$tableMonth;

         $results = DB::select("select tbl_product.product_id ,tbl_product.order_price
                                 from ".$tableNameOrder." as tbl_order
                                 left join ".$tableNameProduct."  as tbl_product
                                 on(tbl_product.order_id = tbl_order.order_id)
                                 where  tbl_order.status_payment = 'SUCCESS'
                                 and tbl_order.status_payment != 'FAIL'
                                 and tbl_order.type_payment = '".$type_payment."'
                                 and tbl_product.product_id = '".$productID."'
                                 ".$sqlDateSelect ."

                                 order by  tbl_order.order_id desc");

        foreach ($results as $row) {
            if($row->product_id != ''){
                    $productPrice = $row->order_price;
                    $totalPrice += $productPrice;
            }
        }//End foreach

        return $totalPrice;
     } //getFuncFinancialClassifyProduct

       public static function getFuncProductStock($productID = null, $event_id, $dateStart = null , $dateEnd = null)
       {

            $sqlDateSelect = '';

            if($dateStart != ''){
                 $sqlDateSelect =   "and DATE_FORMAT(tbl_order.order_resp_on,'%Y-%m-%d') BETWEEN '".$dateStart."' AND '".$dateEnd."'";

            }
            $product = Product::select('event_id')
                              ->where('product_id' , $productID)
                              ->first();

            $events = Events::where('tbl_event.event_id' , $product->event_id)
                    ->orderBy('tbl_event.event_id', 'desc')
                    ->first();


            $eventStart = $events->event_register_start_on;
            $dateNow      = date('Y-m-d H:i:s');

            $start     = strtotime($eventStart);
            $end       = strtotime($dateNow);

            $startmoyr = date('Y', $start) . date('m', $start);
            $endmoyr = date('Y', $end) . date('m', $end);
            $totalPriceAll = 0;
            $num = 0;
            while ($startmoyr <= $endmoyr) {

                $tableMonth =  date("y_m", $start) ;
                $start = strtotime("+1month", $start);
                $startmoyr = date('Y', $start) . date('m', $start);

                $tableNameOrder = 'tbl_order_'.$tableMonth;
                $tableNameProduct = 'tbl_order_product_'.$tableMonth;
                $totalPrice = 0;

                $results = DB::select("select tbl_product.product_id ,tbl_product.order_price
                                     from ".$tableNameOrder." as tbl_order
                                     left join ".$tableNameProduct."  as tbl_product
                                     on(tbl_product.order_id = tbl_order.order_id)
                                     where  tbl_order.status_payment = 'SUCCESS'
                                     and tbl_order.status_payment != 'FAIL'
                                     and tbl_product.product_id = '".$productID."'
                                     ".$sqlDateSelect ."
                                     order by  tbl_order.order_id desc");

                foreach ($results as $row) {
                    if($row->product_id != ''){

                      $num++;
                    }
                }//End foreach
              } //End While



        return $num;
     }//getFuncProductStock

     public static function getFuncStockReport($ticket_id = null, $status_payment = null , $dateStart = null, $typePayment = null, $dateEnd = null, $cond = "="  ){


            $tickets = Ticket::select('event_id')
                              ->where('ticket_id' , $ticket_id)
                              ->first();

            $events = Events::where('tbl_event.event_id' , $tickets->event_id)
                    ->orderBy('tbl_event.event_id', 'desc')
                    ->first();

            $eventStart = $events->event_register_start_on;
            $dateNow      = date('Y-m-d H:i:s');

            $start     = strtotime($eventStart);
            $end       = strtotime($dateNow);

            $startmoyr = date('Y', $start) . date('m', $start);
            $endmoyr = date('Y', $end) . date('m', $end);
            $totalPriceAll = 0;

            while ($startmoyr <= $endmoyr) {

                $tableMonth =  date("y_m", $start) ;
                $start = strtotime("+1month", $start);
                $startmoyr = date('Y', $start) . date('m', $start);

                $tableNameOrder = 'tbl_order_'.$tableMonth;
                $tableNameTicket = 'tbl_order_ticket_'.$tableMonth;

                 $results = DB::table($tableNameOrder.' as tbl_order')
                                ->select('tbl_ticket.ticket_id')
                                ->leftjoin($tableNameTicket.' as tbl_ticket', 'tbl_ticket.order_id', '=', 'tbl_order.order_id')
                                ->where('tbl_ticket.type_event_id', '=', $ticket_id)
                                ->where('tbl_order.status_payment', '=', $status_payment)
                                ->whereBetween('order_resp_on', [(!empty($dateStart) ? $dateStart : date('Y-m-d', strtotime($eventStart))).' 00:00:00', (!empty($dateEnd) ? $dateEnd : date('Y-m-d', $end)).' 23:59:59'])
                                ->orderBy('tbl_order.order_no', 'desc');

                  if(!empty($typePayment)){
                      $results =   $results->where('tbl_order.type_payment', $cond, $typePayment);
                  }

                 $results =  $results->count();
                 $totalPriceAll += $results;

              } //End While
              $results =   $totalPriceAll;




          return  $results ;
     }//getFuncStockReport

      public static function getFuncStockReportRange($ticket_id = null, $status_payment = null , $dateStart = null, $dateEnd= null, $cond = null ){


            $tickets = Ticket::select('event_id')
                              ->where('ticket_id' , $ticket_id)
                              ->first();

            $events = Events::where('tbl_event.event_id' , $tickets->event_id)
                    ->orderBy('tbl_event.event_id', 'desc')
                    ->first();



            $eventStart = $events->event_register_start_on;
            $dateNow      = date('Y-m-d H:i:s');

            $start     = strtotime($eventStart);
            $end       = strtotime($dateNow);

            $startmoyr = date('Y', $start) . date('m', $start);
            $endmoyr = date('Y', $end) . date('m', $end);
            $totalPriceAll = 0;

            while ($startmoyr <= $endmoyr) {

                $tableMonth =  date("y_m", $start) ;
                $start = strtotime("+1month", $start);
                $startmoyr = date('Y', $start) . date('m', $start);

                $tableNameOrder = 'tbl_order_'.$tableMonth;
                $tableNameTicket = 'tbl_order_ticket_'.$tableMonth;

                 $results = DB::table($tableNameOrder.' as tbl_order')
                                ->select('tbl_ticket.ticket_id')
                                ->leftjoin($tableNameTicket.' as tbl_ticket', 'tbl_ticket.order_id', '=', 'tbl_order.order_id')
                                ->where('tbl_ticket.type_event_id', '=', $ticket_id)
                                ->where('tbl_order.status_payment', '=', $status_payment)
                                ->orderBy('tbl_order.order_no', 'desc');
                 if($dateStart != ''){
                     $results =    $results->whereBetween('order_resp_on', [$dateStart.' 00:00:00', $dateEnd.' 23:59:59']);
                  }
                  if(!empty($cond)){
                      $results = $results->whereRaw($cond);
                  }

                 $results =  $results->count();
                 $totalPriceAll += $results;

              } //End While
              $results =   $totalPriceAll;




          return  $results ;
     }//getFuncStockReport

      public static function getFuncStockVIPReport($ticket_id = null ){
       return  $results = DB::table('tbl_order')
                        ->select('tbl_ticket.ticket_id')
                        ->join('tbl_ticket', 'tbl_ticket.order_id', '=', 'tbl_order.order_id')
                        ->where('tbl_ticket.ticket_id', '=', $ticket_id)
                        ->where('tbl_order.type_payment', '=', 'VIP')
                        ->where('tbl_order.status_payment', '=', 'SUCCESS')
                        ->orderBy('tbl_order.order_no', 'desc')
                        ->count();
     }//getFuncStockVIP

      public static function getSouvenirReceivedReport($souvenir_type_id = null, $status_payment = null   ){
        $tableNameOrder = 'tbl_order_'.date('y_m');
        $tableNameTicket = 'tbl_order_ticket_'.date('y_m');

        return  $results = DB::table($tableNameOrder.' as tbl_order')
                        ->select('tbl_ticket.ticket_id')
                       ->join($tableNameTicket.' as tbl_ticket', 'tbl_ticket.order_id', '=', 'tbl_order.order_id')
                        ->where('tbl_ticket.type_souvenir_id', '=', $souvenir_type_id)
                        ->where('tbl_order.status_payment', '=', $status_payment)
                        ->where('tbl_ticket.recive_souvenir', '=', '1')
                        ->groupBy('tbl_ticket.ticket_id')
                        ->orderBy('tbl_order.order_no', 'desc')
                        ->count();

     }//getFuncStockReport



      public static function getFuncSouvenirReport($souvenir_type_id = null, $status_payment = null ){
         $tableNameOrder = 'tbl_order_'.date('y_m');
         $tableNameTicket = 'tbl_order_ticket_'.date('y_m');

        return  $results = DB::table($tableNameOrder.' as tbl_order')
                        ->select('tbl_ticket.ticket_id')
                        ->join($tableNameTicket.' as tbl_ticket', 'tbl_ticket.order_id', '=', 'tbl_order.order_id')
                        ->where('tbl_ticket.type_souvenir_id', '=', $souvenir_type_id)
                        ->where('tbl_order.status_payment', '=', $status_payment)
                        ->orderBy('tbl_order.order_no', 'desc')
                        ->count();
     }//getFuncSouvenirReport

       public static function getUserAccount( ){

        $_MEMBERNO = Session::get('_MEMBERNO');
        $_EMAIL = Session::get('_EMAIL');
        $_NAME = Session::get('_NAME');

        if ($_MEMBERNO == '' || $_EMAIL == ''  || $_NAME == '') {
            return  Redirect::to('user/login/'.$event_id);
            exit();
        }else{  //End  Chk

        return $members = Member::where('member_no' , $_MEMBERNO)
                                ->where('username' , $_EMAIL)
                                ->select('member_avata' , 'member_fb_id')
                                ->first();

          }//End Chk
     }
      public static function getOrderTicket( $orderNo, $orderID, $eventID){

             $year   = substr($orderNo, 0,2);
             $month  = substr($orderNo, 2,2);

             $tableNameTicket = 'tbl_order_ticket_'.$year.'_'.$month;
             $orderTickets = DB::table($tableNameTicket)
                                 ->where('order_id' , $orderID)
                                 ->where('event_id' , $eventID);
            $orderTicketsCount =  $orderTickets->count();

            return $orderTickets =  $orderTickets->get();

      }//getOrderTicket


       public static function getOrderProduct( $orderNo, $orderID, $eventID){

             $year   = substr($orderNo, 0,2);
             $month  = substr($orderNo, 2,2);

              $tableNameProduct = 'tbl_order_product_'.$year.'_'.$month;
              $orderProducts = DB::table($tableNameProduct)
                                     ->where('order_id' , $orderID)
                                     ->where('event_id' , $eventID);

             return $orderProducts =   $orderProducts->get();

       }//getOrderProduct


        public static function getCountTicketEvent( $eventID){

            $year = date('y');
            $month = date('m');

            $dbTableName = 'tbl_order_'.$year.'_'.$month;
            $dbTableTicketName = 'tbl_order_ticket_'.$year.'_'.$month;

            $orderLists = DB::table($dbTableName.' as tbl_order')
                            ->select( 'tbl_ticket.ticket_id' )
                            ->join($dbTableTicketName.' as tbl_ticket', 'tbl_ticket.order_id', '=', 'tbl_order.order_id')
                            ->where(  'tbl_order.event_id' ,  $eventID)
                            ->where(  'tbl_ticket.event_id' ,  $eventID)
                            ->where(  'tbl_order.status_payment' ,'SUCCESS')
                            ->groupBy( 'tbl_ticket.ticket_id')
                            ->orderBy('tbl_order.order_no');
            $orderListsCount = $orderLists->get();

          return  $count = count($orderListsCount);

        }//getCountTicketEvent

    public static function getMenuMeaasge(){
      return  TextMenu::where('text_menu_status', '=', '1')
                        ->select('text_menu_detail', 'text_menu_id' )
                        ->get();
    }
     public static function getLang($lang){
        return  Lang::where('lang_name_l', '!=', $lang)
                    ->where('lang_status' , '=', 1)
                    ->select('lang_name_l', 'lang_name_u', 'lang_name_full')
                    ->get();
    }
      //=================================== End Func Static ==========================================
}
