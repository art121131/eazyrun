<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input,
    Redirect,
    DB;
use App\Models\User;
use App\Models\Events;
use App\Models\Ticket;
use App\Models\Souvenir;
use App\Models\TypeSouvenir;
use App\Models\Order;
use App\Models\Member;
use App\Models\TicketForm;
use Auth;
use App\Http\Controllers\Frontend\ApiContreller;

class MainController extends Controller {

    public function getIndex() {

        $user = \Auth::user();

        if (empty($user)) {
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }
        return \View::make('backend.mainChart');
    }
    
    public function getDetialChart(){
        
        $user = \Auth::user();
        $event_id = $user->event_id;
        
        $events = Events::where('tbl_event.event_id', $event_id)
                ->orderBy('tbl_event.event_id', 'desc')
                ->first();

        $Price = array(
            'all' => 0,
            'month' => 0,
            'day' => 0
        );
        $Gender = array(
            'male' => 0,
            'female' => 0,
            'etc' => 0
        );
        $Age = array(
            'a10' => 0,
            'a20' => 0,
            'a30' => 0,
            'a40' => 0,
            'a50' => 0,
            'a60' => 0,
            'etc' => 0,
        );
        
        $rowTickets = ApiContreller::getTicketTypeLists($event_id);

        foreach ($rowTickets as $rowTicket) {
            $ticket_name = json_decode($rowTicket->ticket_title, true);
            $ticket_name = $ticket_name['data']['th'];
            $ticketID = $rowTicket->ticket_id;

            $ticketSuccess = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS');
            $Price['all'] += $ticketSuccess * $rowTicket->ticket_price;
            
            $ticketSuccessMonth = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS', date('Y-m').'-1', date('Y-m').'-31');
            $Price['month'] += $ticketSuccessMonth * $rowTicket->ticket_price;
            
            $ticketSuccessDay = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS', date('Y-m-d'), date('Y-m-d'));
            $Price['day'] += $ticketSuccessDay * $rowTicket->ticket_price;
            
            $ticketMale = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " tbl_ticket.gender LIKE '%ชาย%' ");
            $Gender['male'] += $ticketMale;
            
            $ticketFemale = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " tbl_ticket.gender LIKE '%หญิง%' ");
            $Gender['female'] += $ticketFemale;
            
            $ticketETC = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " tbl_ticket.gender NOT IN ('ชาย','หญิง') ");
            $Gender['etc'] += $ticketETC;
            
            $ticketA10 = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " YEAR(CURDATE())-YEAR(tbl_ticket.birthday)<='19' ");
            $Age['a10'] += $ticketA10;
            
            $ticketA20 = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " (YEAR(CURDATE())-YEAR(tbl_ticket.birthday)>='20' AND YEAR(CURDATE())-YEAR(tbl_ticket.birthday)<='29') ");
            $Age['a20'] += $ticketA20;
            
            $ticketA30 = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " (YEAR(CURDATE())-YEAR(tbl_ticket.birthday)>='30' AND YEAR(CURDATE())-YEAR(tbl_ticket.birthday)<='39') ");
            $Age['a30'] += $ticketA30;
            
            $ticketA40 = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " (YEAR(CURDATE())-YEAR(tbl_ticket.birthday)>='40' AND YEAR(CURDATE())-YEAR(tbl_ticket.birthday)<='49') ");
            $Age['a40'] += $ticketA40;
            
            $ticketA50 = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " (YEAR(CURDATE())-YEAR(tbl_ticket.birthday)>='50' AND YEAR(CURDATE())-YEAR(tbl_ticket.birthday)<='59') ");
            $Age['a50'] += $ticketA50;
            
            $ticketA60 = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " YEAR(CURDATE())-YEAR(tbl_ticket.birthday)>='60' ");
            $Age['a60'] += $ticketA60;
            
            $ticketAETC = ApiContreller::getFuncStockReportRange($ticketID, 'SUCCESS' ,null, null, " tbl_ticket.birthday IS NULL ");
            $Age['etc'] += $ticketAETC;
        }
        
        $results = array(
            'price' => $Price,
            'gender' => $Gender,
            'age' => $Age
        );
        
        return $results;
    }

}
