<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input,
    Redirect,
    DB;
use App\Models\User;
use App\Models\Events;
use App\Models\Ticket;
use App\Models\Souvenir;
use App\Models\TypeSouvenir;
use App\Models\Order;
use App\Models\Member;
use App\Models\TicketForm;
use Auth;

class OrderContreller extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    // 

    public function getFinancialClassify() {
        
        $dateStart = Input::get('dateStart');
        $dateEnd = Input::get('dateEnd');

        $event_id = Input::get('event_id');

        //======== Check Admin ============//
        $user = \Auth::user();
        $user_level = $user->level;

        if ($user_level == 'Staff') {
            $event_id = $user->event_id;
        }//event_id
        //======== Check Admin ============//

        if ($event_id == '') {
            $events = Events::where('event_status', '=', '1')
                    ->select('event_id', 'event_register_start_on')
                    ->orderBy('event_id', 'desc')
                    ->first();

            $event_id = $events->event_id;
        }//End if  event_id ==   
        
        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->financial_classify)||$per->financial_classify==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }    
        //------------------------------------------------------------------------------//
        
        return \View::make('backend.financialClassify')->with('dateStart', $dateStart)->with('dateEnd', $dateEnd)->with('event_id', $event_id);
    }

    public function getStockReport() {

        $dateStart = Input::get('dateStart');
        $dateEnd = Input::get('dateEnd');
        //======== Check Admin ============//

        $user = \Auth::user();
        $event_id = $user->event_id;
        
        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->stock_report)||$per->stock_report==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }    
        //------------------------------------------------------------------------------//
        
        return \View::make('backend.stockReport')->with('event_id', $event_id)->with('dateStart', $dateStart)->with('dateEnd', $dateEnd);
    }

//getStockReport

    public function getProductStock() {

        $event_id = Input::get('event_id');

        //======== Check Admin ============//
        $user = \Auth::user();
        $user_level = $user->level;

        if ($user_level == 'Staff') {
            $event_id = $user->event_id;
        }//event_id

        if ($event_id == '') {
            $events = Events::where('event_status', '=', '1')
                    ->select('event_id')
                    ->orderBy('event_id', 'desc')
                    ->first();

            $event_id = $events->event_id;
        }//End if  event_id  

        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->product_stock)||$per->product_stock==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }    
        //------------------------------------------------------------------------------//
        
        return \View::make('backend.productStockReport')->with('event_id', $event_id);
    }

//getProductStock

    public function getStockSum() {
        $event_id = Input::get('event_id');

        //======== Check Admin ============//
        $user = \Auth::user();
        $user_level = $user->level;

        if ($user_level == 'Staff') {
            $event_id = $user->event_id;
        }//event_id

        if ($event_id == '') {
            $events = Events::where('event_status', '=', '1')
                    ->select('event_id')
                    ->orderBy('event_id', 'desc')
                    ->first();

            $event_id = $events->event_id;
        }//End if  event_id  

        return \View::make('backend.stockReportSum')->with('event_id', $event_id);
    }

//getStockReport

    public function getStockSouvenir() {
        $event_id = Input::get('event_id');

        //======== Check Admin ============//
        $user = \Auth::user();
        $user_level = $user->level;

        if ($user_level == 'Staff') {
            $event_id = $user->event_id;
        }//event_id

        if ($event_id == '') {
            $events = Events::where('event_status', '=', '1')
                    ->select('event_id')
                    ->orderBy('event_id', 'desc')
                    ->first();

            $event_id = $events->event_id;
        }//End if  event_id  

        return \View::make('backend.stockSouvenirReport')->with('event_id', $event_id);
    }

//getStockReport

    public function getDetail($order_no) {

        $year = substr($order_no, 0, 2);
        $month = substr($order_no, 2, 2);

        $dbTableName = 'tbl_order_' . $year . '_' . $month;

        $orders = DB::table($dbTableName)
                ->where('order_no', $order_no)
                ->first();

        $orderID = $orders->order_id;
        $eventID = $orders->event_id;
        $memberID = $orders->member_id;


        $events = Events::where('tbl_event.event_status', '=', '1')
                ->where('tbl_event.event_id', $eventID)
                ->leftjoin('tbl_dealer', 'tbl_dealer.dealer_id', '=', 'tbl_event.dealer_id')
                ->select('tbl_event.event_date_key_finish', 'tbl_event.event_name', 'tbl_event.event_url', 'tbl_event.event_details', 'tbl_event.event_description', 'tbl_event.event_date_on', 'tbl_event.event_id', 'tbl_event.event_status', 'tbl_dealer.dealer_name', 'tbl_event.event_place_name', 'tbl_event.event_background', 'tbl_event.event_img', 'tbl_event.event_logo', 'tbl_event.event_form_additional', 'tbl_event.event_rate_psb', 'tbl_event.event_rate_paypal', 'tbl_event.event_ticket_select_limit')
                ->orderBy('tbl_event.event_id', 'desc')
                ->first();

        $tableNameTicket = 'tbl_order_ticket_' . $year . '_' . $month;
        $orderTickets = DB::table($tableNameTicket)
                ->where('order_id', $orderID)
                ->where('event_id', $eventID)
                ->get();

        $tableNameProduct = 'tbl_order_product_' . $year . '_' . $month;
        $orderProducts = DB::table($tableNameProduct)
                ->where('order_id', $orderID)
                ->where('event_id', $eventID);
        $orderProductCount = $orderProducts->count();
        $orderProducts = $orderProducts->get();

        $Member = Member::where('member_id', $memberID)
                ->first();

        return \View::make('backend.orderDetail')->with('rowOrder', $orders)
                        ->with('Member', $Member)
                        ->with('events', $events)
                        ->with('orderTickets', $orderTickets)
                        ->with('orderProducts', $orderProducts)
                        ->with('orderProductCount', $orderProductCount);
    }

// End fn getDetail

    public function getReceived() {
        
        $user = \Auth::user();
        
        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->received)||$per->received==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }    
        //------------------------------------------------------------------------------//

        return \View::make('backend.received');
    }

//getReceived

    public function getReceivedSearchTicket() {
        $keyword = Input::get('keyword');
        $keyword_order = Input::get('keyword_order');

        $user = \Auth::user();
        $user_level = $user->level;

        $sqlEvent = '';
        $event_id = $user->event_id;
        $sqlEvent = "and tbl_order.event_id = '" . $event_id . "'";

        $events = Events::where('tbl_event.event_status', '=', '1')
                ->where('tbl_event.event_id', $event_id)
                ->select('tbl_event.event_register_start_on', 'tbl_event.event_register_end_on')
                ->orderBy('tbl_event.event_id', 'desc')
                ->first();


        $eventStart = date('Y-m', strtotime($events->event_register_start_on . "-1 days")) . '-01';
        $dateNow = date('Y-m-d H:i:s');

        $start = strtotime($eventStart);
        $end = strtotime($dateNow);

        $startmoyr = date('Y', $start) . date('m', $start);
        $endmoyr = date('Y', $end) . date('m', $end);
        $resultsAll = 0;
        $i = 1;

        while ($startmoyr <= $endmoyr) {

            $tableMonth = date("y_m", $start);
            $dbTableName = 'tbl_order_' . $tableMonth;
            $dbtableNameTicket = 'tbl_order_ticket_' . $tableMonth;

            $start = strtotime("+1month", $start);
            $startmoyr = date('Y', $start) . date('m', $start);

            $listOrderMonth['order_by'] = $i;
            $listOrderMonth['month'] = $tableMonth;

            if ($keyword_order != '') {
                $orderLists = DB::select("select
                            tbl_order.order_id, tbl_order.order_no, tbl_ticket.f_name, tbl_ticket.l_name, tbl_ticket.citizen_id, tbl_ticket.email, tbl_ticket.remark,
                            tbl_ticket.recive_souvenir, tbl_ticket.ticket_id , tbl_ticket.type_event_id, tbl_ticket.type_souvenir_id, tbl_ticket.ticket_receive_type, tbl_ticket.ticket_receive_by
                            from " . $dbTableName . " as tbl_order
                            left join " . $dbtableNameTicket . " as tbl_ticket
                            on(tbl_order.order_id = tbl_ticket.order_id)
                             where (tbl_order.status_payment = 'SUCCESS' or tbl_order.type_payment = 'vip_2')
                            and ( tbl_order.order_no like '%$keyword_order%'  )
                            $sqlEvent
                             GROUP BY tbl_ticket.ticket_id
                             order by  tbl_order.order_no desc
                             limit 0,10");
            } else if ($keyword != '') {
                $orderLists = DB::select("select
                            tbl_order.order_id, tbl_order.order_no, tbl_ticket.f_name, tbl_ticket.l_name, tbl_ticket.citizen_id, tbl_ticket.email, tbl_ticket.remark,
                            tbl_ticket.recive_souvenir, tbl_ticket.ticket_id , tbl_ticket.type_event_id, tbl_ticket.type_souvenir_id, tbl_ticket.ticket_receive_type, tbl_ticket.ticket_receive_by
                             from " . $dbTableName . " as tbl_order
                            left join " . $dbtableNameTicket . " as tbl_ticket
                            on(tbl_order.order_id = tbl_ticket.order_id)
                          where (tbl_order.status_payment = 'SUCCESS' or tbl_order.type_payment = 'vip_2')
                          and (  tbl_ticket.email like '%$keyword%'  or
                                tbl_ticket.citizen_id like '%$keyword%'  or
                                tbl_ticket.f_name like '%$keyword%' or tbl_ticket.l_name like '%$keyword%')
                            $sqlEvent
                         GROUP BY tbl_ticket.ticket_id
                         order by  tbl_order.order_no desc
                          limit 0,10");
            }  //End keyword_order

            if ($orderLists != '[]' && $orderLists != '') {
                $listOrderMonth['data'] = $orderLists;
            } else {
                $listOrderMonth['data'] = '';
            }

            $listOrderMonthAll[] = $listOrderMonth;
            $i++;
        }//End While

        return \View::make('backend.getTicketRecevied')->with('listOrderMonthAll', $listOrderMonthAll);

        /* $year = date('y');  
          $month = date('m');
          $dbTableName = 'tbl_order_'.$year.'_'.$month;
          $dbtableNameTicket = 'tbl_order_ticket_'.$year.'_'.$month;

          if($keyword_order != ''){
          $qrySel = DB::select("select
          tbl_order.order_id, tbl_order.order_no, tbl_ticket.f_name, tbl_ticket.l_name, tbl_ticket.citizen_id, tbl_ticket.email,
          tbl_ticket.recive_souvenir, tbl_ticket.ticket_id , tbl_ticket.type_event_id, tbl_ticket.type_souvenir_id, tbl_ticket.ticket_receive_type, tbl_ticket.ticket_receive_by
          from ".$dbTableName." as tbl_order
          left join ".$dbtableNameTicket." as tbl_ticket
          on(tbl_order.order_id = tbl_ticket.order_id)
          where (tbl_order.status_payment = 'SUCCESS' or tbl_order.type_payment = 'vip_2')
          and ( tbl_order.order_no like '%$keyword_order%'  )
          $sqlEvent
          GROUP BY tbl_ticket.ticket_id
          order by  tbl_order.order_no desc
          limit 0,10");
          }else if($keyword != ''){
          $qrySel = DB::select("select
          tbl_order.order_id, tbl_order.order_no, tbl_ticket.f_name, tbl_ticket.l_name, tbl_ticket.citizen_id, tbl_ticket.email,
          tbl_ticket.recive_souvenir, tbl_ticket.ticket_id , tbl_ticket.type_event_id, tbl_ticket.type_souvenir_id, tbl_ticket.ticket_receive_type, tbl_ticket.ticket_receive_by
          from ".$dbTableName." as tbl_order
          left join ".$dbtableNameTicket." as tbl_ticket
          on(tbl_order.order_id = tbl_ticket.order_id)
          where (tbl_order.status_payment = 'SUCCESS' or tbl_order.type_payment = 'vip_2')
          and (  tbl_ticket.email like '%$keyword%'  or
          tbl_ticket.citizen_id like '%$keyword%'  or
          tbl_ticket.f_name like '%$keyword%' or tbl_ticket.l_name like '%$keyword%')
          $sqlEvent
          GROUP BY tbl_ticket.ticket_id
          order by  tbl_order.order_no desc
          limit 0,10");
          }  //End keyword_order */



        // return \View::make('backend.getTicketRecevied')-> with('qrySel', $qrySel);
    }

//getReceviedSearchTicket

    public function getReceivedTicketSubmit() {
        $user = \Auth::user();
        $user_level = $user->id;
        
        $ticketID = Input::get('ticketID');
        $ticket_receive_type = Input::get('ticket_receive_type');
        $ticket_receive_by = Input::get('ticket_receive_by');
        $remark = Input::get('remark');
        $adminID = $user->id;
        
        $monthYear = Input::get('monthYear');
        $monthYear = explode('_', $monthYear);

        $year = $monthYear[0];
        $month = $monthYear[1];
        $dbTableName = 'tbl_order_' . $year . '_' . $month;
        $dbtableNameTicket = 'tbl_order_ticket_' . $year . '_' . $month;

        DB::table($dbtableNameTicket)
                ->where('ticket_id', $ticketID)
                ->update(['recive_admin_office' => $adminID,
                    'ticket_receive_type' => $ticket_receive_type,
                    'ticket_receive_by' => $ticket_receive_by,
                    'remark' => $remark,
                    'recive_souvenir' => '1',
                    'recive_date_on' => date("Y-m-d H:i:s")]);
    }

//getReceviedTicketSubmit

    public function getPrintTicket($ticket_id, $table) {

        /*$year = date('y');
        $month = date('m');*/

        $dbtableNameTicket = 'tbl_order_ticket_' . $table;

        $TicketForms = DB::table($dbtableNameTicket)
                ->where('ticket_id', $ticket_id)
                ->first();


        return \View::make('backend.getTicketPrint')->with('TicketForms', $TicketForms);
    }

//End

    public function getPrintDetail($RETURNINV) {
        //============ Sendmail ====================//

        $Order = Order::where('order_no', $RETURNINV)
                ->select('order_id', 'event_id', 'member_id', 'order_no', 'order_create_on', 'type_payment', 'status_payment', 'order_price', 'walkin_type_payment')
                ->first();

        $order_id = $Order->order_id;
        $event_id = $Order->event_id;
        $member_id = $Order->member_id;

        $Member = Member::where('member_id', '!=', '')
                ->where('member_id', $member_id)
                ->select('f_name', 'l_name', 'username')
                ->first();

        $email = $Member->username;

        $events = Events::where('event_status', '1')
                ->where('event_id', $event_id)
                ->select('event_name', 'event_details', 'event_description', 'event_date_on', 'event_id', 'event_place_name', 'event_background', 'event_img', 'event_logo')
                ->orderBy('event_id', 'desc')
                ->first();

        $event_name = json_decode($events->event_name, true);
        $event_name = $event_name['data']['th'];

        $TicketForms = TicketForm::where('event_id', $event_id)
                ->where('order_id', $order_id)
                ->orderBy('ticket_id', 'asc')
                ->get();



        return \View::make('backend/printOrderSuccess')->with('events', $events)
                        ->with('Member', $Member)
                        ->with('Order', $Order)
                        ->with('TicketForms', $TicketForms);
        //============ Sendmail ====================//
    }

//getOrderSuccessSendmail

    public function getCancelReceive() {
        $user = \Auth::user();
        $user_level = $user->level;
        $id = $user->id;
        $input = Input::all();
        DB::beginTransaction();   
        
        try {
            
            $dbtableNameTicket = 'tbl_order_ticket_' . $input['monthYear'];
            
            $remark = DB::table($dbtableNameTicket)
                    ->where('ticket_id', $input['ticketID'])
                    ->select('remark')
                    ->first();;
            $remark = $remark->remark . ' {"cancel":' . date("Y-m-d H:i:s") . '}';

            DB::table($dbtableNameTicket)
                ->where('ticket_id', $input['ticketID'])
                ->update(['recive_cancel_admin_office' => $id,
                    'remark' => $remark,
                    'recive_souvenir' => '0']);

            DB::commit();
            $results['status'] = "ok";
            return $results;
        } catch (\Throwable $e) {
            DB::rollback();
            $results['status'] = "fail";
            $results['error'] = "$e";
            return $results;
        }
    }
    

//getCancelOrder

    public function getReceivedOrderPostpaid() {
        $user = \Auth::user();
        $user_level = $user->level;
        $id = $user->id;

        if (($user_level == 'Admin')) {
            $order_no = Input::get('order_no');

            return Order::where('order_no', $order_no)
                            ->where('type_payment', 'vip_2')
                            ->update(array('order_create_on' => date("Y-m-d H:i:s"),
                                'order_create_on' => date("Y-m-d H:i:s"),
                                'status_payment' => 'SUCCESS'));
        }// 
    }

    public function getEditTicket($ticket_id, $event_id) {

        $TicketForms = TicketForm::where('ticket_id', $ticket_id)
                ->orderBy('ticket_id', 'asc')
                ->first();
        $Souvenirs = Souvenir::where('souvenir_public', '=', '1')
                ->where('event_id', $event_id)
                ->orderBy('souvenir_order_by', 'asc')
                ->get();

        return \View::make('backend.ticketEdit')->with('chkTicket', $TicketForms)->with('Souvenirs', $Souvenirs)->with('event_id', $event_id)->with('ticket_id', $ticket_id);
    }

//getEditTicket

    public function postEditTicket() {
        $ticket_id = Input::get('ticket_id');
        $event_id = Input::get('event_id');

        $souvenir_type_id = Input::get('souvenir_type_id');
        $pre_name = Input::get('pre_name');
        $f_name = Input::get('f_name');
        $l_name = Input::get('l_name');
        $email = Input::get('email');
        $gender = Input::get('gender');
        $tel = Input::get('tel');
        $citizen_id = Input::get('citizen_id');

        $birthday = Input::get('birthday');
        $nationality = Input::get('nationality');

        TicketForm::where('ticket_id', $ticket_id)
                ->update(array('type_souvenir_id' => $souvenir_type_id,
                    'pre_name' => $pre_name,
                    'f_name' => $f_name,
                    'l_name' => $l_name,
                    'email' => $email,
                    'gender' => $gender,
                    'tel' => $tel,
                    'citizen_id' => $citizen_id,
                    'birthday' => $birthday,
                    'nationality' => $nationality));


        $TicketForms = TicketForm::where('ticket_id', $ticket_id)
                ->orderBy('ticket_id', 'asc')
                ->first();
        $Souvenirs = Souvenir::where('souvenir_public', '=', '1')
                ->where('event_id', $event_id)
                ->orderBy('souvenir_order_by', 'asc')
                ->get();

        return \View::make('backend.ticketEdit')->with('chkTicket', $TicketForms)->with('Souvenirs', $Souvenirs)->with('event_id', $event_id)->with('ticket_id', $ticket_id);
    }

//getEditTicket

    public function getUpdateWaitStatus($event_id) {
        $chkTime = date("Y-m-d H:i", strtotime(date("Y-m-d H:i") . " -1 hour"));
        $chkDay = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s") . " -1 days -1 hour"));

        $Orders = DB::select("select  * from tbl_order
                                where order_id !=''
                                and event_id = '" . $event_id . "'
                               
                                and status_payment = 'WAIT' 
                                order by order_id desc ");

        // and (DATE_FORMAT(order_create_on,'%Y-%m-%d %T') < '".$chkTime."')

        foreach ($Orders as $row_order) {
            echo $order_no = $row_order->order_no;

            if ($row_order->type_payment == 'kbank') {
                Order::where('order_no', $order_no)
                        ->where('status_payment', '!=', 'SUCCESS')
                        ->update(array('order_remark' => 'Bot Auto Fail',
                            'order_create_on' => date("Y-m-d H:i:s"),
                            'status_payment' => 'FAIL'));
            }// kbank

            if ($row_order->type_payment == 'psb' && $row_order->psb_channel != 'cash' && $row_order->data_psb_resp_back == '' && $row_order->data_resp_resp_front == '') {
                Order::where('order_no', $order_no)
                        ->where('status_payment', '!=', 'SUCCESS')
                        ->update(array('order_remark' => 'Bot Auto Fail',
                            'order_create_on' => date("Y-m-d H:i:s"),
                            'status_payment' => 'FAIL'));
            }//Psb

            if ($row_order->type_payment == 'psb' && $row_order->psb_channel == 'cash' && $row_order->order_create_on < $chkDay) {
                Order::where('order_no', $order_no)
                        ->where('status_payment', '!=', 'SUCCESS')
                        ->update(array('order_remark' => 'Bot Auto Fail',
                            'order_create_on' => date("Y-m-d H:i:s"),
                            'status_payment' => 'FAIL'));
            }//Psb cash
        }//foreach



        return $Orders;
    }

    //======== Souvenir =========== 
    //genExcel.php?type_payment=kbank&status_payment=SUCCESS&dateStart=2017-04-18&dateEnd=2017-04-18
    //=================================Summary=================================//
    public function getSummary() {
        
    }

    //=================================Summary=================================//

    public function getGenexcel($type_export, $type_payment, $status_payment, $dateStart, $dateEnd) {
        //  echo $type_export.$type_payment.$status_payment.$dateStart.$dateEnd;

        $event_id = '';
        $sqlStatusPayment = '';
        $sqlDateSelect = '';
        $sqlStatus = '';
        $sqlStatusSend = '';
        $sqlEvent = '';

        $dateNow = date('Y-m-d');
        //======== Check Admin ============//
        $user = \Auth::user();
        $user_level = $user->level;

        if ($user_level == 'Staff') {
            $event_id = $user->event_id;
        }//event_id
        //======== Check Admin ============//

        if ($dateStart == '') {
            $dateStart = date('Y-m-d');
        }
        if ($event_id == '') {
            $events = Events::where('event_status', '=', '1')
                    ->select('event_id')
                    ->orderBy('event_id', 'desc')
                    ->first();

            $event_id = $events->event_id;
        }
        if ($event_id != '') {
            $sqlEvent = "and event_id = '" . $event_id . "'";
        }

        if ($dateEnd == '') {
            $dateEnd = date('Y-m-d');
        }

        if ($type_payment != '') {
            $sqlStatusPayment = "and type_payment = '" . $type_payment . "'";
            $linkStatusPayment = "&type_payment=" . $type_payment;
        }

        if ($status_payment != '') {
            $sqlStatus = "and status_payment = '" . $status_payment . "'";
            $linkStatus = "&status_payment=" . $status_payment;
        } else {
            $status_payment = 'SUCCESS';
            $sqlStatus = "and status_payment = 'SUCCESS'";
            $linkStatus = "&status_payment=SUCCESS";
        }
        //===================== Start Kbank =====================
        if (($dateStart != '' || $dateEnd != '') && $type_payment == 'kbank') {
            $kbankDateStart = date('Y-m-d', strtotime($dateStart . "-1 days"));
            $kbankDateStart = $kbankDateStart . " 21:00:00";
            $kbankDateEnd = $dateEnd . " 20:59:59";
            $sqlDateSelect = "and DATE_FORMAT(order_create_on,'%Y-%m-%d %T') BETWEEN '" . $kbankDateStart . "' AND '" . $kbankDateEnd . "'";
        } else {
            $sqlDateSelect = "and DATE_FORMAT(order_create_on,'%Y-%m-%d') BETWEEN '" . $dateStart . "' AND '" . $dateEnd . "'";
        }

        if (($dateStart != '' || $dateEnd != '') && $type_payment == 'psb') {
            $sqlDateSelect = "and ((DATE_FORMAT(order_create_on,'%Y-%m-%d') BETWEEN '" . $dateStart . "' AND '" . $dateEnd . "'  and psb_channel != 'cash') or (DATE_FORMAT(order_create_on,'%Y-%m-%d') BETWEEN '" . $dateStart . "' AND '" . $dateEnd . "') and psb_channel = 'cash' ) ";
        }

        return $qrySelOrder = DB::select("select * from tbl_order
                               where order_id !=''
                                " . $sqlStatusPayment . "
                                " . $sqlDateSelect . " 
                                " . $sqlEvent . "
                                " . $sqlStatus . "
                                and type_payment = '" . $type_payment . "'
                                 
                                order by order_id desc ");

        $TicketForms = TicketForm::where('order_id', $order_id)
                ->orderBy('ticket_id', 'asc')
                ->get();
    }

}
