<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input,
    Redirect,
    DB;
use App\Models\User;
use Auth;
use Session;

class LoginController extends Controller {

    //
    public function getIndex() {
        //return 'Hello';
        $textError = "";

        return \View::make('frontend.login')->with('textError', $textError);
    }

    public function postChecklogin() {
        //Check login Page

        $Username = Input::get('username');
        $PassWord = Input::get('password');
        $textError = "";

        if (\Auth::attempt(['username' => $Username, 'password' => $PassWord])) {
            if (Auth::check()) {
                User::where('username', $Username)->update(array(
                    'lastlogin_on' => date("Y-m-d H:i:s")
                ));
                $user = \Auth::user();

                $level = $user->level;

                /*if ($level == 'Staff Received') {
                    return \Redirect::intended('backoffice_management/order/received')->with('textError', $textError);
                    exit();
                } else if ($level == 'Staff') {
                    return \Redirect::to('backoffice_management/order/financial-classify');
                    exit();
                } else {

                    return \Redirect::intended('backoffice_management/events')->with('textError', $textError);
                    // return \Redirect::to('login')->with('textError', $textError);
                    exit();
                }*/

                if ($level == 'Staff') {
                    /*return \Redirect::to('backoffice_management/main');
                    exit();*/
                    $per = $user->permission_menu;
                    if(!empty($per)){
                        $per = json_decode($per);
                        if(!empty($per->main)&&$per->main==1){
                            return \Redirect::to('backoffice_management/main');
                            exit();
                        } else if(!empty($per->financial_classify)&&$per->financial_classify==1){
                            return \Redirect::to('backoffice_management/order/financial-classify');
                            exit();
                        } else if(!empty($per->stock_report)&&$per->stock_report==1){
                            return \Redirect::to('backoffice_management/order/stock-report');
                            exit();
                        } else if(!empty($per->product_stock)&&$per->product_stock==1){
                            return \Redirect::to('backoffice_management/order/product-stock');
                            exit();
                        } else if(!empty($per->walkin)&&$per->walkin==1){
                            return \Redirect::to('backoffice_management/walkin');
                            exit();
                        } else if(!empty($per->ticket_runner)&&$per->ticket_runner==1){
                            return \Redirect::to('backoffice_management/ticket/ticket-runner');
                            exit();
                        } else if(!empty($per->import_order)&&$per->import_order==1){
                            return \Redirect::to('backoffice_management/import/import-order');
                            exit();
                        } else if(!empty($per->received)&&$per->received==1){
                            return \Redirect::to('backoffice_management/order/received');
                            exit();
                        } else if(!empty($per->staffs)&&$per->staffs==1){
                            return \Redirect::to('backoffice_management/staffs');
                            exit();
                        } else {
                            return \View::make('frontend.login')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
                            exit();
                        }
                    } else {
                        return \View::make('frontend.login')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
                        exit();
                    }
                } else {
                    return \Redirect::intended('backoffice_management/events')->with('textError', $textError);
                    exit();
                }
            }
        } else {#end if Auth
            $textError = "Invalid the username and password to gain access to the backend";
            //return \Redirect::to('login')->with('textError', $textError);
            return \View::make('frontend.login')->with('textError', $textError);
        }
    }

#end
}
