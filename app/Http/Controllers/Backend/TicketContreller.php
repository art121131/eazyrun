<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\ApiContreller;
use Input,
    Redirect,
    DB;
use App\Models\Ticket;
use App\Models\Events;
use App\Models\TypeSouvenir;
use App\Models\Souvenir;
use PHPExcel;
use PHPExcel_IOFactory;
use Auth;

class TicketContreller extends Controller {

    public function getTicketRunner() {
        $params = array();

        $user = \Auth::user();
        $params['event_id'] = $user->event_id;

        $tickets = Ticket::where('ticket_public', '=', '1')
                ->select('ticket_title', 'ticket_id', 'ticket_kilometer')
                ->where('event_id', $params['event_id'])
                ->orderBy('ticket_order_by', 'asc')
                ->get();

        $typeSouvenir = TypeSouvenir::where('souvenir_type_public', '=', '1')
                ->select('souvenir_type_name', 'souvenir_type_id', 'souvenir_id')
                ->where('event_id', $params['event_id'])
                ->orderBy('souvenir_type_order_by', 'asc')
                ->get();

        $Souvenir = Souvenir::select('souvenir_id', 'souvenir_name')
                ->where('event_id', $params['event_id'])
                ->orderBy('souvenir_order_by', 'asc')
                ->get();

        $countries = DB::table('tbl_countries')
                ->select('id', 'country_name')
                ->orderBy('country_name', 'asc')
                ->get();

        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->ticket_runner)||$per->ticket_runner==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }
        //------------------------------------------------------------------------------//

        return \View::make('backend.ticketRunner')->with('tickets', $tickets)
                        ->with('typeSouvenir', $typeSouvenir)
                        ->with('souvenir', $Souvenir)
                        ->with('countries', $countries);
    }
    
    public function getTicketSearch() {
        $params = array();
        $params['dateStart'] = Input::get('dateStart');
        $params['dateEnd'] = Input::get('dateEnd');
        $params['ageStart'] = Input::get('ageStart');
        $params['ageEnd'] = Input::get('ageEnd');
        $params['ticket_id'] = Input::get('ticket_id');
        $params['gender'] = Input::get('gender');

        $params['nameMember'] = Input::get('nameMember');
        $params['bibNo'] = Input::get('bibNo');
        $params['email'] = Input::get('email');
        $params['telNo'] = Input::get('telNo');
        $params['citizen_id'] = Input::get('citizen_id');
        $params['country'] = Input::get('country');

        $user = \Auth::user();
        $params['event_id'] = $user->event_id;

        $tickets = Ticket::where('ticket_public', '=', '1')
                ->select('ticket_title', 'ticket_id', 'ticket_kilometer')
                ->where('event_id', $params['event_id'])
                ->orderBy('ticket_order_by', 'asc')
                ->get();

        $typeSouvenir = TypeSouvenir::where('souvenir_type_public', '=', '1')
                ->select('souvenir_type_name', 'souvenir_type_id', 'souvenir_id')
                ->where('event_id', $params['event_id'])
                ->orderBy('souvenir_type_order_by', 'asc')
                ->get();

        $Souvenir = Souvenir::select('souvenir_id', 'souvenir_name')
                ->where('event_id', $params['event_id'])
                ->orderBy('souvenir_order_by', 'asc')
                ->get();

        $countries = DB::table('tbl_countries')
                ->select('id', 'country_name')
                ->orderBy('country_name', 'asc')
                ->get();

        $listOrderMonthAll = self::queryRunner($params);

        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->ticket_runner)||$per->ticket_runner==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }
        //------------------------------------------------------------------------------//

        return \View::make('backend.apis.getTicketTable')->with('listOrderMonthAll', $listOrderMonthAll)
                        ->with('tickets', $tickets)
                        ->with('typeSouvenir', $typeSouvenir)
                        ->with('souvenir', $Souvenir)
                        ->with('eventID', $params['event_id'])
                        ->with('countries', $countries)
                        ->with('dateStart', $params['dateStart'])
                        ->with('dateEnd', $params['dateEnd'])
                        ->with('ageStart', $params['ageStart'])
                        ->with('ageEnd', $params['ageEnd'])
                        ->with('ticket_id', $params['ticket_id'])
                        ->with('gender', $params['gender'])
                        ->with('nameMember', $params['nameMember'])
                        ->with('bibNo', $params['bibNo'])
                        ->with('telNo', $params['telNo'])
                        ->with('email', $params['email'])
                        ->with('citizen_id', $params['citizen_id'])
                        ->with('country', $params['country']);
    }

    public function getTicketExport() {
        //set params
        $params = array();
        $params['dateStart'] = $_GET['dateStart'];
        $params['dateEnd'] = $_GET['dateEnd'];
        $params['ageStart'] = $_GET['ageStart'];
        $params['ageEnd'] = $_GET['ageEnd'];
        $params['ticket_id'] = $_GET['ticket_id'];
        $params['gender'] = $_GET['gender'];

        $params['nameMember'] = Input::get('nameMember');
        $params['bibNo'] = Input::get('bibNo');
        $params['email'] = Input::get('email');
        $params['telNo'] = Input::get('telNo');
        $params['citizen_id'] = Input::get('citizen_id');
        $params['country'] = Input::get('country');

        $user = \Auth::user();
        $params['event_id'] = $user->event_id;

        //set PHPExcel
        $fileName = 'Members';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties();
        $objPHPExcel->setActiveSheetIndex(0);

        //write Header
        $row = 1;
        $columnHeader = 0;
        $objPHPExcel->getActiveSheet()
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'No.')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'Order No.')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'Ticket No.')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ประเภทตั๋ว')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'วันเวลา ชำระเงิน')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ราคาบัตร')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ของที่ระลึก')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ประเภท')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ชื่อ-นามสกุล')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'หมายเลขวิ่ง')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'เลขบัตรประชาชน')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'วันเดือนปี เกิด')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'อายุ')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'เพศ')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ประเทศ')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'สัญชาติ')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'อีเมล์')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'เบอร์โทร')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'กรุ๊ปเลือด')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ชื่อ ผู้ติดต่อฉุกเฉิน')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'เบอร์โทร ผู้ติดต่อฉุกเฉิน')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ชื่อสมาชิกที่สมัคร')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'วิธีการรับของ')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ชื่อผู้ส่ง')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'ที่อยู่จัดส่ง')
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'จังหวัด')
                ->setCellValueByColumnAndRow($columnHeader++, $row++, 'รหัสไปร์ษณีย์');

        //read data
        $tickets = Ticket::where('ticket_public', '=', '1')
                ->select('ticket_title', 'ticket_id', 'ticket_kilometer')
                ->where('event_id', $params['event_id'])
                ->orderBy('ticket_order_by', 'asc')
                ->get();

        $typeSouvenir = TypeSouvenir::where('souvenir_type_public', '=', '1')
                ->select('souvenir_type_name', 'souvenir_type_id', 'souvenir_id')
                ->where('event_id', $params['event_id'])
                ->orderBy('souvenir_type_order_by', 'asc')
                ->get();

        $souvenir = Souvenir::select('souvenir_id', 'souvenir_name')
                ->where('event_id', $params['event_id'])
                ->orderBy('souvenir_order_by', 'asc')
                ->get();

        $countries = DB::table('tbl_countries')
                ->select('id', 'country_name')
                ->orderBy('country_name', 'asc')
                ->get();

        $listOrderMonthAll = self::queryRunner($params);
        //write body
        $num = 1;
        foreach ($listOrderMonthAll as $orderMonth) {
            if ($orderMonth['data'] != '' || $orderMonth['data'] != '[]') {
                foreach ($orderMonth['data'] as $row_order) {
                    $columnBody = 0;
                    if (( $row_order->type_event_id != '0')) {
                        foreach ($tickets as $row_event_type) {
                            if ($row_event_type->ticket_id == $row_order->type_event_id) {
                                if ($params['event_id'] == 2) {
                                    $ticket_name = $row_event_type->ticket_kilometer . 'km.';
                                } else {
                                    $ticket_name = json_decode($row_event_type->ticket_title, true);
                                    $ticket_name = $ticket_name['data']['th'];
                                }
                            }
                        }
                        if ($row_order->type_souvenir_id != 0) {
                            foreach ($typeSouvenir as $row_souvenir_type) {
                                if ($row_souvenir_type->souvenir_type_id == $row_order->type_souvenir_id) {
                                    $souvenir_type_name = json_decode($row_souvenir_type->souvenir_type_name, true);
                                    $souvenir_type_name = $souvenir_type_name['data']['th'];
                                    if(!empty($row_souvenir_type->souvenir_id)){
                                        foreach ($souvenir as $row_souvenir){
                                            if($row_souvenir_type->souvenir_id == $row_souvenir->souvenir_id){
                                                $souvenir_name = json_decode($row_souvenir->souvenir_name, true);
                                                $souvenir_name = $souvenir_name['data']['th'];
                                            }
                                        }
                                    } else {
                                        $souvenir_name = '-';
                                    }
                                }
                            }
                        } else {
                            $souvenir_name = '-';
                            $souvenir_type_name = '-';
                        }

                        if ($row_order->type_souvenir_id_vip != 0) {
                            foreach ($typeSouvenir as $row_souvenir_type) {
                                if ($row_souvenir_type->souvenir_type_id == $row_order->type_souvenir_id_vip) {
                                    $souvenir_type_nameVip = json_decode($row_souvenir_type->souvenir_type_name, true);
                                    $souvenir_type_nameVip = $souvenir_type_nameVip['data']['th'];
                                }
                            }
                        } else {
                            $souvenir_type_nameVip = '-';
                        }

                        $zones = explode(",", $row_order->zone);
                        $vNo = "";
                        $iNum = 1;
                        foreach ($zones as $zone) {
                            $zone = trim($zone);
                            if ($zone == 1) {
                                $vNo .= $iNum . '.ทุนการศึกษาเด็ก <br>';
                            } else if ($zone == 2) {
                                $vNo .= $iNum . '.อุปกรณ์การแพทย์ ';
                            }
                            $iNum++;
                        }

                        $county = '';
                        if ($row_order->countries_id == '216') {
                            $county = 'Thailand (ไทย)';
                        } else if ($row_order->countries_id != '') {
                            foreach ($countries as $countrie) {
                                if ($countrie->id == $row_order->countries_id) {
                                    $county = $countrie->country_name;
                                }
                            }
                        }

                        $orderFname = '';
                        if ($orderFname != '' && $row_order->order_lname != '') {
                            $orderFname .= $row_order->order_fname . ' ' . $row_order->order_lname . ', ' . $row_order->order_email . ', ' . $row_order->order_tel;
                        } else {
                            $member_id = $row_order->member_id;
                            $rowMember = ApiContreller::getMemberDetail($member_id);
                            if ($member_id != '' && $rowMember->username != '') {
                                $orderFname .= $rowMember->f_name . ' ' . $rowMember->l_name . ', ' . $rowMember->username . ', ' . $rowMember->tel;
                            }
                        }

                        $addr = $row_order->address;
                        if ($row_order->sub_district_id != '') {
                            $DISTRICT = ApiContreller::getFirstDistrict($row_order->sub_district_id);
                            $addr .= ($addr != "" ? " " : "") . $DISTRICT->DISTRICT_NAME;
                        }
                        if ($row_order->district_id != '') {
                            $AMPHUR = ApiContreller::getFirstAmphur($row_order->district_id );
                            $addr .= ($addr != "" ? " " : "") . $AMPHUR->AMPHUR_NAME;
                        }

                        $province = "";
                        if ($row_order->province_id != '' && $row_order->province_id != '0') {
                            $PROVINCE = ApiContreller::getFirstProvince($row_order->province_id);
                            $province .= $PROVINCE->PROVINCE_NAME . ' ';
                        }

                        $zipcode = "";
                        if ($row_order->zipcode != '') {
                            $ZIPCODE = ApiContreller::getFirstZipcode($row_order->zipcode);
                            $zipcode .= $ZIPCODE->ZIPCODE;
                        }
                        
                        $type_payment = $row_order->type_payment;
                        if($type_payment == 'paypal'){
                            $payment = "PAYPAL";
                        }else if($type_payment == 'WALKIN' && $row_order->psb_channel == 'CASH'){
                            $payment = "Walk-in Cash";
                        }else if($type_payment == 'WALKIN' && $row_order->psb_channel == 'CREDIT'){
                            $payment = "Walk-in Credit";
                        }else if($type_payment == 'WALKIN' && $row_order->psb_channel == 'BARCODE'){
                            $payment = "Walk-in Barcode";
                        }else if($type_payment == 'WALKIN' && $row_order->psb_channel == 'OTHER'){
                            $payment = "Walk-in ".$row_order->order_remark;
                        }else if($type_payment == '2c2p' && $row_order->psb_channel == 'Credit'){
                            $payment = "2c2p Credit";
                        }else if($type_payment == '2c2p' && $row_order->psb_channel == 'Cash'){
                            $payment = "2c2p Cash/Internet";
                        }else{
                            $payment = "2c2p";
                        }

                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $num);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columnBody++, $row, $row_order->order_no);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $row_order->ticket_code);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $ticket_name);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $row_order->order_resp_on);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $row_order->ticket_price);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $souvenir_name);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $souvenir_type_name);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, (!empty($row_order->f_name) && !empty($row_order->l_name)) ? $row_order->f_name . " " . $row_order->l_name : ' - ');
                        $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columnBody++, $row, $row_order->bib);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columnBody++, $row, $row_order->citizen_id);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, date('Y-m-d', strtotime($row_order->birthday)));
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, ApiContreller::getAge(date('Y-m-d', strtotime($row_order->birthday))));
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $row_order->gender);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $county);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $row_order->nationality);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $row_order->email);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columnBody++, $row, $row_order->tel);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $row_order->blood_group);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $row_order->ticket_contact_name);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columnBody++, $row, $row_order->ticket_contact_tel);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $orderFname);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, ($row_order->order_type_delivery == 'SEND') ? "จัดส่งที่บ้าน" : "รับเอง");
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $payment);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, ($row_order->name_delivery != "") ? $row_order->name_delivery : "-");
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $addr);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row, $province);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnBody++, $row++, $zipcode);
                    }

                    $num++;
                }
            }
        }

        //set Auto format size
        $cols = explode(":", trim(preg_replace('/\d+/u', '', $objPHPExcel->getActiveSheet()->calculateWorksheetDimension())));
        $col = $cols[0];
        $end = ++$cols[1];

        while ($col != $end) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $col++;
        }

        //output
        $objPHPExcel->getActiveSheet()->setTitle($fileName);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    public static function queryRunner(array $params) {

        $user = \Auth::user();
        $params['event_id'] = $user->event_id;

        $sqlEvent = " and tbl_order.event_id = '" . $params['event_id'] . "' ";

        if (!empty($params['dateStart'])) {
            $sqlEvent .= " and order_resp_on >= '" . $params['dateStart'] . "' ";
        }

        if (!empty($params['dateEnd'])){
            $sqlEvent .= " and order_resp_on <= '" . $params['dateEnd'] . "' ";
        }

        if (!empty($params['ageStart'])){
            $sqlEvent .= " and YEAR(CURDATE()) - YEAR(tbl_ticket.birthday) >= '" . $params['ageStart'] . "' ";
            /* $sqlEvent .= " and YEAR(CURDATE()) - YEAR(tbl_ticket.birthday) - "
              . " IF(STR_TO_DATE(CONCAT(YEAR(CURDATE()), '-', MONTH(tbl_ticket.birthday), '-', DAY(tbl_ticket.birthday))"
              . " ,'%Y-%c-%e') > CURDATE(), 1, 0 )  >= '".$params['ageStart']."' "; */
        }

        if (!empty($params['ageEnd'])) {
            $sqlEvent .= " and YEAR(CURDATE()) - YEAR(tbl_ticket.birthday) <= '" . $params['ageEnd'] . "' ";
            /* $sqlEvent .= " and YEAR(CURDATE()) - YEAR(tbl_ticket.birthday) - "
              . " IF(STR_TO_DATE(CONCAT(YEAR(CURDATE()), '-', MONTH(tbl_ticket.birthday), '-', DAY(tbl_ticket.birthday))"
              . " ,'%Y-%c-%e') > CURDATE(), 1, 0 )  <= '".$params['ageEnd']."' "; */
        }

        if (!empty($params['ticket_id'])) {
            $sqlEvent .= " and tbl_ticket.type_event_id = '" . $params['ticket_id'] . " '";
        }

        if (!empty($params['gender'])) {
            $sqlEvent .= " and tbl_ticket.gender LIKE '%" . $params['gender'] . "%' ";
        }

        if (!empty($params['country'])) {
            $sqlEvent .= " and tbl_ticket.countries_id = '" . $params['country'] . "' ";
        }

        if (!empty($params['nameMember'])) {
            $sqlEvent .= " and (tbl_ticket.f_name LIKE '%" . $params['nameMember'] . "%' OR tbl_ticket.l_name LIKE '%" . $params['nameMember'] . "%' ) ";
        }

        if (!empty($params['bibNo'])) {
            $sqlEvent .= " and tbl_ticket.bib LIKE '%" . $params['bibNo'] . "%' ";
        }

        if (!empty($params['telNo'])) {
            $sqlEvent .= " and tbl_ticket.tel LIKE '%" . $params['telNo'] . "%' ";
        }

        if (!empty($params['email'])) {
            $sqlEvent .= " and tbl_ticket.email LIKE '%" . $params['email'] . "%' ";
        }

        if (!empty($params['citizen_id'])) {
            $sqlEvent .= " and tbl_ticket.citizen_id LIKE '%" . $params['citizen_id'] . "%' ";
        }


        /* if(!empty($params['dateStart'])|| !empty($params['dateEnd'])){
          $sqlDateSelect =   "and order_resp_on BETWEEN '".$params['dateStart']."' AND '".$params['dateEnd']."'";
          } *///End if

        $events = Events::where('tbl_event.event_id', $params['event_id'])
                ->select('tbl_event.event_register_start_on', 'tbl_event.event_register_end_on')
                ->orderBy('tbl_event.event_id', 'desc')
                ->first();

        $eventStart = date('Y-m', strtotime($events->event_register_start_on . "-1 days")) . '-01';
        $dateNow = date('Y-m-d');

        $start = strtotime($eventStart);
        $end = strtotime($dateNow);

        $startmoyr = date('Y', $start) . date('m', $start);
        $endmoyr = date('Y', $end) . date('m', $end);
        $resultsAll = 0;
        $i = 1;

        while ($startmoyr <= $endmoyr) {

            $tableMonth = date("y_m", $start);
            $dbTableName = 'tbl_order_' . $tableMonth;
            $dbtableNameTicket = 'tbl_order_ticket_' . $tableMonth;

            $start = strtotime("+1month", $start);
            $startmoyr = date('Y', $start) . date('m', $start);

            $listOrderMonth['order_by'] = $i;
            $listOrderMonth['month'] = $tableMonth;

            $orderLists = DB::select("select
                tbl_order.order_id, tbl_order.order_no, tbl_ticket.f_name, tbl_ticket.l_name, tbl_ticket.citizen_id, tbl_ticket.email, 
                tbl_ticket.bib, tbl_order.type_payment, tbl_order.psb_channel, tbl_order.order_remark,
                tbl_ticket.recive_souvenir, tbl_ticket.ticket_id , tbl_ticket.type_event_id, tbl_ticket.type_souvenir_id, tbl_ticket.ticket_receive_type, tbl_ticket.ticket_receive_by
                , tbl_ticket.countries_id   , tbl_ticket.birthday, tbl_ticket.gender , tbl_ticket.nationality, tbl_ticket.tel , tbl_ticket.blood_group
                , tbl_ticket.blood_group, tbl_ticket.ticket_contact_name, tbl_ticket.ticket_contact_tel, tbl_order.order_lname, tbl_order.order_fname,
                tbl_order.order_email, tbl_order.order_tel, tbl_order.order_type_delivery , tbl_order.member_id , tbl_ticket.zone,   tbl_ticket.type_souvenir_id_vip
                , tbl_ticket.name_delivery , tbl_ticket.address , tbl_ticket.province_id , tbl_ticket.district_id , tbl_ticket.sub_district_id , tbl_ticket.zipcode , tbl_ticket.ticket_price
                 , tbl_ticket.ticket_code
                ,tbl_order.order_resp_on

                from " . $dbTableName . " as tbl_order
                left join " . $dbtableNameTicket . " as tbl_ticket
                on(tbl_order.order_id = tbl_ticket.order_id)
                where (tbl_order.status_payment = 'SUCCESS'  )

                " . $sqlEvent . "

                GROUP BY tbl_ticket.ticket_id
                order by  tbl_ticket.type_event_id desc, tbl_ticket.countries_id = '216' asc,  tbl_ticket.type_event_id asc,
                    tbl_ticket.countries_id = '0', tbl_order.order_no desc
            ");

            if ($orderLists != '[]' && $orderLists != '') {
                $listOrderMonth['data'] = $orderLists;
            } else {
                $listOrderMonth['data'] = '';
            }

            $listOrderMonthAll[] = $listOrderMonth;
            $i++;
        }

        return $listOrderMonthAll;
    }

    public function getTicketData() {

        $data_id = Input::get('dataID');
        $dataID = explode('-', $data_id);
        $ticket_id = $dataID[0];
        $yearMonth = $dataID[1];

        $ticket = DB::table('tbl_order_ticket_'.$yearMonth)
                ->select("*")
                ->where('ticket_id', $ticket_id)
                ->first();

        $typeSouvenir = TypeSouvenir::where('souvenir_type_public', '=', '1')
                ->where('event_id', $ticket->event_id)
                ->orderBy('souvenir_type_order_by', 'asc')
                ->get();

        $countries = DB::table('tbl_countries')
                ->select('id', 'country_name')
                ->orderBy('country_name', 'asc')
                ->get();

        return $data = [
            'ticket' => $ticket,
            'type_souvenir' => $typeSouvenir,
            'countries' => $countries,
            'year_month' => $yearMonth
        ];
    }

    public function postTicketEdit() {

        $inputAll = Input::all();
        $results = array();
        $tableNameTicket = 'tbl_order_ticket_' . $inputAll['year_month'];
        DB::beginTransaction();

        try {
            DB::table($tableNameTicket)
                ->where('ticket_id', $inputAll['ticket_id'])
                ->update([
                        'pre_name' => $inputAll['pre_name'],
                        'f_name' => $inputAll['f_name'],
                        'l_name' => $inputAll['l_name'],
                        'bib' => $inputAll['bib_no'],
                        'citizen_id' => $inputAll['citizen_id'],
                        'email' => $inputAll['email'],
                        'tel' => $inputAll['tel'],
                        'birthday' => $inputAll['birthday'],
                        'countries_id' => $inputAll['countries_id'],
                        'nationality' => $inputAll['nationality'],
                        'religion' => $inputAll['religion'],
                        'blood_group' => $inputAll['blood_group'],
                        'gender' => $inputAll['gender'],
                        'type_souvenir_id' => $inputAll['type_souvenir_id'],
                        'ticket_contact_name' => $inputAll['ticket_contact_name'],
                        'ticket_contact_tel' => $inputAll['ticket_contact_tel'],
                    ]);
            DB::commit();
            $results['status'] = "ok";
            return $results;
        } catch (\Throwable $e) {
            DB::rollback();
            $results['status'] = "fail";
            $results['error'] = "$e";
            return $results;
        }

    }

}
