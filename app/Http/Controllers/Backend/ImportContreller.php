<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\ApiContreller;
use Input,
    Redirect,
    DB;
use App\Models\Ticket;
use App\Models\Events;
use App\Models\TypeSouvenir;
use App\Models\Souvenir;
use PHPExcel;
use PHPExcel_IOFactory;
use Auth;
use App\Http\Controllers\Backend\TicketContreller;
set_time_limit(120);
ini_set('memory_limit', '256M');

class ImportContreller extends Controller {

    public function getImportOrder() {
        
        $user = \Auth::user();
        
        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->import_order)||$per->import_order==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }    
        //------------------------------------------------------------------------------//

        return \View::make('backend.importOrder');
    }
    
    public function postImportOrderReview() {
        $fileUpload = $_FILES["fileUpload"]["tmp_name"];
        $inputFileType = PHPExcel_IOFactory::identify($fileUpload);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($fileUpload);

        $data = array(1, $objPHPExcel->getActiveSheet()->toArray(null, true, true, true));
        
        $user = \Auth::user();
        $params['event_id'] = $user->event_id;
        
        $tickets = Ticket::where('ticket_public', '=', '1')
                ->select('ticket_title', 'ticket_id', 'ticket_kilometer')
                ->where('event_id', $params['event_id'])
                ->orderBy('ticket_order_by', 'asc')
                ->get();

        $typeSouvenir = TypeSouvenir::where('souvenir_type_public', '=', '1')
                ->select('souvenir_type_name', 'souvenir_type_id', 'souvenir_id')
                ->where('event_id', $params['event_id'])
                ->orderBy('souvenir_type_order_by', 'asc')
                ->get();

        $Souvenir = Souvenir::select('souvenir_id', 'souvenir_name')
                ->where('event_id', $params['event_id'])
                ->orderBy('souvenir_order_by', 'asc')
                ->get();
        
        $results = array();
        
        $ticketTable = array();
 
        try {
            if ($data[0] == 1) {
                $results['found'] = [];
                $results['not'] = [];
                foreach ($data[1] as $key => $value) {
                    if ($value['A'] != "" && $key != 1) {
                        $tableOrder = "tbl_order_" . substr($value['A'], 1, 2) . '_' . substr($value['A'], 3, 2);
                        $tableOrderTicket = "tbl_order_ticket_" . substr($value['A'], 1, 2) . '_' . substr($value['A'], 3, 2);
                        if(empty($ticketTable[$tableOrderTicket])){
                            $ticketTable[$tableOrderTicket] = DB::select(" SELECT t.* ,o.order_no, tt.ticket_title, ts.souvenir_type_name, s.souvenir_name "
                                                        . " FROM ".$tableOrderTicket." t "
                                                        . " LEFT JOIN ".$tableOrder." o ON (o.order_id = t.order_id) "
                                                        . " LEFT JOIN tbl_type_ticket tt ON (tt.event_id = t.event_id AND tt.ticket_id = t.type_event_id) "
                                                        . " LEFT JOIN tbl_type_souvenir ts ON (ts.souvenir_type_id = t.type_souvenir_id AND ts.event_id = t.event_id) "
                                                        . " LEFT JOIN tbl_souvenir s ON (s.souvenir_id = ts.souvenir_id AND s.event_id = t.event_id) "
                                                        . " WHERE t.event_id='".$params['event_id']."'");
                        }
                        
                        $data = array();
                        
                        foreach ($ticketTable[$tableOrderTicket] as  $k => $v){
                            if($v->ticket_code == $value['A']){
                                $data = $v;
                            }
                        }
                        
                        if(!empty($data)){
                            $data->bib = $value['B'];
                            array_push($results['found'], $data);
                        } else {
                            array_push($results['not'], $value);
                        }
                    }
                }
                $results['status'] = 'OK';
            } else {
                $results['status'] = 'Fail';
            }
        } catch (\Throwable $e) {
            $results['status'] = "fail";
            $results['error'] = "$e";
        }
        return $results;
    }

    public function postImportOrderSave() {

        $fileUpload = $_FILES["fileUpload"]["tmp_name"];
        $inputFileType = PHPExcel_IOFactory::identify($fileUpload);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($fileUpload);

        $data = array(1, $objPHPExcel->getActiveSheet()->toArray(null, true, true, true));

        if ($data[0] == 1) {
            foreach ($data[1] as $key => $value) {
                if ($value['A'] != "" && $key != 1) {
                    $tableOrder = "tbl_order_ticket_" . substr($value['A'], 1, 2) . '_' . substr($value['A'], 3, 2);

                    $ticket = DB::table($tableOrder)
                            ->select('ticket_id')
                            ->where('ticket_code', $value['A'])
                            ->first();
                    
                    if(!empty($ticket->ticket_id)){
                        DB::table($tableOrder)
                            ->where('ticket_id', $ticket->ticket_id)
                            ->update(
                            [
                                'ticket_code' => $value['A'],
                                'bib' => $value['B']
                            ]
                        );                   
                    }               
                }
            }
        }
        
        return 1;
    }

    public function getDownloadExample (){
        
        $user = \Auth::user();
        $params['event_id'] = $user->event_id;

        //set PHPExcel
        $fileName = 'TicketList';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties();
        $objPHPExcel->setActiveSheetIndex(0);

        //write Header
        $row = 1;
        $columnHeader = 0;
        $objPHPExcel->getActiveSheet()
                ->setCellValueByColumnAndRow($columnHeader++, $row, 'Ticket No.')
                ->setCellValueByColumnAndRow($columnHeader++, $row++, 'bib');
        
        $listOrderMonthAll = TicketContreller::queryRunner($params);
        
        $num = 1;
        foreach ($listOrderMonthAll as $orderMonth) {
            if ($orderMonth['data'] != '' || $orderMonth['data'] != '[]') {
                foreach ($orderMonth['data'] as $row_order) {
                    $columnBody = 0;
                    $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columnBody++, $row, $row_order->ticket_code);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columnBody++, $row++, $row_order->bib);
                    $num++;
                }
            }
        }
        
        //set Auto format size
        $cols = explode(":", trim(preg_replace('/\d+/u', '', $objPHPExcel->getActiveSheet()->calculateWorksheetDimension())));
        $col = $cols[0];
        $end = ++$cols[1];

        while ($col != $end) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $col++;
        }

        //output
        $objPHPExcel->getActiveSheet()->setTitle($fileName);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        
        /*$file = public_path(). "/resources/downloads/example.xls";
        return response()->download($file);*/
    }
}
