<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input, Redirect, DB; 
use App\Models\User;
use App\Models\Banner;
use App\Models\Information;
use App\Models\LogStaff; 
use App\Models\TextMenu; 
use App\Models\Lang; 
use Auth;
use Session;

class PageContreller extends Controller
{
	
	 public function __construct() {
		$this->middleware('auth');
	} 
    //
	public function getIndex($id=null){ 

	 return 'ok'; 
		  
	} 

	public function getBanner(){   
        
        //** Select Start movie_banner **** 
      $Banner = Banner::where('banner_publish', '!=', '2') 
                              ->where('banner_id', '!=', 0)
                              ->orderBy('order_by', 'asc')
                              ->orderBy('banner_create', 'desc')
                              ->get();
        //** Select End movie_banner ****
                                 
       return  \View::make('backend.banner')-> with('sbdBanner', $Banner);
     } 
      public function getBannerAdd(){  
        return  \View::make('backend.bannerAdd');
      }
       public function getBannerEdit($banner_id){  

         $contents = Banner::where('banner_id', $banner_id)
                                ->where('banner_publish', '!=', '2') 
                                ->first(); 

         return \View::make('backend.bannerEdit')-> with('row', $contents);
      }

      public function getDragdropBanner(){  
     
        $result = Input::get('table-1');
        
        //$num = count($result);
        $order  = 0;
        foreach($result as $value) {
            $order++;
            DB::table('tbl_banner')
                    ->where('banner_id', $value)
                    ->update(array('order_by' => $order)); 
            }#end foreach
      }

      public function postBannerSaveform(){  

        $action = Input::get('action');   
        $user = \Auth::user();
        $user_id = $user->id;

         if($action == 'addBanner'){//
              $file2 = Input::file('file2');
              $banner_url = Input::get('banner_url'); 
             if($file2 != ''){  
                
                 $destinationPath = 'public/resources/uploads/content/';  
                 $nowTime = date("YmdHi");
                 $filename2 = str_random(10).''.$nowTime.'.'.$file2->getClientOriginalExtension();
                 Input::file('file2')->move($destinationPath, $filename2); 
                
            }else{
                $filename2 = '';
            }

            $Banner = new Banner(); 
            $Banner->banner_name         = Input::get('banner_name'); 
            $Banner->banner_url          = Input::get('banner_url'); 
            $Banner->banner_publish      = '1';    
            @$Banner->banner_pic_mobile  = $filename2; 
            $Banner->banner_create = date("Y-m-d H:i:s");  
            $Banner->save(); //Add to DB 

            $LogStaff = new LogStaff(); 
            $LogStaff->staff_id           = $user_id; 
            $LogStaff->log_staff_detail       = 'เพิ่ม Banner : '.Input::get('banner_name'); 
            $LogStaff->log_staff_create_date_on = date("Y-m-d H:i:s"); 
            $LogStaff->save(); //Add to DB 
              
         }//addBanner

          if($action == 'editBanner'){//
                $oldImageMobile = Input::get('oldImageMobile'); 
                $file2 = Input::file('file2');

                if($file2 != ''){
                 $destinationPath = 'public/resources/uploads/content/';  
                $nowTime = date("YmdHi");
                $filename2 = str_random(10).''.$nowTime.'.'.$file2->getClientOriginalExtension();
                Input::file('file2')->move($destinationPath, $filename2);
                
                    
            }else{//end if file
                $filename2 = Input::get('oldImageMobile');  
            }

             Banner::where('banner_id', Input::get('banner_id'))
                  ->update(array('banner_name' =>  Input::get('banner_name') ,
                                 'banner_pic_mobile' => $filename2 ,  
                                 'banner_url' => Input::get('banner_url'),
                                 'banner_create' => date("Y-m-d H:i:s")  )); 

              $LogStaff = new LogStaff(); 
              $LogStaff->staff_id           = $user_id; 
              $LogStaff->log_staff_detail       = 'แก้ไข Banner : '.Input::get('banner_name'); 
              $LogStaff->log_staff_create_date_on = date("Y-m-d H:i:s"); 
              $LogStaff->save(); //Add to DB    
          }//editBanner 

        if($action == 'updateBannerLists'){//  Check select dropdown
            $action_up = Input::get('action_up');         
        
            $input = Input::all();  
            $banner_id = $input['banner_id'];         
            $num =  count($banner_id);  
              
            switch($action_up){
                case "ลบ | Delete" : 
                $query = "update tbl_banner set banner_publish ='2 ' where ";
                for( $i = 0; $i < $num; $i++){
                    $query .= "(banner_id = $banner_id[$i])";
                    if($i < $num-1){
                        $query .= " or ";
                    }
                }  
                DB::update($query);
                    
                break;
                case "เผยแพร่ | Publish" :
                 $query = "update tbl_banner set banner_publish ='1' where ";
                for( $i = 0; $i < $num; $i++){
                    $query .= "(banner_id = $banner_id[$i])";
                    if($i < $num-1){
                        $query .= " or ";
                    }
                }  
                DB::update($query);  
                     
                break;                    
                case "ซ่อน | Unpublish" :
                $query = "update tbl_banner set banner_publish ='0' where ";
                for( $i = 0; $i < $num; $i++){
                    $query .= "(banner_id = $banner_id[$i])";
                    if($i < $num-1){
                        $query .= " or ";
                    }
                }  
                DB::update($query);  
                break;
            }  
             
        }#end input Select ************* 

        return Redirect::to('backoffice_management/page/banner'); 
      }

      public function getAnnouncement(){   
          
          $textMessage = false;
          $information = Information::where('pages_id' , '1')  
                                     ->first(); 

         return \View::make('backend.pageAnnouncement')-> with('information', $information)-> with('textMessage', $textMessage);
      }// end getAnnouncement

       public function postAnnouncement(){  

          $user = \Auth::user();
          $user_id = $user->id; 

          $has_text_news    = Input::get('has_text_news');
          
          $outputArrTopic['topic'] = "announcement"; 
          $outputArrTopicLang['en'] = Input::get('text_news_en');
          $outputArrTopicLang['th'] = Input::get('text_news_th');
          $outputArrTopicLang['cn'] = Input::get('text_news_cn');
          $outputArrTopicLang['jp'] = Input::get('text_news_jp');
          $outputArrTopicLang['de'] = Input::get('text_news_de');
          $outputArrTopic['data'] = $outputArrTopicLang;
          $text_news = json_encode($outputArrTopic);

          $textMessage = true;

          Information::where('pages_id', '1')
                               ->update(array('text_news' => $text_news, 'has_text_news' => $has_text_news)); 

           $information = Information::where('pages_id' , '1')  
                                       ->first(); 

            $LogStaff = new LogStaff(); 
            $LogStaff->staff_id           = $user_id; 
            $LogStaff->log_staff_detail       = 'แก้ไข Announcement : '.$text_news; 
            $LogStaff->log_staff_create_date_on = date("Y-m-d H:i:s"); 
            $LogStaff->save(); //Add to DB 

           return  \View::make('backend.pageAnnouncement')-> with('information', $information)-> with('textMessage', $textMessage);
      }//end postAnnouncement

      public function getLanding(){   
          
          $textMessage = false;
          $information = Information::where('pages_id' , '1')  
                                     ->first(); 

         return \View::make('backend.pageLandingPage')-> with('information', $information)-> with('textMessage', $textMessage);
      }// end getLanding

    public function postSaveformLoading()
    {
      $action = Input::get('action');   
      $user = \Auth::user();
      $user_id = $user->id; 

      $action = Input::get('action');

      if($action == 'updatePopupPage'){
            $textMessage = true;

            $file = Input::file('file1');    
            $has_pages_popup  = Input::get('has_pages_popup'); 
            $pages_popup_url  = Input::get('pages_popup_url');  
          
            $oldImages  = Input::get('oldImages');    
         
            if($action != ''){

                if($file != ''){ 
                     
                        $destinationPath = 'public/resources/uploads/content/';  
                        $nowTime = date("YmdHi");
                        $filename = str_random(10).''.$nowTime.'.'.$file->getClientOriginalExtension();
                        Input::file('file1')->move($destinationPath, $filename);
                        
                }else{//end if file
                    $filename = Input::get('oldImages');    
                }

                Information::where('pages_id', '1')
                                    ->update(array('has_pages_popup' => $has_pages_popup, 
                                                    'pages_popup_url' => $pages_popup_url,
                                                    'pages_popup_img' => $filename)); 

                 $information = Information::where('pages_id' , '1')  
                                     ->first();

                $LogStaff = new LogStaff(); 
                $LogStaff->staff_id           = $user_id; 
                $LogStaff->log_staff_detail       = 'แก้ไข Loading Page : '; 
                $LogStaff->log_staff_create_date_on = date("Y-m-d H:i:s"); 
                $LogStaff->save(); //Add to DB 

                return  \View::make('backend.pageLandingPage')-> with('information', $information)-> with('textMessage', $textMessage);
            } //action
      }//updatePopupPage 

    }//postSavePage

     public function getFooterImg(){   
          
          $textMessage = false;
          $information = Information::where('pages_id' , '1')  
                                     ->first(); 

         return \View::make('backend.pageFooterImg')-> with('information', $information)-> with('textMessage', $textMessage);
      }// end getLanding

       public function postSaveformFooterImg()
    {
      $action = Input::get('action');   
      $user = \Auth::user();
      $user_id = $user->id; 

      $action = Input::get('action');

      if($action == 'updateFooterImg'){
            $textMessage = true;

            $file = Input::file('file1');    
           
            $oldImages  = Input::get('oldImages');    
         
            if($action != ''){

                if($file != ''){ 
                     
                        $destinationPath = 'public/resources/uploads/content/';  
                        $nowTime = date("YmdHi");
                        $filename = str_random(10).''.$nowTime.'.'.$file->getClientOriginalExtension();
                        Input::file('file1')->move($destinationPath, $filename);
                        
                }else{//end if file
                    $filename = Input::get('oldImages');    
                }

                Information::where('pages_id', '1')
                                    ->update(array( 'footer_img' => $filename)); 

                 $information = Information::where('pages_id' , '1')  
                                     ->first();

                $LogStaff = new LogStaff(); 
                $LogStaff->staff_id           = $user_id; 
                $LogStaff->log_staff_detail       = 'แก้ไข รูป Footer : '; 
                $LogStaff->log_staff_create_date_on = date("Y-m-d H:i:s"); 
                $LogStaff->save(); //Add to DB 

                return  \View::make('backend.pageFooterImg')-> with('information', $information)-> with('textMessage', $textMessage);
            } //action
      }//updatePopupPage 

    }//postSavePage
    


     public function getDisclaimer(){   
          
          $textMessage = false;
          $information = Information::where('pages_id' , '1')  
                                     ->first(); 

         return \View::make('backend.pageDisclaimer')-> with('information', $information)-> with('textMessage', $textMessage);
      }// end getAnnouncement


       public function postDisclaimer(){ 
          $user = \Auth::user();
          $user_id = $user->id; 
 
          $outputArrTopic['topic'] = "Disclaimer"; 
          $outputArrTopicLang['en'] = Input::get('disclaimer_en');
          $outputArrTopicLang['th'] = Input::get('disclaimer_th');
          $outputArrTopicLang['cn'] = Input::get('disclaimer_cn');
          $outputArrTopicLang['jp'] = Input::get('disclaimer_jp');
          $outputArrTopicLang['de'] = Input::get('disclaimer_de');
          $outputArrTopic['data'] = $outputArrTopicLang;
          $details = json_encode($outputArrTopic);

          $textMessage = true;

          Information::where('pages_id', '1')
                               ->update(array('disclaimer_page' => $details )); 

           $information = Information::where('pages_id' , '1')  
                                       ->first(); 

            $LogStaff = new LogStaff(); 
            $LogStaff->staff_id           = $user_id; 
            $LogStaff->log_staff_detail       = 'แก้ไข Disclaimer : '.$details; 
            $LogStaff->log_staff_create_date_on = date("Y-m-d H:i:s"); 
            $LogStaff->save(); //Add to DB 

           return  \View::make('backend.pageDisclaimer')-> with('information', $information)-> with('textMessage', $textMessage);
       }

       public function getSetting(){  
          
          $TextMenu = TextMenu::where('text_menu_id', '!=' , '')  
                                ->get(); 

         return \View::make('backend.menuSetting')-> with('TextMenu', $TextMenu);
      }// end getSetting
       public function getCreateTextMenu(){   
          $lang = Lang::where('lang_status' , '1') 
                      ->orderBy('lang_id', 'asc')
                      ->get();

        return \View::make('backend.createMenuSetting')->with('lang', $lang);

      }//getCategoryLists
      
      public function postCreateTextMenu(){   

        $user = \Auth::user();
        $user_id = $user->id; 

         $lang  = Lang::where('lang_status' , '1') 
                      ->orderBy('lang_id', 'asc')
                      ->get();

        $outputArrTopic['topic'] = "Text Menu"; 
         foreach($lang as $rowLang){ 
           $outputArrTopicLang[$rowLang->lang_name_l]  =  Input::get('text_name_'.$rowLang->lang_name_l); 
         }
        $outputArrTopicLang['remark'] = Input::get('remark');
        
        $outputArrTopic['data'] = $outputArrTopicLang;
        $text_name = json_encode($outputArrTopic);

        $TextMenu = new TextMenu(); 
        $TextMenu->text_menu_detail          = $text_name; 
        $TextMenu->text_menu_status         = 1;   
        $TextMenu->save(); //Add to DB 

        $LogStaff = new LogStaff(); 
        $LogStaff->staff_id           = $user_id; 
        $LogStaff->log_staff_detail       = ' เพิ่ม '. Input::get('text_name_en'); 
        $LogStaff->log_staff_create_date_on = date("Y-m-d H:i:s"); 
        $LogStaff->save(); //Add to DB
    
        return Redirect::to('backoffice_management/page/setting'); 
      }//getCategoryLists

       public function getEditTextMenu($id){   
         $lang  = Lang::where('lang_status' , '1') 
                      ->orderBy('lang_id', 'asc')
                      ->get(); 

         $textMenu = TextMenu::where('text_menu_id' , $id)  
                              ->first();

        return \View::make('backend.editMenuSetting')-> with('textMenu', $textMenu)->with('lang', $lang);

      }//getCategoryLists
      public function postEditTextMenu(){   
        $user = \Auth::user();
        $user_id = $user->id; 

        $text_menu_id = Input::get('text_menu_id'); 

         $lang  = Lang::where('lang_status' , '1') 
                      ->orderBy('lang_id', 'asc')
                      ->get(); 


        $outputArrTopic['topic'] = "Text Menu"; 
         foreach($lang as $rowLang){ 
           $outputArrTopicLang[$rowLang->lang_name_l]  =  Input::get('text_name_'.$rowLang->lang_name_l); 
         }
        $outputArrTopicLang['remark'] = Input::get('remark');
        $outputArrTopic['data'] = $outputArrTopicLang;
        $text_name = json_encode($outputArrTopic);

        TextMenu::where('text_menu_id', $text_menu_id)
              ->update(array('text_menu_detail' => $text_name ));

        $LogStaff = new LogStaff(); 
        $LogStaff->staff_id           = $user_id; 
        $LogStaff->log_staff_detail       = ' แก้ไข '. Input::get('text_name_en'); 
        $LogStaff->log_staff_create_date_on = date("Y-m-d H:i:s"); 
        $LogStaff->save(); //Add to DB
              
          return Redirect::to('backoffice_management/page/setting'); 

      }//getCategoryLists
    
}
