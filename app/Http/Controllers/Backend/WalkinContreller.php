<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input,
    Redirect,
    DB;
use App\Models\User;
use App\Models\Events;
use App\Models\Ticket;
use App\Models\Souvenir;
use App\Models\TypeSouvenir;
use App\Models\Order;
use App\Models\Member;
use App\Models\TicketForm;
use App\Models\Product;
use Auth;
use Session;

class WalkinContreller extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    //
    public function getIndex() {
        //  $eventID = Input::get('event_id');
        $eventID = 9;

        $user = \Auth::user();
        $user_level = $user->level;

        if ($user_level == 'Staff') {
            $eventID = $user->event_id;
        }//event_id

        $events = Events::where('event_status', '!=', '2')
                ->where('event_id', $eventID)
                ->orderBy('event_id', 'desc')
                ->first();

        $tickets = Ticket::where('ticket_public', '=', '1')
                ->where('ticket_frontend_public', '=', '1')
                ->where('event_id', $eventID)
                ->orderBy('ticket_order_by', 'asc')
                ->get();

        $products = Product::where('product_publish', '=', '1')
                ->where('event_id', $eventID)
                ->orderBy('product_orderby', 'asc');
        $products = $products->get();
        $countProducts = $products->count();

        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->walkin)||$per->walkin==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }
        //------------------------------------------------------------------------------//

        return \View::make('backend.walkin')
                        ->with('events', $events)
                        ->with('products', $products)
                        ->with('countProducts', $countProducts)
                        ->with('tickets', $tickets);
    }

    public function postAddTicket() {

        $ticketBoxID = Input::get('ticketBoxID');
        $countTicket = Input::get('oldValue');
        $ticketNumber = Input::get('ticketNumber');
        $lang = Input::get('lang');
        $eventID = Input::get('eventID');
        $totalPlus = Input::get('totalPlus');


        $events = Events::where('tbl_event.event_status', '=', '1')
                ->where('tbl_event.event_id', $eventID)
                ->select('tbl_event.event_field_required')
                ->orderBy('tbl_event.event_id', 'desc')
                ->first();

        $souvenirs = Souvenir::where('souvenir_public', '=', '1')
                ->where('souvenir_frontend_public', '=', '1')
                ->where('event_id', $eventID)
                ->orderBy('souvenir_order_by', 'asc')
                ->get();
        $typeSouvenir = TypeSouvenir::where('souvenir_type_public', '=', '1')
                ->where('event_id', $eventID)
                ->orderBy('souvenir_type_order_by', 'asc')
                ->get();

        $tickets = Ticket::where('ticket_public', '=', '1')
                ->where('ticket_frontend_public', '=', '1')
                ->where('ticket_id', $ticketBoxID)
                ->orderBy('ticket_order_by', 'asc')
                ->first();

        $countries = DB::table('tbl_countries')
                ->select('id', 'country_name')
                ->orderBy('country_name', 'asc')
                ->get();

        return \View::make('frontend.apis.getTicketFormBackend')
                        ->with('lang', $lang)
                        ->with('totalPlus', $totalPlus)
                        ->with('events', $events)
                        ->with('ticketNumber', $ticketNumber)
                        ->with('countTicket', $countTicket)
                        ->with('ticketBoxID', $ticketBoxID)
                        ->with('eventID', $eventID)
                        ->with('souvenirs', $souvenirs)
                        ->with('typeSouvenir', $typeSouvenir)
                        ->with('countries', $countries)
                        ->with('tickets', $tickets);
    }

//add ticket

    public function postSaveOrder() {

        $eventID = Input::get('event_id');

        $inputAll = Input::all();

        $net_amt = 0;
        foreach ($inputAll['ticket_num'] as $key => $value) {
            if ($value > 0) {
                $net_amt += $inputAll['ticket_price'][$key];
            }
        }

        $t = microtime(true);
        $micro = sprintf("%03d", ($t - floor($t)) * 10000);

        $orderNumber = date('ymdhis') . $micro;
        $orderSess = str_random(12);

        //******************* Add order Start *******************//
        $tableNameOrder = 'tbl_order_' . date('y_m'); // tbl_order_17_08 tbl_order_2017_08

        DB::beginTransaction();
        $results = array();
        try {
            DB::table($tableNameOrder)->insert(
                    ['status_payment' => 'SUCCESS',
                        'type_payment' => 'WALKIN',
                        'psb_channel' => $inputAll['psb_channel'],
                        'order_session' => $orderSess,
                        'order_no' => $orderNumber,
                        'event_id' => $eventID,
                        'member_id' => 0,
                        'order_create_on' => date("Y-m-d H:i:s"),
                        'order_resp_on' => date("Y-m-d H:i:s"),
                        'order_price' => $net_amt,
                        'order_price_net' => $net_amt,
                        'order_remark' => (!empty($inputAll['order_remark']) ? $inputAll['order_remark'] : null )
                    ]
            );

            $Order = DB::table($tableNameOrder)
                    ->where('order_session', $orderSess)
                    ->select('order_id')
                    ->first();
            $orderID = $Order->order_id;

            //******************* Add order End  *******************//

            $tableNameTicket = 'tbl_order_ticket_' . date('y_m');

            foreach ($inputAll['member_ticket_id'] as $k => $v) {
                $t = microtime(true);
                $micro = sprintf("%03d", ($t - floor($t)) * 10000);
                $ticketRefNo = 'T' . date('ymdhis') . $micro . sprintf("%02d", $eventID);
                DB::table($tableNameTicket)->insert(
                        [
                            'order_id' => $orderID,
                            'event_id' => $eventID,
                            'ticket_price' => $inputAll['member_ticket_price'][$k],
                            'type_event_id' => $inputAll['member_ticket_id'][$k],
                            'type_souvenir_id' => (!empty($inputAll['member_souvenir_type_id'][$k]) ? $inputAll['member_souvenir_type_id'][$k] : null ),
                            //'type_souvenir_id_vip' => $inputAll['member_souvenir_type_id'][$k],
                            'ticket_contact_tel' => $inputAll['ticket_contact_tel'][$k],
                            'ticket_contact_name' => $inputAll['ticket_contact_name'][$k],
                            'blood_group' => $inputAll['blood_group'][$k],
                            'pre_name' => $inputAll['pre_name'][$k],
                            'tel' => $inputAll['tel'][$k],
                            'countries_id' => $inputAll['country'][$k],
                            'nationality' => $inputAll['nationality'][$k],
                            'religion' => $inputAll['religion'][$k],
                            'ticket_code' => $ticketRefNo,
                            'f_name' => $inputAll['f_name'][$k],
                            'bib' => (!empty($inputAll['member_bib'][$k]) ? $inputAll['member_bib'][$k] : null ),
                            'l_name' => $inputAll['l_name'][$k],
                            'gender' => $inputAll['member_gender'][$k],
                            'citizen_id' => $inputAll['citizen_id'][$k],
                            'birthday' => $inputAll['birthdate'][$k],
                            'email' => $inputAll['email'][$k]
                        //'form_additional' => $inputAll['member_ticket_price'][$k]
                        ]
                );
            }
            DB::commit();
            $results['status'] = "ok";
            return $results;
        } catch (\Throwable $e) {
            DB::rollback();
            $results['status'] = "fail";
            $results['error'] = "$e";
            return $results;
        }

        return $status;
    }

}
