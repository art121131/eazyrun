<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input,
    Redirect,
    DB;
use App\Models\User;
use Auth;
use Session;

class StaffContreller extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    //
    public function getIndex() {

        $user = \Auth::user();
        $event_id = $user->event_id;     

        $Users = User::leftJoin('tbl_event', 'tbl_event.event_id', '=', 'tbl_staff.event_id')
                ->where('tbl_staff.status', '!=', 'D')
                ->where('tbl_staff.event_id', '=', $event_id)
                ->orderBy('tbl_staff.id', 'desc')
                ->paginate(20);
        
        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->staffs)||$per->staffs==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }    
        //------------------------------------------------------------------------------//

        return \View::make('backend.apis.staffs')->with('Users', $Users);
    }

    public function getCreate() {
        
        $user = \Auth::user();
        
        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->staffs)||$per->staffs==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }    
        //------------------------------------------------------------------------------//
        
        return \View::make('backend.apis.createStaffs');
    }

#getCreate

    public function getEdit($id) {
        $User = User::where('id', $id)->get();
        
        $user = \Auth::user();
        
        //----------- check right -----------------------------------------------------//
        $per = $user->permission_menu;
        if(!empty($per)){
            $per = json_decode($per);
        }
        if(empty($per->staffs)||$per->staffs==0){
            return \Redirect::intended('login_admin')->with('textError', 'คุณไม่มีสิทธิ์เข้าใช้งาน');
            exit();
        }    
        //------------------------------------------------------------------------------//
        
        return \View::make('backend.apis.editStaffs')->with('User', $User);
    }

#getEdit

    public function postSaveform() {

        $action = Input::get('action');
        
        if ($action == 'CreateStaff') {
            
            $results = array();
            $input = Input::all();
            DB::beginTransaction();
            
            try {
                $username_dub = User::where('status', '!=', 'D')
                   ->where('username', '=', $input['username'])
                   ->first();

                if($username_dub != ''){
                    $results['status'] = "dup";
                    return $results;
                }

                $user = \Auth::user();
                $event_id = $user->event_id;
                
                $permission_menu = array(
                    "main" => (!empty($input['main']) ? 1 : 0),
                    "financial_classify" => (!empty($input['financial_classify']) ? 1 : 0),
                    "stock_report" => (!empty($input['stock_report']) ? 1 : 0),
                    "product_stock" => (!empty($input['product_stock']) ? 1 : 0),
                    "walkin" => (!empty($input['walkin']) ? 1 : 0),
                    "ticket_runner" => (!empty($input['ticket_runner']) ? 1 : 0),
                    "import_order" => (!empty($input['import_order']) ? 1 : 0),
                    "received" => (!empty($input['received']) ? 1 : 0),
                    "staffs" => (!empty($input['staffs']) ? 1 : 0),
                );

                $Users = User::orderBy('id', 'desc')
                        ->first();

                $UsersID = ($Users->id) + 1;

                $User = new User();
                $User->id = $UsersID;
                $User->username = Input::get('username');
                $User->password = \Hash::make(Input::get('password'));
                $User->name = Input::get('name');
                $User->email = Input::get('email');
                $User->level = 'Staff';
                $User->event_id = $event_id;
                $User->status = 'Y';
                $User->permission_menu = json_encode($permission_menu);
                $User->create_on = date("Y-m-d H:i:s");
                $User->save();
                
                DB::commit();
                $results['status'] = "ok";
                return $results;
            } catch (\Throwable $e) {
                DB::rollback();
                $results['status'] = "fail";
                $results['error'] = "$e";
                return $results;
            }
        }//Save

        if ($action == 'UpdateContentsList') {// 
            $action_up = Input::get('action_up');
            $_id = Input::get('id');

            if(!empty($_id)){
                $num = count($_id);

                switch ($action_up) {
                    case "ลบ | Delete" :
                        $query = "update tbl_staff set status='D' where ";
                        for ($i = 0; $i < $num; $i++) {
                            $query .= "(id = $_id[$i])";
                            if ($i < $num - 1) {
                                $query .= " or ";
                            }
                        }
                        DB::update($query);
                        break;


                    case "เผยแพร่ | Publish" :
                        echo $query = "update tbl_staff set status='Y' where ";
                        for ($i = 0; $i < $num; $i++) {
                            $query .= "(id = $_id[$i])";
                            if ($i < $num - 1) {
                                $query .= " or ";
                            }
                        }
                        DB::update($query);
                        break;

                    case "ซ่อน | Unpublish" :
                        $query = "update tbl_staff set status='N' where ";
                        for ($i = 0; $i < $num; $i++) {
                            $query .= "(id = $_id[$i])";
                            if ($i < $num - 1) {
                                $query .= " or ";
                            }
                        }
                        DB::update($query);
                        break;
                }#end switch
            }
        }

        if ($action == 'UpdateStaff') {
            
            $results = array();
            $input = Input::all();
            
            $id = Input::get('id');
            
            DB::beginTransaction();
            
            try {
                
                $permission_menu = array(
                    "main" => (!empty($input['main']) ? 1 : 0),
                    "financial_classify" => (!empty($input['financial_classify']) ? 1 : 0),
                    "stock_report" => (!empty($input['stock_report']) ? 1 : 0),
                    "product_stock" => (!empty($input['product_stock']) ? 1 : 0),
                    "walkin" => (!empty($input['walkin']) ? 1 : 0),
                    "ticket_runner" => (!empty($input['ticket_runner']) ? 1 : 0),
                    "import_order" => (!empty($input['import_order']) ? 1 : 0),
                    "received" => (!empty($input['received']) ? 1 : 0),
                    "staffs" => (!empty($input['staffs']) ? 1 : 0),
                );

                User::where('id', $id)->update(array(
                    'name' => Input::get('name'),
                    'email' => Input::get('email'),
                    'permission_menu' => json_encode($permission_menu),
                    'update_on' => date("Y-m-d H:i:s"),
                ));

                $change = Input::get('change');

                if ($change == 'on') {
                    $password = \Hash::make(Input::get('password'));

                    User::where('id', $id)
                            ->update(array('password' => $password
                    ));
                }#end change
                
                DB::commit();
                $results['status'] = "ok";
                return $results;
            } catch (\Throwable $e) {
                DB::rollback();
                $results['status'] = "fail";
                $results['error'] = "$e";
                return $results;
            }
        }//Save

        return Redirect::to('backoffice_management/staffs');
    }

    public function getLogout() {
        \Auth::logout();
        return Redirect::to('login_admin');
    }

}
