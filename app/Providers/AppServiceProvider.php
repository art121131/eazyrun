<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        view()->share('metaTitle', "Eazyrun.com");
        view()->share('metaKeyword', "Eazyrun.com");
        view()->share('metaDes', "Eazyrun.com");

        view()->share('rateVatFrontend', 3.75);
        view()->share('limitOrderFrontend', 5);
        view()->share('dateNow', date('Y-m-d H:i:s'));

        view()->share('linkPreview', "https://www.eazyrun.com"); 

        view()->share('menuEventName', "งานอีเว้นท์");
        view()->share('menuStaffName', "ผู้ดูแลระบบ"); 
        view()->share('menuOrderLists', "รายการสั่งซื้อ");
        view()->share('menuFinancialClassify', "รายงานการเงินแยกประเภท");
        view()->share('menuStockReport', "รายงานสต๊อกบัตร");
        view()->share('menuReceived', "รับของที่ระลึก");
        view()->share('menuWalkin', "ลงทะเบียน Walk-in");

        view()->share('menuSummary', "รายงานสรุปการสั่งซื้อ"); 
        view()->share('textPicture', "รูปภาพ");  
        view()->share('textPictureThumb', "รูปภาพปก"); 
  
        view()->share('ticketRegularPrice', "REGULAR"); 
        view()->share('ticketEarlybirdPrice', "EARLY BIRD");  

        view()->share('textStatusPublic', "โชว์"); 
        view()->share('textStatusUnPublic', "ซ่อน");
        view()->share('textStatusDelete', "ลบ"); 
        view()->share('textStatus', "สถานะ"); 
        view()->share('textSelect', "เลือก"); 
        view()->share('textTopic', "หัวข้อ");
        view()->share('textTypeTicket', "ประเภทตั๋ว");
        view()->share('textTypeSouvenir', "ของที่ระลึก");

        view()->share('textDateCreate', "วันที่สร้าง");   
        view()->share('textEdit', "แก้ไข"); 

        view()->share('btnTextAdd', "เพิ่ม | Add New"); 
        view()->share('btnTextDelete', "Delete"); 
        view()->share('btnTextEdit', "Edit"); 

        view()->share('Config_Key_Pre', "Running");
        view()->share('Config_Key_Last', "NCCEvent");
 
        view()->share('btnTextDelete', "Delete"); 
        view()->share('btnTextEdit', "Edit"); 

       //view()->share('configPathUrl', "https://www.12aughalfmarathon.com/events");
         view()->share('configPathUrl', "http://localhost:8080/run/v1");

        view()->share('textVip_2', "Postpaid"); 
        view()->share('textPaymentKbank', "KBANK"); 
        view()->share('textPaymentPsb', "PAYSBUY");
        view()->share('textPaymentPaypal', "PAYPAL");
        
        view()->share('textPayment2c2pCredit', "2c2p Credit");
        view()->share('textPayment2c2pCash', "2c2p Cash/Internet");
        view()->share('textPayment2c2p', "2c2p");
        view()->share('textPaymentWalkinCash', "Walk-in Cash");
        view()->share('textPaymentWalkinCredit', "Walk-in Credit");
        view()->share('textPaymentWalkinBarcode', "Walk-in Barcode");
        view()->share('textPaymentWalkinOther', "Walk-in");

        view()->share('textPaymentWalkin', "ทั่วไป");
        view()->share('textPaymentVIP', "VIP");
        view()->share('textPaymentPromotion', "1 Get 1 Free");

        view()->share('textPaymentStatusWait', "รอการชำระเงิน");
        view()->share('textPaymentStatusFail', "ไม่ทำการชำระเงิน");
        view()->share('textPaymentStatusSuccess', "ชำระเงินเรียบร้อยแล้ว");

        view()->share('textDesPicLogo', "ขนาดความกว้างไม่เกิน 100*100 พิกเซล <br> (อนุญาติเฉพาะไฟล์ .gif .jpg .jpeg .png เท่านั้น)");
        view()->share('textDesPicThumb', "ขนาดความกว้างไม่เกิน 380*200 พิกเซล <br> (อนุญาติเฉพาะไฟล์ .gif .jpg .jpeg .png เท่านั้น)");
        view()->share('textDesPicBG', "ขนาดความกว้างสูงไม่เกิน 450 พิกเซล <br> (อนุญาติเฉพาะไฟล์ .gif .jpg .jpeg .png เท่านั้น)");
        view()->share('textDesPicSouvenir', "ขนาดความกว้างไม่เกิน 800*600 พิกเซล <br> (อนุญาติเฉพาะไฟล์ .gif .jpg .jpeg .png เท่านั้น)");

        view()->share('lang_en', false);
        view()->share('menuBannerName', " Hero Banner");
      
        view()->share('menuLandingPage', " Landing Page");

        // view()->share('FbAppID', "115021259203512");//Sandbox
         view()->share('FbAppID', "110167149702506"); //Live 

      //  \URL::forceSchema('https');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
