<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
     public $timestamps = false;
     protected $table = 'tbl_event';
}
