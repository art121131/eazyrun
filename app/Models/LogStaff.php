<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogStaff extends Model
{
     public $timestamps = false;
     protected $table = 'tbl_log_staff';
}
