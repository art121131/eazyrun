<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeSouvenir extends Model
{
     public $timestamps = false;
     protected $table = 'tbl_type_souvenir';
     protected $primaryKey = 'souvenir_type_id';

     
}
