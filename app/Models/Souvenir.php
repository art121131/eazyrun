<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Souvenir extends Model
{
     public $timestamps = false;
     protected $primaryKey = 'souvenir_id';
     protected $table = 'tbl_souvenir';

     public function souvenirs()
    {
        return $this->hasMany(TypeSouvenir::class);
    }
}
