<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
     public $timestamps = false;
     protected $table = 'tbl_pages_infomation';
}
