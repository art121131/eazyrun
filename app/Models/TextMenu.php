<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextMenu extends Model
{
     public $timestamps = false;
     protected $table = 'tbl_text_menu';
}
